-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-11-2018 a las 20:31:03
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sqa`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarCliente` (IN `Gerente_Proyecto` VARCHAR(60), IN `Nombre_Cliente` VARCHAR(45), IN `Hora_Inicio` TIME, IN `Hora_Fin` TIME, IN `Horas_Laboradas` FLOAT, `Id_ClienteP` INT, IN `nombreAbreviado` VARCHAR(50))  BEGIN

UPDATE `cliente` SET `Gerente_Proyecto`=Gerente_Proyecto , `Nombre_Cliente`=Nombre_Cliente , `Hora_Inicio`=Hora_Inicio , `Hora_Fin`=Hora_Fin , `Horas_Laboradas`=Horas_Laboradas , `nombreAbreviado` =nombreAbreviado

where `Id_Cliente`=Id_ClienteP;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarDatosAnalista` (IN `id_CiudadP` INT, IN `Id_ServicioP` INT, IN `Id_UsuarioP` INT, IN `id_Rolp` INT, IN `id_cargoP` INT)  BEGIN

		UPDATE `datos_analista` 

		SET`Id_Ciudad` = id_CiudadP,`Id_Servicio` = Id_ServicioP, `Id_Rol` = id_Rolp, `Id_Cargo`=id_cargoP

		WHERE `datos_analista`.`Id_Datos` = Id_UsuarioP;		 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarRutaAnexos` (IN `Id_DatosR` INT, IN `Ruta_Anexos` VARCHAR(250))  BEGIN

UPDATE `datos_analista` SET `Ruta_Anexos`= Ruta_Anexos WHERE Id_Datos=Id_DatosR;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarUsuario` (IN `Id_Tipo_DocumentoP` INT, IN `Id_PrivilegioP` INT, IN `Lugar_ExpediP` VARCHAR(60), IN `EstadoP` TINYINT(1), IN `Id_UsuarioP` INT, IN `Ruta_AnexosP` VARCHAR(250), IN `idDatosP` INT, IN `FacturaP` VARCHAR(3), IN `UbicacionP` VARCHAR(50), IN `SalarioP` VARCHAR(15))  BEGIN 

UPDATE `usuario` 

SET `Id_Tipo_Documento` = Id_Tipo_DocumentoP, 

`Id_Privilegio` = Id_PrivilegioP,

`Lugar_Expedi` = Lugar_ExpediP,

`Estado` = EstadoP

WHERE `usuario`.`Id_Usuario` = Id_UsuarioP;

UPDATE `datos_analista` SET  `Salario` = SalarioP, `Ruta_Anexos`= Ruta_AnexosP, `Factura` = FacturaP, `Ubicacion`= UbicacionP  WHERE `Id_Datos`= idDatosP; 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Actualizar_datos` (IN `id` INT, `nombre` VARCHAR(60), `apellido` VARCHAR(60), `observa` VARCHAR(200), `gProyec` VARCHAR(50), `factura` CHAR(3), `facturacion` INT(11), `ubica` VARCHAR(20), `idCargo` INT, `idRol` INT, `idCiudad` INT, `idNovedad` INT, `idServicio` INT, `idCliente` INT)  BEGIN 



UPDATE datos_analista SET Nombres=nombre, Apellidos=apellido ,Observaciones=observa,Gerente_Proyectos=gProyec,Factura=factura,Facturacion=facturacion,Ubicacion=ubica,Id_Cargo=idCargo,Id_Rol=idRol,Id_Ciudad=idCiudad,Id_Novedad=idNovedad,Id_Servicio=idServicio,Id_Cliente=idCliente WHERE Id_datos=id;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Actualiza_documento` (IN `_nombres` VARCHAR(60), `_apellidos` VARCHAR(60), `_observacion` VARCHAR(200), `_factura` CHAR(2), `_facturacion` INT, `_ubicacion` VARCHAR(20), `_idCargo` INT, `_idRol` INT, `_idCiudad` INT, `_idNovedad` INT, `_idServicio` INT, `_idCliente` INT, `_identidad` INT, `_Id` INT)  BEGIN 



UPDATE Datos_Analista SET Nombres = _nombres, Apellidos = _apellidos, Observaciones = _observacion, Factura = _factura, Facturacion = _facturacion, Ubicacion = _ubicacion, Id_Cargo = _idCargo, Id_Rol = _idRol,  Id_Ciudad = _idCiudad, Id_Novedad = _idNovedad, Id_Servicio = _idServicio, Id_Cliente = _idCliente WHERE _Id = Id_Usuario;

UPDATE Usuario SET Num_Documento = _Identidad WHERE _Id = Id_Usuario;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `allcapacidad` ()  select nov.Id_Novedad as novedad,
  nov.Tipo_Novedad as tNovedad,
   dan.fecha as fecha_novedad,
   usuario.Id_Usuario as Id_Usuario,
   ciudad.Nombre_Ciudad,

CONCAT(datos_analista.Nombres,' ',datos_analista.Apellidos) AS Nombres,

usuario.Num_Documento, 

cargo.Nombre_Cargo,

rol.Nombre_Rol,

cliente.Nombre_Cliente,

cliente.Gerente_Proyecto,

   servicio.Nombre_Servicio,  

   datos_analista.Id_Datos,

datos_analista.Observaciones,
       datos_analista.Factura,
       datos_analista.Ubicacion
       
      from novedad nov right join datos_analista_novedad dan 
      on nov.Id_novedad = dan.novedad_Id_Novedad right join datos_analista da
       on dan.datos_analista_Id_Datos = da.Id_datos right join  usuario
      on da.Id_Usuario = usuario.Id_Usuario inner join datos_analista 

       on datos_analista.Id_Usuario  = usuario.Id_Usuario

       inner join ciudad

       on ciudad.Id_Ciudad = datos_analista.Id_Ciudad

       INNER JOIN servicio

       on servicio.Id_Servicio = datos_analista.Id_Servicio

       inner join cliente 

       on cliente.Id_Cliente = datos_analista.Id_Cliente

       LEFT join rol 

       on rol.Id_Rol = datos_analista.Id_Rol

       inner join cargo

       on cargo.Id_Cargo = datos_analista.Id_Cargo

 group by Id_Usuario$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `allcapacidad1` ()  select nov.Id_Novedad as novedad,
  nov.Tipo_Novedad as tNovedad,
  rm.nombre as nombre,
				rm.documento as documento,
				rm.horas as horas,
        rm.empresa as empresa,
   dan.fecha as fecha_novedad,
   usuario.Id_Usuario as Id_Usuario,
   ciudad.Nombre_Ciudad,

CONCAT(datos_analista.Nombres,' ',datos_analista.Apellidos) AS Nombres,

usuario.Num_Documento, 

cargo.Nombre_Cargo,

rol.Nombre_Rol,

cliente.Nombre_Cliente,

cliente.Gerente_Proyecto,

   servicio.Nombre_Servicio,  

   datos_analista.Id_Datos,

datos_analista.Observaciones,
       datos_analista.Factura,
       datos_analista.Ubicacion
       
      from novedad nov inner join datos_analista_novedad dan 
      on nov.Id_novedad = dan.novedad_Id_Novedad inner join datos_analista da
       on dan.datos_analista_Id_Datos = da.Id_datos inner join  usuario
      on da.Id_Usuario = usuario.Id_Usuario right join redmine rm 
      on usuario.Id_Usuario = rm.idusuario left join  datos_analista 

       on  rm.idusuario = datos_analista.Id_Usuario

       inner join ciudad

       on ciudad.Id_Ciudad = datos_analista.Id_Ciudad

       INNER JOIN servicio

       on servicio.Id_Servicio = datos_analista.Id_Servicio

       inner join cliente 

       on cliente.Id_Cliente = datos_analista.Id_Cliente

       LEFT join rol 

       on rol.Id_Rol = datos_analista.Id_Rol

       inner join cargo

       on cargo.Id_Cargo = datos_analista.Id_Cargo

 group by Id_Usuario$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `BuscarPermisosPorPagina` (IN `idPrivilegio` INT, IN `idAction` INT)  BEGIN

	select Distinct privilegios.Tipo_Privilegio, privilegios.Id_Privilegio, action.nombreUrl, action.idaction, permiso.tipoPrivilegios, permiso.idprivilegios

	from privilegios

	INNER JOIN permiso_has_action as p1

	ON privilegios.Id_Privilegio=p1.permiso_idprivilegios

	INNER JOIN permiso

	ON p1.privilegios_Id_Privilegio=permiso.idprivilegios

	INNER JOIN action

	ON action.idaction=p1.action_idaction

	where privilegios.Id_Privilegio=idPrivilegio and action.idaction=idAction;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `BuscarPorIdDatos` (IN `id` INT)  BEGIN

	select  usuario.Id_Usuario as Id_Usuario, usuario.Email, usuario.Num_Documento,

    ciudad.Nombre_Ciudad as Nombre_Ciudad,servicio.Nombre_Servicio, cliente.Nombre_Cliente, datos_analista.Id_Datos,rol.Nombre_Rol,cargo.Nombre_Cargo,concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) names 

    from usuario INNER join datos_analista

    on datos_analista.Id_Usuario  = usuario.Id_Usuario

    inner join ciudad

    on ciudad.Id_Ciudad = datos_analista.Id_Ciudad

    INNER JOIN servicio

    on servicio.Id_Servicio = datos_analista.Id_Servicio

    inner join cliente

    on cliente.Id_Cliente = datos_analista.Id_Cliente

    LEFT join rol

    on rol.Id_Rol = datos_analista.Id_Rol

    LEFT join cargo

    on cargo.Id_Cargo = datos_analista.Id_Cargo

    where usuario.Id_Usuario= id

	group by Id_Usuario;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `BuscarPorUsuario` (IN `Doc` VARCHAR(15))  BEGIN 

	select  usuario.Id_Usuario as Id_Usuario, 

	usuario.Email, usuario.Num_Documento, 

    ciudad.Nombre_Ciudad,servicio.Nombre_Servicio, cliente.Nombre_Cliente, datos_analista.Id_Datos,rol.Nombre_Rol,cargo.Nombre_Cargo, 

	concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) names 

	from usuario

	INNER join datos_analista

	on datos_analista.Id_Usuario  = usuario.Id_Usuario

	inner join ciudad

	on ciudad.Id_Ciudad = datos_analista.Id_Ciudad

	INNER JOIN servicio

	on servicio.Id_Servicio = datos_analista.Id_Servicio

	inner join cliente 

	on cliente.Id_Cliente = datos_analista.Id_Cliente

	LEFT join rol 

	on rol.Id_Rol = datos_analista.Id_Rol

	LEFT join cargo

    on cargo.Id_Cargo = datos_analista.Id_Cargo

    where usuario.Num_Documento=Doc

	group by Id_Usuario;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CALCULAREXTRAS` ()  BEGIN



DECLARE HORAS FLOAT; 

DECLARE resultado varchar (6);

SELECT Analista, Fecha,Horas, Comentario, Empresa_Contrata FROM temporal_extras;

SELECT Hora_Inicio, Hora_Fin, Horas_Laboradas FROM cliente;





	IF HORAS = Horas_Laboradas THEN

 

	SET resultado = 'HOED';

	

    elseif (HORAS < Horas_Laboradas) THEN

	

	SET  resultado = 'HOEN'; 

	

    elseif(Horas_inicio > '18:00' and Horas_Fin <= '12:00') THEN 

    

    SET resultado = 'HOED';

    

    elseif Horas_Inicio > '12:00' and Horas_Fin < '06:00' THEN 

    

    SET resultado = 'HOEN';

            

	END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `cambiarEstadoNovedad` (IN `Id_DatosAnalista` INT)  BEGIN	

		UPDATE datos_analista_novedad

		SET EstadoNovedad =0

		WHERE datos_analista_Id_Datos=Id_DatosAnalista AND EstadoNovedad = 1;	

	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Capacidad` (IN `numrRegistros` INT, IN `pagina` INT, IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN

CASE

    WHEN TIPOBUSQUEDA = 0 and busqueda like ''  THEN

select  usuario.Id_Usuario as Id_Usuario,

        ciudad.Nombre_Ciudad,

		CONCAT(datos_analista.Nombres,' ',datos_analista.Apellidos) AS Nombres,

  		usuario.Num_Documento, 

		cargo.Nombre_Cargo,

		rol.Nombre_Rol,

		cliente.Nombre_Cliente,

		cliente.Gerente_Proyecto,

        servicio.Nombre_Servicio,  

        datos_analista.Id_Datos,

		datos_analista.Observaciones,
        datos_analista.Factura,
        datos_analista.Ubicacion

       from usuario

        INNER join datos_analista 

        on datos_analista.Id_Usuario  = usuario.Id_Usuario

        inner join ciudad

        on ciudad.Id_Ciudad = datos_analista.Id_Ciudad

        INNER JOIN servicio

        on servicio.Id_Servicio = datos_analista.Id_Servicio

        inner join cliente 

        on cliente.Id_Cliente = datos_analista.Id_Cliente

        LEFT join rol 

        on rol.Id_Rol = datos_analista.Id_Rol

        inner join cargo

        on cargo.Id_Cargo = datos_analista.Id_Cargo

		group by Id_Usuario

        limit numrRegistros, pagina;	

   

   WHEN TIPOBUSQUEDA = 1  THEN

select  usuario.Id_Usuario as Id_Usuario,

        ciudad.Nombre_Ciudad,

		CONCAT(datos_analista.Nombres,' ',datos_analista.Apellidos) AS Nombres,

  		usuario.Num_Documento, 

		cargo.Nombre_Cargo,

		rol.Nombre_Rol,

		cliente.Nombre_Cliente,

		cliente.Gerente_Proyecto,

        servicio.Nombre_Servicio,  

        datos_analista.Id_Datos,

		datos_analista.Observaciones,
        datos_analista.Factura,
        datos_analista.Ubicacion

       from usuario

        INNER join datos_analista 

        on datos_analista.Id_Usuario  = usuario.Id_Usuario

        inner join ciudad

        on ciudad.Id_Ciudad = datos_analista.Id_Ciudad

        INNER JOIN servicio

        on servicio.Id_Servicio = datos_analista.Id_Servicio

        inner join cliente 

        on cliente.Id_Cliente = datos_analista.Id_Cliente

        LEFT join rol 

        on rol.Id_Rol = datos_analista.Id_Rol

        inner join cargo

        on cargo.Id_Cargo = datos_analista.Id_Cargo

		where CONCAT('%',usuario.Num_Documento,datos_analista.Nombres,ciudad.Nombre_Ciudad,cargo.Nombre_Cargo,rol.Nombre_Rol,

		cliente.Nombre_Cliente,cliente.Gerente_Proyecto,servicio.Nombre_Servicio,'%') like CONCAT('%',busqueda,'%') 

        group by Id_Usuario

        limit numrRegistros, pagina;

		
		 WHEN TIPOBUSQUEDA  = 2 THEN
select usuario.Id_Usuario as Id_Usuario,
        ciudad.Nombre_Ciudad,
		CONCAT(datos_analista.Nombres,' ',datos_analista.Apellidos) AS Nombres,
  		usuario.Num_Documento, 
		cargo.Nombre_Cargo,
		rol.Nombre_Rol,
		cliente.Nombre_Cliente,
		cliente.Gerente_Proyecto,
        servicio.Nombre_Servicio,  
        datos_analista.Id_Datos,
	   	datos_analista.Observaciones,
        datos_analista.Factura,
        datos_analista.Ubicacion,				
		  	datos_analista_novedad.fecha,
	    	datos_analista_novedad.Descripcion,
				novedad.Tipo_Novedad
        from usuario
        INNER join datos_analista 
        on datos_analista.Id_Usuario  = usuario.Id_Usuario
        inner join ciudad
        on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
        INNER JOIN servicio
        on servicio.Id_Servicio = datos_analista.Id_Servicio
        inner join cliente 
        on cliente.Id_Cliente = datos_analista.Id_Cliente
        inner join rol 
        on rol.Id_Rol = datos_analista.Id_Rol
        inner join cargo
        on cargo.Id_Cargo = datos_analista.Id_Cargo
				left join datos_analista_novedad 
				on  datos_analista.id_datos=datos_analista_novedad.datos_analista_Id_Datos
				left join novedad 
				on novedad.id_novedad = datos_analista_novedad.novedad_Id_Novedad
				WHERE EstadoNovedad!=0 OR EstadoNovedad IS NULL
				group by usuario.Id_Usuario
				order by usuario.Id_Usuario;

		 END CASE;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Capacidad1` ()  select  nov.Id_Novedad as novedad,
        nov.Tipo_Novedad as tNovedad,
        dan.fecha as fecha_novedad,
				ciu.Nombre_Ciudad,
				rm.nombre as nombre,
				rm.documento as documento,
				rm.horas as horas,
				car.Nombre_Cargo,
				rl.Nombre_Rol,
				rm.empresa as empresa,
				cl.Gerente_Proyecto,
				s.Nombre_Servicio,
				da.Factura,
				da.Ubicacion,
				da.Observaciones,
				da.Id_Datos,	
        u.Num_Documento	
				from redmine rm left join usuario u
			 on rm.idusuario = u.Id_Usuario
			  left join datos_analista da
        on da.Id_Usuario  = u.Id_Usuario
        left join datos_analista_novedad dan
        on  da.Id_datos = dan.datos_analista_Id_Datos
        left join novedad nov 
        on dan.novedad_Id_Novedad = nov.Id_novedad
        left join ciudad ciu
        on ciu.Id_Ciudad = da.Id_Ciudad
        left JOIN servicio s
        on s.Id_Servicio = da.Id_Servicio
        left join cliente cl
        on cl.Id_Cliente = da.Id_Cliente
        left join rol rl
        on rl.Id_Rol = da.Id_Rol
        left join cargo car

       on car.Id_Cargo = da.Id_Cargo


				group by rm.idusuario$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `capacidadConsumida` (IN `business_contracting_idParam` INT, IN `initialDate` DATE, IN `finalDate` DATE)  BEGIN

	select projects.name Proyecto, concat(users.firstname,' ',users.lastname) Nombres , sum(time_entries.hours)Horas, customers.name Cliente, count(DISTINCT(spent_on)) Dias, users.id 

	from time_entries

	inner join projects

	on projects.id = time_entries.project_id

	INNER join users

	on users.id = time_entries.user_id

	inner join customers 

	on customers.id = projects.business_contracting_id

	inner join custom_values

	on custom_values.customized_id= time_entries.id

	where projects.business_contracting_id=business_contracting_idParam and time_entries.spent_on between initialDate and finalDate and activity_id= 9 and custom_values.value='NO' 

	group by users.id

	order by Horas desc;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `capacidadInstaladaClient` (IN `paramFecha1` DATE, IN `paramFecha2` DATE, IN `paramIdProject` INT)  BEGIN

  SELECT

    CONCAT(`users`.`firstname`, ' ', `users`.`lastname`) 'Nombres',

    `users`.`title` 'Cargo',

    `projects`.`name` 'Proyecto',

    SUM(`time_entries`.`hours`) 'Horas',

    workdaydiff(paramFecha2, paramFecha1) 'CantidadDiasHabiles',

    (SELECT COUNT(DISTINCT `times`.`user_id`) FROM `time_entries` times WHERE `times`.`project_id` = `time_entries`.`project_id`) * SUM(`time_entries`.`hours`) * workdaydiff(paramFecha2, paramFecha1) 'CapacidadInstalada'

  FROM

    `time_entries`

      INNER JOIN `users` ON `time_entries`.`user_id` = `users`.`id`

      INNER JOIN `issues` ON `time_entries`.`issue_id` = `issues`.`id`

      INNER JOIN `projects` ON `projects`.`id` = `time_entries`.`project_id`

  WHERE

    `projects`.`business_contracting_id` = paramIdProject AND `time_entries`.`spent_on` BETWEEN paramFecha1 AND paramFecha2

  GROUP BY

    `projects`.`name`, `users`.`id`

  LIMIT 0 , 30;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Capacidadnumero` (IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN


CASE


WHEN TIPOBUSQUEDA = 0 and busqueda like ''  THEN

select count(vista.nombre) as cantidad from (select  ciu.Nombre_Ciudad,

		rm.nombre as nombre,

		rm.documento as documento,


				rm.horas as horas,

				car.Nombre_Cargo,


				rl.Nombre_Rol,


				rm.empresa as empresa,


				cl.Gerente_Proyecto,


				s.Nombre_Servicio,


				da.Factura,


				da.Ubicacion,


				da.Observaciones,


				da.Id_Datos		


				from redmine rm left join usuario u


			 on rm.idusuario = u.Id_Usuario

			  left join datos_analista da


        on da.Id_Usuario  = u.Id_Usuario


        left join ciudad ciu


        on ciu.Id_Ciudad = da.Id_Ciudad


        left JOIN servicio s


        on s.Id_Servicio = da.Id_Servicio


        left join cliente cl


        on cl.Id_Cliente = da.Id_Cliente


        left join rol rl


        on rl.Id_Rol = da.Id_Rol


        left join cargo car


       on car.Id_Cargo = da.Id_Cargo

				group by rm.idusuario
)as vista;

  WHEN TIPOBUSQUEDA = 1  THEN

 			select count(vista.nombre) as cantidad from (select  ciu.Nombre_Ciudad,

		rm.nombre as nombre,

		rm.documento as documento,


				rm.horas as horas,

				car.Nombre_Cargo,


				rl.Nombre_Rol,


				rm.empresa as empresa,


				cl.Gerente_Proyecto,


				s.Nombre_Servicio,


				da.Factura,


				da.Ubicacion,


				da.Observaciones,


				da.Id_Datos		


				from redmine rm left join usuario u


			 on rm.idusuario = u.Id_Usuario

			  left join datos_analista da


        on da.Id_Usuario  = u.Id_Usuario


        left join ciudad ciu


        on ciu.Id_Ciudad = da.Id_Ciudad


        left JOIN servicio s


        on s.Id_Servicio = da.Id_Servicio


        left join cliente cl


        on cl.Id_Cliente = da.Id_Cliente


        left join rol rl


        on rl.Id_Rol = da.Id_Rol


        left join cargo car


       on car.Id_Cargo = da.Id_Cargo				

				where CONCAT('%',u.Num_Documento,da.Nombres,ciu.Nombre_Ciudad,car.Nombre_Cargo,rl.Nombre_Rol,

		cl.Nombre_Cliente,cl.Gerente_Proyecto,s.Nombre_Servicio,'%') like CONCAT('%',busqueda,'%') 

		group by rm.idusuario)as vista;

			 


				

 END CASE;






END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `capInstalada` (IN `idCliente` INT, IN `fechaInicio` DATE, IN `fechaFin` DATE)  BEGIN 

SELECT (count(*) * cliente.Horas_Laboradas* workdaydiff(fechaFin,fechaInicio))Capacidad, cliente.Horas_Laboradas, COUNT(*) Analistas,  workdaydiff(fechaFin,fechaInicio) dias, cliente.Nombre_Cliente, nombreAbreviado

FROM datos_analista

INNER JOIN cargo

ON datos_analista.Id_Cargo = cargo.Id_Cargo

INNER JOIN cliente

on cliente.Id_Cliente = datos_analista.Id_Cliente

WHERE cargo.Id_Cargo = 1 and cliente.Id_Cliente = idCliente;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CargarItemsMenu` (IN `idprivilegio` INT)  BEGIN

	select distinct 

                action2.idaction, action2.urlAction, action2.nombreUrl, action2.action_idaction, action2.classValue

                from action

                Inner Join action as action2 ON action.action_idaction=action2.action_idaction

                Inner Join action as action3 ON action2.action_idaction= action3.idaction

                Inner Join action as action4 ON action3.action_idaction= action4.idaction

                Inner JOIN permiso_has_action as pha ON action2.idaction=pha.action_idaction

                INNER JOIN privilegios as p ON pha.permiso_idprivilegios=p.Id_Privilegio

				where p.Id_Privilegio=idprivilegio

                group by action2.idaction

                order by action2.nombreUrl asc;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CargarMenu` (IN `idprivilegio` INT)  BEGIN

	select distinct action4.idaction, action4.urlAction, action4.nombreUrl, action4.action_idaction, action4.classValue

                from action

                Inner Join action as action2 ON action.action_idaction=action2.action_idaction

                Inner Join action as action3 ON action2.action_idaction= action3.idaction

                Inner Join action as action4 ON action3.action_idaction= action4.idaction

                Inner JOIN permiso_has_action as pha ON action2.idaction=pha.action_idaction

                INNER JOIN privilegios as p ON pha.permiso_idprivilegios=p.Id_Privilegio

                where p.Id_Privilegio=idprivilegio

                group by action2.idaction

                order by action4.nombreUrl desc;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CargarSubMenu` (IN `idprivilegio` INT)  BEGIN

	select distinct

               action3.idaction, action3.urlAction, action3.nombreUrl, action3.action_idaction, action3.classValue

                from action

                Inner Join action as action2 ON action.action_idaction=action2.action_idaction

                Inner Join action as action3 ON action2.action_idaction= action3.idaction

                Inner Join action as action4 ON action3.action_idaction= action4.idaction

                Inner JOIN permiso_has_action as pha ON action2.idaction=pha.action_idaction

                INNER JOIN privilegios as p ON pha.permiso_idprivilegios=p.Id_Privilegio

                where p.Id_Privilegio=idprivilegio and action3.urlAction like 'Sub-Menu'

                group by action2.idaction

                order by action3.nombreUrl asc;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CargosUsuarios` (IN `Id_Usuario` INT)  BEGIN
	SELECT ca.Nombre_Cargo FROM  cargo ca
	inner join datos_analista da
	on ca.Id_Cargo = da.Id_Cargo
	inner join  usuario u
	on da.Id_Usuario = u.Id_Usuario
	where u.Id_Usuario like(Id_Usuario);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarDatosAnalisEditar` (IN `idDatos` INT)  BEGIN

SELECT Id_Datos,Id_Ciudad,Id_Servicio, Id_Cliente,Id_Cargo, Id_Usuario

from datos_analista

where Id_Datos=idDatos;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarDatosAnalisidUsus` (IN `id_UsuarioP` INT)  BEGIN



SELECT Id_Datos,Id_Ciudad,Id_Servicio, Id_Cliente,Id_Cargo, Id_Usuario, Ruta_Anexos, Factura, Ubicacion, Salario



from datos_analista



where Id_Usuario=id_UsuarioP;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarDatosEstudioEditar` (IN `id_Estudios` INT)  BEGIN

SELECT Id_Estudio,Id_Tipo_Estudio,Titulo,Institucion,Fecha_Inicio,Fecha_Fin,Observaciones,Tarjeta_Profesional,Nivel,Ciudad

from estudio

where Id_Estudio=id_Estudios;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_usuario` ()  NO SQL
SELECT dts.Id_Datos, dts.Nombres, dts.Apellidos, dts.Direccion, dts.Telefono, dts.Celular, dts.Ruta_Anexos, u.Num_Documento,tp.Descripcion

        FROM datos_analista dts



        INNER JOIN usuario u

        ON dts.Id_Usuario = u.Id_Usuario



        INNER JOIN tipo_documento tp

        ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultaUsuarioEditar` (IN `idUsuarioP` INT)  BEGIN 

select usuario.*, privilegios.Tipo_Privilegio

FROM usuario 

INNER JOIN privilegios 

on usuario.Id_Privilegio = privilegios.Id_Privilegio

where usuario.Id_Usuario = idUsuarioP;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Consulta_Analista` ()  BEGIN

SELECT Id_Datos, 

CONCAT(Nombres,' ', Apellidos) AS 'Nombre completo', 

usuario.num_documento AS 'Cédula', 

usuario.nombre_usuario AS 'Usuario', cargo.nombre_cargo 

FROM datos_analista, usuario, cargo

WHERE usuario.id_usuario = datos_analista.id_usuario 

and cargo.id_cargo = datos_analista.id_cargo;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Consulta_Auditoria` (`Nombre_Nuevo` VARCHAR(60), `Nombre_Viejo` VARCHAR(60))  BEGIN



DECLARE _campo VARCHAR(60);



SET _campo = CONCAT(Nombre_Nuevo, Nombre_Viejo);



IF (campo != NULL) THEN



SELECT Nombre_Nuevo, Nombre_Viejo

FROM auditoria;



END IF;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CounCapacidad` (IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN

CASE

    WHEN TIPOBUSQUEDA =0 and busqueda like '' THEN

select count(*)

       from usuario

        INNER join datos_analista 

        on datos_analista.Id_Usuario  = usuario.Id_Usuario

        inner join ciudad

        on ciudad.Id_Ciudad = datos_analista.Id_Ciudad

        INNER JOIN servicio

        on servicio.Id_Servicio = datos_analista.Id_Servicio

        inner join cliente 

        on cliente.Id_Cliente = datos_analista.Id_Cliente

        LEFT join rol 

        on rol.Id_Rol = datos_analista.Id_Rol

        inner join cargo

        on cargo.Id_Cargo = datos_analista.Id_Cargo

;

   WHEN TIPOBUSQUEDA = 1  THEN

select  count(*)

       from usuario

       INNER join datos_analista 

        on datos_analista.Id_Usuario  = usuario.Id_Usuario

        inner join ciudad

        on ciudad.Id_Ciudad = datos_analista.Id_Ciudad

        INNER JOIN servicio

        on servicio.Id_Servicio = datos_analista.Id_Servicio

        inner join cliente 

        on cliente.Id_Cliente = datos_analista.Id_Cliente

        LEFT join rol 

        on rol.Id_Rol = datos_analista.Id_Rol

        inner join cargo

        on cargo.Id_Cargo = datos_analista.Id_Cargo

		where CONCAT(usuario.Num_Documento,datos_analista.Nombres,ciudad.Nombre_Ciudad,cargo.Nombre_Cargo,rol.Nombre_Rol,

		cliente.Nombre_Cliente,cliente.Gerente_Proyecto,servicio.Nombre_Servicio) like CONCAT('%',busqueda,'%') 

;

END CASE;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CountHistoricos` (IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50), IN `fechaInicio` VARCHAR(10), IN `fechaFin` VARCHAR(10))  BEGIN

CASE

   WHEN TIPOBUSQUEDA = 0  and busqueda like ''  THEN

SELECT

   count(Id) AS hitoricos

   FROM auditoria where Fecha_Registro between fechaInicio AND fechaFin;

WHEN TIPOBUSQUEDA = 1   THEN

SELECT

   count(Id) AS historicos

   FROM auditoria  where Fecha_Registro between fechaInicio AND fechaFin AND

   

   (ACTION like concat('%',busqueda,'%') OR Fecha_Registro like concat('%',busqueda,'%') OR Nombre_Viejo like concat('%',busqueda,'%') OR Nombre_Nuevo like concat('%',busqueda,'%') OR

   Apellido_Viejo like concat('%',busqueda,'%') OR Apellido_Nuevo like concat('%',busqueda,'%') OR Factura_Nuevo like concat('%',busqueda,'%') OR

   Ubicacion_Viejo like concat('%',busqueda,'%') OR Ubicacion_Nuevo like concat('%',busqueda,'%') OR  Observacion_Viejo like concat('%',busqueda,'%') OR

   Observacion_Nuevo like concat('%',busqueda,'%') OR TipoNovedad like concat('%',busqueda,'%'));   

END CASE;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `countListClientes` (IN `busqueda` VARCHAR(30))  BEGIN

	SELECT count(*)

	FROM `cliente`

	WHERE cliente.Nombre_Cliente like concat('%',busqueda,'%')

    ;	

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CrearUsuario` (IN `Id_Tipo_DocumentoP` INT, IN `Id_PrivilegioP` INT, IN `Num_DocumentoP` VARCHAR(15), IN `Lugar_ExpediP` VARCHAR(60), IN `idUsuario` INT(45), IN `PasswordP` VARCHAR(250), IN `EmailP` VARCHAR(50), IN `EstadoP` TINYINT(1), IN `nombre` VARCHAR(50), IN `apellido` VARCHAR(50), IN `Ruta_AnexosP` VARCHAR(250), IN `SalarioP` VARCHAR(12), IN `CreatedP` VARCHAR(25), IN `FacturaP` VARCHAR(2), IN `UbicacionP` VARCHAR(50))  BEGIN 

START TRANSACTION;

INSERT INTO `usuario` 

(`Id_Usuario`, `Id_Tipo_Documento`, `Id_Privilegio`, `Num_Documento`, `Lugar_Expedi`, `Password`, `Email`, `Estado`, `Fecha_Registro`,  `Created`) 

VALUES (idUsuario, Id_Tipo_DocumentoP, Id_PrivilegioP, Num_DocumentoP, Lugar_ExpediP, PasswordP, EmailP, EstadoP, now(),   CreatedP);

INSERT INTO `datos_analista` (`Id_Cargo`, `Id_Rol`, `Id_Ciudad`, `Id_Servicio`, `Id_Cliente`, `Id_Usuario`, `Nombres`, `Apellidos`, `Direccion`, `Telefono`, `Celular`, `Ruta_Anexos`, `Observaciones`, `Factura`, `Facturacion`, `Ubicacion`, `Fecha_Registro`, `Salario` ) 

VALUES (NULL, NULL, NULL, NULL, NULL, idUsuario, nombre, apellido, NULL, NULL, NULL, Ruta_AnexosP, NULL, FacturaP, NULL, UbicacionP, CURRENT_TIMESTAMP, SalarioP);

COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Desactiva_Analista` (IN `tipNovedad` VARCHAR(35), IN `identity` INT)  BEGIN

DECLARE state VARCHAR(1);

   

SELECT t2.Id_Usuario,t3.Tipo_Novedad,t2.fecha_registro,t2.Num_Documento, t2.estado 

FROM datos_analista t1, usuario t2, novedad t3 

where t1.Id_Usuario = t2.Id_Usuario and t1.Id_Novedad= t3.Id_Novedad;



IF (t3.Tipo_Novedad = 'Renuncia' AND t2.fecha_registro > DAY(60))THEN

SET state = 0 and state=t2.estado;



END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarUsuario` (IN `idUsuarioP` INT)  BEGIN 
DELETE FROM usuario
WHERE usuario.Id_Usuario =idUsuarioP;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EXTRA` ()  BEGIN
insert into TBL_PRUEBA
SELECT Analista_datos, Fecha, Comentario, Horas,  Empresa_Contratante, estados
 FROM (
		SELECT DISTINCT concat (D.Nombres,' ',D.Apellidos) as Analista_datos,  'IGUAL' as estados, 
		T.Fecha, T.Comentario, T.Horas, T.Empresa_Contrata as Empresa_Contratante
		FROM datos_analista D, temporal_extras T
		WHERE T.Analista=concat (D.Nombres,' ',D.Apellidos)
		UNION 
		SELECT DISTINCT T.Analista AS Analista_datos, 'TEMPORAL' as estados,
		T.Fecha, T.Comentario, T.Horas, T.Empresa_Contrata as Empresa_Contratante
		FROM temporal_extras T
		WHERE NOT EXISTS (SELECT concat (D.Nombres,' ',D.Apellidos) as Analista_datos
						  FROM datos_analista D WHERE concat (D.Nombres,' ',D.Apellidos)=T.Analista)
		UNION
		SELECT DISTINCT concat (D.Nombres,' ',D.Apellidos) as Analista_datos, 'DATOS' as estados,
		'' AS Fecha, '' AS Comentario, '' AS Horas, '' AS Empresa_Contratante
		FROM datos_analista D
		WHERE NOT EXISTS (SELECT  T.Analista
						  FROM temporal_extras T
						  WHERE T.Analista=concat (D.Nombres,' ',D.Apellidos))
)TBL_PRUEBA;	

case 1 
WHEN 'IGUAL' THEN 
INSERT INTO horas_extras (Fecha,Comentario,Horas,Empresa_Contrata) 
					SELECT Fecha, Comentario,Horas,Empresa_Contrata FROM TBL_PRUEBA;
					select ' se inserto';
					iNSERT INTO horas_extras (Usuario,Fecha,Comentario,Cantidad_Horas,Empresa_Contrata) 
					SELECT Analista_datos,Fecha,Comentario,Horas,Empresa_Contratante FROM TBL_PRUEBA;
                    select'no inserto';
					                   	
           END CASE; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GenerarPdf` ()  BEGIN 
SELECT Fecha,Empresa_Contrata,Analista,Num_documento,Nombre_Cargo,horas FROM temporal_extras,usuario,cargo;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Generar_Reporte` ()  BEGIN

SELECT Ci.Nombre_Ciudad AS 'Ciudad',
CONCAT(DC.Nombres , DC.Apellidos) AS 
'Nombre completo',u.Num_Documento,C.Nombre_Cargo AS 'Cargo',
R.Nombre_Rol AS 'Rol',Cl.Nombre_Cliente AS 'Cliente',
DC.Ubicacion,NV.Tipo_Novedad AS 'Novedad',
Cl.Gerente_Proyecto AS 'Gerente de proyectos',
DC.Factura,DC.Facturacion,S.Nombre_Servicio AS 'Servicio',
DC.Observaciones

FROM datos_analista DC 
INNER JOIN Cargo C ON DC.Id_Cargo = C.Id_Cargo 
INNER JOIN Rol R ON R.Id_Rol = DC.Id_Rol 
INNER JOIN Ciudad Ci ON Ci.Id_Ciudad = DC.Id_Ciudad 
INNER JOIN Cliente Cl ON Cl.Id_Cliente = DC.Id_Cliente 
INNER JOIN Novedad NV ON NV.Id_Novedad = DC.Id_Novedad 
INNER JOIN Servicio S ON S.Id_Servicio = DC.Id_Servicio
INNER JOIN Usuario U ON U.Id_Usuario = DC.Id_Usuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getHorasExtraPorTipo` (IN `fechaInicio` VARCHAR(10), IN `fechaFin` VARCHAR(10), IN `id_user` INT)  begin 
		select *
		from horas_extras
		inner join detalle_horas
		on detalle_horas.Id_Horas_Extras = horas_extras.Id_Horas_Extras
		where horas_extras.Empresa_Contrata= id_user
		and horas_extras.Fecha BETWEEN fechaInicio 
		and fechaFin; 
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getHorasExtrasPorLiquidar` (IN `id_Usuario` INT, IN `fechaInicio` VARCHAR(10), IN `fechaFin` VARCHAR(10))  begin 
select case
when (horas.Cantidad_Horas - (select sum(detalle_horas.CantidadHoras) 
from detalle_horas where detalle_horas.Id_Horas_Extras= horas.Id_Horas_Extras) is null) then
horas.Cantidad_Horas 
when (horas.Cantidad_Horas - (select sum(detalle_horas.CantidadHoras) 
from detalle_horas where detalle_horas.Id_Horas_Extras= horas.Id_Horas_Extras) is not null) 
then
horas.Cantidad_Horas - (select sum(detalle_horas.CantidadHoras) 
from detalle_horas where detalle_horas.Id_Horas_Extras= horas.Id_Horas_Extras) 
end Horas,
horas.* 
from horas_extras horas where horas.Empresa_Contrata=id_Usuario and Fecha between fechaInicio AND fechaFin
order by horas.Id_Tipo_Hora_fk;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Historicos` (IN `numrRegistros` INT, IN `pagina` INT, IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50), IN `fechaInicio` VARCHAR(10), IN `fechaFin` VARCHAR(10))  BEGIN
CASE
   WHEN TIPOBUSQUEDA = 0  and busqueda like ''  THEN
SELECT
   Id,
   ACTION,
   Fecha_Registro,
   Nombre_Viejo,
   Nombre_Nuevo,
   Apellido_Viejo,
   Apellido_Nuevo,
   Factura_Nuevo,    
   Ubicacion_Viejo,
   Ubicacion_Nuevo,
   Observacion_Viejo,
   Observacion_Nuevo,
   TipoNovedad
   FROM auditoria where Fecha_Registro between fechaInicio AND fechaFin 
   
limit numrRegistros, pagina;
WHEN TIPOBUSQUEDA = 1   THEN
SELECT
   Id,
   ACTION,
   Fecha_Registro,
   Nombre_Viejo,
   Nombre_Nuevo,
   Apellido_Viejo,
   Apellido_Nuevo,
   Factura_Nuevo,    
   Ubicacion_Viejo,
   Ubicacion_Nuevo,
   Observacion_Viejo,
   Observacion_Nuevo,
   TipoNovedad
   FROM auditoria  
   where Fecha_Registro between fechaInicio AND fechaFin 
   and (ACTION like concat('%',busqueda,'%') OR Fecha_Registro like concat('%',busqueda,'%') OR Nombre_Viejo like concat('%',busqueda,'%') OR Nombre_Nuevo like concat('%',busqueda,'%') OR
   Apellido_Viejo like concat('%',busqueda,'%') OR Apellido_Nuevo like concat('%',busqueda,'%') OR Factura_Nuevo like concat('%',busqueda,'%') OR
   Ubicacion_Viejo like concat('%',busqueda,'%') OR Ubicacion_Nuevo like concat('%',busqueda,'%') OR  Observacion_Viejo like concat('%',busqueda,'%') OR
   Observacion_Nuevo like concat('%',busqueda,'%') OR TipoNovedad like concat('%',busqueda,'%'))
   
   
limit numrRegistros, pagina;
END CASE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `informacionDelPersonal` (IN `numrRegistros` INT, IN `pagina` INT, IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN
CASE
   WHEN TIPOBUSQUEDA = 0  and busqueda like ''  THEN
SELECT dts.Id_Datos, dts.Nombres, dts.Apellidos, u.Num_Documento, dts.Celular, dts.Ruta_Anexos, tp.Descripcion, cl.Nombre_Cliente, dts.Salario, u.Created, u.Lugar_Expedi, s.Nombre_Servicio
       , dts.Telefono, car.Nombre_Cargo, ro.Nombre_Rol, cy.Nombre_Ciudad
      FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null
       
limit numrRegistros, pagina;

  WHEN TIPOBUSQUEDA = 1 and busqueda is not null  THEN
SELECT dts.Id_Datos as Id_Datos, dts.Nombres, dts.Apellidos, u.Num_Documento, dts.Celular, dts.Ruta_Anexos, tp.Descripcion, cl.Nombre_Cliente, dts.Salario, u.Created, u.Lugar_Expedi, s.Nombre_Servicio
       , dts.Telefono, car.Nombre_Cargo, ro.Nombre_Rol, cy.Nombre_Ciudad
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (dts.Id_Datos like concat('%',busqueda,'%') 
       OR dts.Nombres like concat('%',busqueda,'%') 
       OR dts.Apellidos like concat('%',busqueda,'%') 
       OR u.Num_Documento like concat('%',busqueda,'%') 
       OR dts.Celular like concat('%',busqueda,'%') 
       OR dts.Ruta_Anexos like concat('%',busqueda,'%') 
       OR tp.Descripcion like concat('%',busqueda,'%') 
       OR dts.Salario like concat('%',busqueda,'%') 
       OR u.Created like concat('%',busqueda,'%') 
       OR u.Lugar_Expedi like concat('%',busqueda,'%') 
       OR dts.Celular like concat('%',busqueda,'%') 
       )
       group by Id_Datos
limit numrRegistros, pagina;

WHEN TIPOBUSQUEDA = 2 and busqueda is not null  THEN
SELECT dts.Id_Datos as Id_Datos, dts.Nombres, dts.Apellidos, u.Num_Documento, dts.Celular, dts.Ruta_Anexos, tp.Descripcion, cl.Nombre_Cliente, dts.Salario, u.Created, u.Lugar_Expedi, s.Nombre_Servicio
       , dts.Telefono, car.Nombre_Cargo, ro.Nombre_Rol, cy.Nombre_Ciudad, e.Tarjeta_Profesional, e.Titulo
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad
       
       INNER JOIN estudio e
       ON dts.Id_Datos = e.Id_Datos
       
       
       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (e.Tarjeta_Profesional like concat('%',busqueda,'%') 
       OR e.Titulo like concat('%',busqueda,'%'))
       group by Id_Datos
limit numrRegistros, pagina;

WHEN TIPOBUSQUEDA = 3 and busqueda is not null  THEN
SELECT dts.Id_Datos as Id_Datos, dts.Nombres, dts.Apellidos, u.Num_Documento, dts.Celular, dts.Ruta_Anexos, tp.Descripcion, cl.Nombre_Cliente, dts.Salario, u.Created, u.Lugar_Expedi, s.Nombre_Servicio
       , dts.Telefono, car.Nombre_Cargo, ro.Nombre_Rol, cy.Nombre_Ciudad
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (cl.Nombre_Cliente like concat('%',busqueda,'%') 
       )
       group by Id_Datos
limit numrRegistros, pagina;

WHEN TIPOBUSQUEDA = 4 and busqueda is not null  THEN
SELECT dts.Id_Datos as Id_Datos, dts.Nombres, dts.Apellidos, u.Num_Documento, dts.Celular, dts.Ruta_Anexos, tp.Descripcion, cl.Nombre_Cliente, dts.Salario, u.Created, u.Lugar_Expedi, s.Nombre_Servicio
       , dts.Telefono, car.Nombre_Cargo, ro.Nombre_Rol, cy.Nombre_Ciudad
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (s.Nombre_Servicio like concat('%',busqueda,'%') 
       )
       group by Id_Datos
limit numrRegistros, pagina;

WHEN TIPOBUSQUEDA = 5 and busqueda is not null  THEN
SELECT dts.Id_Datos as Id_Datos, dts.Nombres, dts.Apellidos, u.Num_Documento, dts.Celular, dts.Ruta_Anexos, tp.Descripcion, cl.Nombre_Cliente, dts.Salario, u.Created, u.Lugar_Expedi, s.Nombre_Servicio
       , dts.Telefono, car.Nombre_Cargo, ro.Nombre_Rol, cy.Nombre_Ciudad
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (car.Nombre_Cargo like concat('%',busqueda,'%')
       )
       group by Id_Datos
limit numrRegistros, pagina;

WHEN TIPOBUSQUEDA = 6 and busqueda is not null  THEN
SELECT dts.Id_Datos as Id_Datos, dts.Nombres, dts.Apellidos, u.Num_Documento, dts.Celular, dts.Ruta_Anexos, tp.Descripcion, cl.Nombre_Cliente, dts.Salario, u.Created, u.Lugar_Expedi, s.Nombre_Servicio
       , dts.Telefono, car.Nombre_Cargo, ro.Nombre_Rol, cy.Nombre_Ciudad
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (ro.Nombre_Rol like concat('%',busqueda,'%')
       )
       group by Id_Datos
limit numrRegistros, pagina;

WHEN TIPOBUSQUEDA = 7 and busqueda is not null  THEN
SELECT dts.Id_Datos as Id_Datos, dts.Nombres, dts.Apellidos, u.Num_Documento, dts.Celular, dts.Ruta_Anexos, tp.Descripcion, cl.Nombre_Cliente, dts.Salario, u.Created, u.Lugar_Expedi, s.Nombre_Servicio
       , dts.Telefono, car.Nombre_Cargo, ro.Nombre_Rol, cy.Nombre_Ciudad
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (cy.Nombre_Ciudad like concat('%',busqueda,'%') 
       )
       group by Id_Datos
limit numrRegistros, pagina;
END CASE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `informacionEstudios` (IN `numrRegistros` INT, IN `pagina` INT, IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN
CASE
    WHEN TIPOBUSQUEDA = 0  then
	
	    SELECT DISTINCT CONCAT(da.Nombres," ",da.Apellidos) AS nombres, usu.Num_Documento as Cedula,s.Descripcion,e.Titulo, te.Nivel_Formacion
        FROM estudio e

        INNER JOIN datos_analista da
        ON e.Id_Datos = da.Id_Datos

        INNER JOIN experiencia_laboral ex
        ON da.Id_Datos = ex.Id_Datos
        
        INNER JOIN sector s
        ON ex.Id_Sector = s.Id_Sector
		
		INNER JOIN usuario usu
		ON da.Id_Usuario = usu.Id_Usuario 
        
        INNER JOIN tipo_estudio as te
        ON te.Id_Tipo_Estudio= e.Id_Tipo_Estudio
		
		where e.Titulo is not null
		
		limit numrRegistros, pagina;
    
	WHEN TIPOBUSQUEDA = 1 THEN 
		SELECT DISTINCT CONCAT(da.Nombres," ",da.Apellidos) AS nombres, usu.Num_Documento as Cedula,s.Descripcion,e.Titulo, te.Nivel_Formacion
        FROM estudio e

        INNER JOIN datos_analista da
        ON e.Id_Datos = da.Id_Datos

        INNER JOIN experiencia_laboral ex
        ON da.Id_Datos = ex.Id_Datos
        
        INNER JOIN sector s
        ON ex.Id_Sector = s.Id_Sector
		
		INNER JOIN usuario usu
		ON da.Id_Usuario = usu.Id_Usuario
        
        INNER JOIN tipo_estudio as te
        ON te.Id_Tipo_Estudio= e.Id_Tipo_Estudio
		
        where e.Titulo is not null and CONCAT(da.Nombres, da.Apellidos, usu.Num_Documento, s.Descripcion, e.Titulo, te.Nivel_Formacion) like concat('%',busqueda,'%')
      limit numrRegistros, pagina;


	WHEN TIPOBUSQUEDA = 2 THEN
 
		SELECT DISTINCT CONCAT(da.Nombres," ",da.Apellidos) AS nombres, usu.Num_Documento as Cedula,s.Descripcion,e.Titulo, te.Nivel_Formacion
        FROM estudio e
        INNER JOIN datos_analista da
        ON e.Id_Datos = da.Id_Datos

        INNER JOIN experiencia_laboral ex
        ON da.Id_Datos = ex.Id_Datos
        
        INNER JOIN sector s
        ON ex.Id_Sector = s.Id_Sector
		
		INNER JOIN usuario usu
		ON da.Id_Usuario = usu.Id_Usuario
        
        INNER JOIN tipo_estudio as te
        ON te.Id_Tipo_Estudio= e.Id_Tipo_Estudio
		
        where e.Titulo is not null and s.Descripcion like concat ('%',busqueda,'%')
     limit numrRegistros, pagina;	
		
		 END CASE; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `IngresoUsuario` (IN `idUsuarioParam` INT)  BEGIN 
SELECT u.Nombre_Usuario,u.Num_Documento, u.Estado, u.Id_Privilegio, u.Id_Usuario, da.Id_Datos,u.Created, 
u.Lugar_Expedi,  pr.Tipo_Privilegio, da.*      
from usuario u
inner join privilegios pr
on u.Id_Privilegio = pr.Id_Privilegio

left join datos_analista da
on u.Id_Usuario = da.Id_Usuario
WHERE u.id_usuario= idUsuarioParam;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarCliente` (IN `Gerente_Proyecto` VARCHAR(60), IN `Nombre_Cliente` VARCHAR(45), IN `Hora_Inicio` TIME, IN `Hora_Fin` TIME, IN `Horas_Laboradas` FLOAT, `Id_ClienteP` INT, IN `nombreAbreviado` VARCHAR(50))  INSERT INTO `cliente`(`Id_Cliente`,`Gerente_Proyecto`, `Nombre_Cliente`, `Hora_Inicio`, `Hora_Fin`, `Horas_Laboradas`, `nombreAbreviado`) 
VALUES (Id_ClienteP,Gerente_Proyecto,Nombre_Cliente,Hora_Inicio,Hora_Fin,Horas_Laboradas,nombreAbreviado)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertarDatosAnalista` (IN `Id_CargoP` INT, IN `Id_RolP` INT, IN `id_CiudadP` INT, IN `Id_ServicioP` INT, IN `Id_ClienteP` INT, IN `Id_UsuarioP` INT)  BEGIN
		UPDATE `datos_analista` 
		SET `Id_Cargo` = Id_CargoP, `Id_Rol` = Id_RolP, `Id_Ciudad` = id_CiudadP, `Id_Servicio` = Id_ServicioP, `Id_Cliente` = Id_ClienteP 
		WHERE datos_analista.Id_Usuario = Id_UsuarioP;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarEstudio` (IN `Id_Datos` INT, IN `Id_Tipo_Estudio` INT, IN `Titulo` VARCHAR(100), IN `Institucion` VARCHAR(200), IN `Fecha_Inicio` VARCHAR(12), IN `Fecha_Fin` VARCHAR(12), IN `Observaciones` VARCHAR(100), IN `Tarjeta_Profesional` VARCHAR(30), IN `Nivel` VARCHAR(11), IN `Ciudad` VARCHAR(100))  INSERT INTO `estudio`(`Id_Datos`, `Id_Tipo_Estudio`, `Titulo`,
`Institucion`, `Fecha_Inicio`, `Fecha_Fin`, `Observaciones`, `Tarjeta_Profesional`,`Nivel`,`Ciudad`) 
VALUES (Id_Datos,Id_Tipo_Estudio,Titulo,Institucion,Fecha_Inicio,Fecha_Fin,Observaciones,Tarjeta_Profesional,Nivel,Ciudad)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarExcepcion` (IN `idCliente` INT, IN `idUsuario` INT, IN `Contara` INT)  BEGIN
set @existsRe= (select COUNT(*) from usuario_has_cliente where `usuario_Id_Usuario`=idUsuario and`cliente_Id_Cliente`=idCliente);

	Case
		WHEN @existsRe =0 THEN
			INSERT INTO usuario_has_cliente VALUES (null, idUsuario, idCliente, 1);
		WHEN @existsRe != 0 THEN
			UPDATE usuario_has_cliente SET Contar=0 WHERE usuario_Id_Usuario=idUsuario and cliente_Id_Cliente=idCliente;
            set @existsReUpdate= (select Contar from usuario_has_cliente where `usuario_Id_Usuario`=idUsuario and `cliente_Id_Cliente`=idCliente);
            set @idExcepcionA= (select idExcepcion from usuario_has_cliente where `usuario_Id_Usuario`=idUsuario and`cliente_Id_Cliente`=idCliente);
				CASE
					WHEN @existsReUpdate= 0 then
					DELETE FROM `usuario_has_cliente` WHERE `idExcepcion`=@idExcepcionA;
                END CASE;
    END CASE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarExperiencia` (IN `Id_Datos` INT, IN `Id_Sector` INT, IN `Trabaja_en_empresa` VARCHAR(3), IN `Empresa` VARCHAR(100), IN `Cargo` VARCHAR(100), IN `Fecha_Inicio` VARCHAR(12), IN `Fecha_Fin` VARCHAR(12), IN `Contacto` VARCHAR(15), IN `Descripcion` VARCHAR(1500))  INSERT INTO `experiencia_laboral`(`Id_Datos`, `Id_Sector`, `Trabaja_en_empresa`,
 `Empresa`, `Cargo`, `Fecha_Inicio`, `Fecha_Fin`, `Contacto`,`Descripcion`) 
VALUES (Id_Datos,Id_Sector,Trabaja_en_empresa,Empresa,Cargo,Fecha_Inicio,Fecha_Fin,Contacto,Descripcion)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarIdioma` (IN `Id_Datos` INT, IN `Id_Tipo_Estudio` INT, IN `Institucion` VARCHAR(200), IN `Nivel` INT, IN `Idioma` VARCHAR(50), IN `Ciudad` VARCHAR(100))  INSERT INTO `estudio`(`Id_Datos`, `Id_Tipo_Estudio`,`Institucion`, `Nivel`, `Idioma`,`Ciudad`) 
VALUES (Id_Datos,Id_Tipo_Estudio,Institucion,Nivel,Idioma,Ciudad)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarMisDatosPersonales` (IN `NombresP` VARCHAR(60), IN `ApellidosP` VARCHAR(60), IN `DireccionP` VARCHAR(50), IN `CelularP` VARCHAR(15), IN `TelefonoP` VARCHAR(10), IN `Lugar_ExpediP` VARCHAR(60), `IdUsuario` INT)  BEGIN
UPDATE `datos_analista` 
SET `Nombres` = NombresP, `Apellidos` = ApellidosP, 
`Direccion` = DireccionP, `Celular` = CelularP,
`Telefono` = TelefonoP
WHERE `datos_analista`.`Id_Usuario` = IdUsuario;
UPDATE `usuario` 
set `Lugar_Expedi` = Lugar_ExpediP
WHERE `usuario`.`Id_Usuario` = IdUsuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Insertar_datos` (IN `observa` VARCHAR(200), `gProyec` VARCHAR(50), `factura` CHAR(3), `facturacion` INT(11), `id_Rol` INT, `id_Ciudad` INT, `id_Novedad` INT, `id_Servicio` INT, `id_Cliente` INT, OUT `nombre` VARCHAR(60), `apellido` VARCHAR(60), `cedula` INT(15), `cargo` VARCHAR(60), `rol` VARCHAR(60), `cliente` VARCHAR(45))  BEGIN 
SELECT d.Id_datos AS 'Id',
d.Nombres,d.Apellidos,
u.Num_Documento AS 'Cédula',
c.Nombre_Cargo AS 'Cargo',
r.Nombre_Rol AS 'Rol',
cl.Nombre_Cliente AS'Cliente'

FROM datos_analista d, cargo c,
usuario u, rol r,cliente cl
where d.Id_Cargo = c.Id_cargo 
and d.Id_usuario = u.Id_usuario 
and d.Id_Rol = r.Id_Rol 
and d.Id_Cliente = cl.Id_Cliente;

INSERT INTO datos_analista (Observaciones,Gerente_Proyectos,Factura,
Facturacion,Id_Rol,Id_Ciudad,Id_Novedad,Id_Servicio,Id_Cliente) 
VALUES(Observa,gProyec,factura,facturacion,id_Rol,id_Ciudad,
id_Novedad ,id_Servicio ,id_Cliente );
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertDetalleDeHoras` (IN `id_datos_analistaParam` INT, IN `id_horas_extraParam` INT, IN `id_tipo_pagoParam` INT, IN `cantidad_horasParam` FLOAT)  BEGIN 
set @existsRe= (select Id_Horas_Extras from detalle_horas where Id_Horas_Extras=id_horas_extraParam and Id_Tipo_Pago = id_tipo_pagoParam); 
set @cantidad=(select CantidadHoras from detalle_horas where Id_Horas_Extras=id_horas_extraParam and Id_Tipo_Pago = id_tipo_pagoParam); 
case 
	when @existsRe =0 or  @existsRe is null then
	
			insert into detalle_horas values(null,id_datos_analistaParam, id_horas_extraParam, id_tipo_pagoParam, cantidad_horasParam); 
	when  @existsRe !=0  then
		update detalle_horas
		set CantidadHoras = cantidad_horasParam+@cantidad
		where Id_Horas_Extras = @existsRe and Id_Tipo_Pago = id_tipo_pagoParam; 
		
end case; 
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertNovedad` (IN `novedad_Id_NovedadParam` INT, IN `datos_analista_Id_DatosParam` INT, IN `cliente_Id_ClienteParam` INT, IN `EstadoNovedadParam` TINYINT(2), IN `fechaParam` DATETIME, IN `DescripcionParam` TEXT, IN `Factura` INT)  BEGIN 
	CASE
		
		WHEN cliente_Id_ClienteParam IS NULL or cliente_Id_ClienteParam = 0 THEN 
			INSERT INTO `datos_analista_novedad` 
			(`id_datos_analista_novedad`, `novedad_Id_Novedad`, `datos_analista_Id_Datos`, `cliente_Id_Cliente`, `EstadoNovedad`, `fecha`, `Descripcion`, `Factura`) 
			VALUES (NULL, novedad_Id_NovedadParam, datos_analista_Id_DatosParam, NULL, EstadoNovedadParam, CURRENT_TIMESTAMP, DescripcionParam, Factura);
		WHEN cliente_Id_ClienteParam IS NOT NULL OR cliente_Id_ClienteParam = 0 THEN 
			INSERT INTO `datos_analista_novedad` 
			(`id_datos_analista_novedad`, `novedad_Id_Novedad`, `datos_analista_Id_Datos`, `cliente_Id_Cliente`, `EstadoNovedad`, `fecha`, `Descripcion`, `Factura`) 
			VALUES (NULL, novedad_Id_NovedadParam, datos_analista_Id_DatosParam, cliente_Id_ClienteParam, EstadoNovedadParam, fechaParam, DescripcionParam, Factura);
		WHEN fechaParam IS NOT NULL OR fechaParam NOT LIKE '' THEN
			INSERT INTO `datos_analista_novedad` 
			(`id_datos_analista_novedad`, `novedad_Id_Novedad`, `datos_analista_Id_Datos`, `cliente_Id_Cliente`, `EstadoNovedad`, `fecha`, `Descripcion`, `Factura`) 
			VALUES (NULL, novedad_Id_NovedadParam, datos_analista_Id_DatosParam, NULL, EstadoNovedadParam, fechaParam, DescripcionParam, Factura);
	END CASE; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertPrivilegio` (IN `idActionParam` INT, IN `permisoPrivilegioParam` INT, IN `privilegiosParam` INT)  BEGIN 
		 INSERT INTO permiso_has_action VALUES(privilegiosParam,idActionParam,permisoPrivilegioParam);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertRol` (IN `nombre` VARCHAR(20))  begin 

INSERT INTO `privilegios` (`Id_Privilegio`, `Tipo_Privilegio`, `Fecha_Registro`) 
VALUES (NULL, nombre, CURRENT_TIMESTAMP);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListaEstudios` (IN `id_DatosAnalistaPrametro` INT)  BEGIN
SELECT  Observaciones,
    institucion,
    CONCAT(Fecha_Inicio, '--', Fecha_Fin) AS Fechas 
    from estudio
    where id_datos=id_DatosAnalistaPrametro ;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListaExperiencia` (IN `id_DatosAnalistaPrametro` INT)  BEGIN
SELECT
    Empresa, 
    Cargo,
    CONCAT(Fecha_Inicio, ' -- ', Fecha_Fin) AS Fechas
    
    from experiencia_laboral 
    where id_datos=id_DatosAnalistaPrametro;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListaFormacionAca` (IN `id_DatosAnalistaPrametro` INT)  BEGIN
SELECT  
    Titulo,
    institucion,
    CONCAT(Fecha_Inicio, '--', Fecha_Fin) AS Fechas 
    from estudio
    where id_datos=id_DatosAnalistaPrametro ;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListaIdiomas` (IN `id_DatosAnalistaPrametro` INT)  BEGIN
SELECT  
    Titulo,
    institucion,
    Nivel,
    Ciudad,
    te.Nivel_Formacion as TipoEs
    from estudio as es
    INNER JOIN tipo_Estudio AS te ON es.Id_Tipo_Estudio=te.Id_Tipo_Estudio
    where id_datos=id_DatosAnalistaPrametro;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarAnalistaPorCliente` ()  BEGIN
  SELECT
    SUM(`time_entries`.`hours`) 'Horas', CONCAT(`users`.`firstname`, ' ', `users`.`lastname`) 'Nombres'
  FROM
    `users`
      INNER JOIN `time_entries` ON `time_entries`.`user_id` = `users`.`id`
      INNER JOIN `custom_values` ON `time_entries`.`id` = `custom_values`.`customized_id`
      INNER JOIN `projects` ON `projects`.`id` = `time_entries`.`project_id`
  WHERE
    `custom_values`.`value` != 'NO'
      AND `custom_values`.`custom_field_id` = 4
      AND `projects`.`business_contracting_id` = 72
      AND `time_entries`.`spent_on` BETWEEN '2016-12-01' AND '2016-12-31'
  GROUP BY
    `time_entries`.`user_id`;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarCargos` ()  NO SQL
select *
from cargo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarCiudades` ()  NO SQL
select* FROM ciudad$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarClientes` ()  NO SQL
select *
FROM cliente$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarDatosAnalistasPost` (IN `pagina` INT, IN `numeroReg` INT, IN `typeSearch` INT, IN `SearchText` VARCHAR(50))  BEGIN
		CASE 
			WHEN typeSearch= 0 OR SearchText LIKE '' THEN /*ALL*/
				select  usuario.Id_Usuario as Id_Usuario, 
				usuario.Email, usuario.Num_Documento, 
				ciudad.Nombre_Ciudad,servicio.Nombre_Servicio, cliente.Nombre_Cliente, datos_analista.Id_Datos,rol.Nombre_Rol,cargo.Nombre_Cargo, 
				concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) names 
				from usuario
				INNER join datos_analista
				on datos_analista.Id_Usuario  = usuario.Id_Usuario
				inner join ciudad
				on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
				INNER JOIN servicio
				on servicio.Id_Servicio = datos_analista.Id_Servicio
				inner join cliente 
				on cliente.Id_Cliente = datos_analista.Id_Cliente
				LEFT join rol 
				on rol.Id_Rol = datos_analista.Id_Rol
				LEFT join cargo
				on cargo.Id_Cargo = datos_analista.Id_Cargo
				group by Id_Usuario
				limit pagina,numeroReg;
			WHEN typeSearch	= 1 AND SearchText NOT LIKE '' THEN /*Nombres*/
				select  usuario.Id_Usuario as Id_Usuario, 
				usuario.Email, usuario.Num_Documento, 
				ciudad.Nombre_Ciudad,servicio.Nombre_Servicio, cliente.Nombre_Cliente, datos_analista.Id_Datos,rol.Nombre_Rol,cargo.Nombre_Cargo,
				concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) names 
				from usuario
				INNER join datos_analista
				on datos_analista.Id_Usuario  = usuario.Id_Usuario
				inner join ciudad
				on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
				INNER JOIN servicio
				on servicio.Id_Servicio = datos_analista.Id_Servicio
				inner join cliente 
				on cliente.Id_Cliente = datos_analista.Id_Cliente
				LEFT join rol 
				on rol.Id_Rol = datos_analista.Id_Rol
				LEFT join cargo
				on cargo.Id_Cargo = datos_analista.Id_Cargo
				where CONCAT(Nombres, apellidos) like concat('%',SearchText,'%')
				group by Id_Usuario
				limit pagina,numeroReg;
			WHEN typeSearch	= 2 AND SearchText NOT LIKE '' THEN /*Email*/
				select  usuario.Id_Usuario as Id_Usuario, 
				usuario.Email, usuario.Num_Documento, 
				ciudad.Nombre_Ciudad,servicio.Nombre_Servicio, cliente.Nombre_Cliente, datos_analista.Id_Datos,rol.Nombre_Rol,cargo.Nombre_Cargo,
				concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) names 
				from usuario
				INNER join datos_analista
				on datos_analista.Id_Usuario  = usuario.Id_Usuario
				inner join ciudad
				on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
				INNER JOIN servicio
				on servicio.Id_Servicio = datos_analista.Id_Servicio
				inner join cliente 
				on cliente.Id_Cliente = datos_analista.Id_Cliente
				LEFT join rol 
				on rol.Id_Rol = datos_analista.Id_Rol
				LEFT join cargo
				on cargo.Id_Cargo = datos_analista.Id_Cargo
				where usuario.Email like concat('%',SearchText,'%')
				group by Id_Usuario
				limit pagina,numeroReg;
			WHEN typeSearch	= 3 AND SearchText NOT LIKE '' THEN /*Cliente*/
				select  usuario.Id_Usuario as Id_Usuario, 
				usuario.Email, usuario.Num_Documento, 
				ciudad.Nombre_Ciudad,servicio.Nombre_Servicio, cliente.Nombre_Cliente, datos_analista.Id_Datos,rol.Nombre_Rol,cargo.Nombre_Cargo,
				concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) names 
				from usuario
				INNER join datos_analista
				on datos_analista.Id_Usuario  = usuario.Id_Usuario
				inner join ciudad
				on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
				INNER JOIN servicio
				on servicio.Id_Servicio = datos_analista.Id_Servicio
				inner join cliente 
				on cliente.Id_Cliente = datos_analista.Id_Cliente
				LEFT join rol 
				on rol.Id_Rol = datos_analista.Id_Rol
				LEFT join cargo
				on cargo.Id_Cargo = datos_analista.Id_Cargo
				WHERE  cliente.Nombre_Cliente like concat('%',SearchText,'%')
				group by Id_Usuario
				limit pagina,numeroReg;			
		END CASE;	
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarNovedad` (IN `Id_DatosAnalista` INT)  BEGIN 
SELECT novedad.Tipo_Novedad, cliente.Nombre_Cliente, datos_analista_novedad.fecha,datos_analista_novedad.EstadoNovedad,datos_analista_novedad.Descripcion, datos_analista_novedad.Factura
FROM datos_analista_novedad 
INNER JOIN novedad
on novedad.Id_Novedad = datos_analista_novedad.novedad_Id_Novedad
LEFT JOIN  cliente
ON cliente.Id_Cliente = datos_analista_novedad.cliente_Id_Cliente
WHERE datos_analista_novedad.datos_analista_Id_Datos = Id_DatosAnalista
order by EstadoNovedad desc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarNovedades` ()  NO SQL
SELECT *
FROM novedad$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarNovedadInformeUser` (IN `Id_DatosAnalista` INT, IN `fechaInicio` DATE, IN `fechaFin` DATE)  BEGIN 
	SELECT novedad.Tipo_Novedad, cliente.Nombre_Cliente, datos_analista_novedad.fecha,datos_analista_novedad.EstadoNovedad,datos_analista_novedad.Descripcion
	FROM datos_analista_novedad 
	INNER JOIN novedad
	on novedad.Id_Novedad = datos_analista_novedad.novedad_Id_Novedad
	LEFT JOIN  cliente
	ON cliente.Id_Cliente = datos_analista_novedad.cliente_Id_Cliente
	WHERE datos_analista_novedad.datos_analista_Id_Datos = Id_DatosAnalista and  datos_analista_novedad.fecha >= fechaInicio and  datos_analista_novedad.fecha <= fechaFin
	order by EstadoNovedad desc;
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarNovedadParaInforme` (IN `Id_DatosAnalista` INT, IN `fechaInicio` DATE, IN `fechaFin` DATE)  BEGIN 
	SELECT novedad.Tipo_Novedad, cliente.Nombre_Cliente, datos_analista_novedad.fecha,datos_analista_novedad.EstadoNovedad,datos_analista_novedad.Descripcion
	FROM datos_analista_novedad 
	INNER JOIN novedad
	on novedad.Id_Novedad = datos_analista_novedad.novedad_Id_Novedad
	LEFT JOIN  cliente
	ON cliente.Id_Cliente = datos_analista_novedad.cliente_Id_Cliente
	INNER JOIN datos_analista 
    on datos_analista.Id_Datos = datos_analista_novedad.datos_analista_Id_Datos
	WHERE datos_analista.Id_Usuario = Id_DatosAnalista and  datos_analista_novedad.fecha >= fechaInicio and  datos_analista_novedad.fecha <= fechaFin
	order by EstadoNovedad desc;
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarPrivilegiosPorId` (IN `idPrivilegio` INT)  BEGIN 
select Distinct privilegios.Tipo_Privilegio, privilegios.Id_Privilegio, action.nombreUrl, action.idaction, permiso.tipoPrivilegios, permiso.idprivilegios
from privilegios
INNER JOIN permiso_has_action as p1
ON privilegios.Id_Privilegio=p1.permiso_idprivilegios
INNER JOIN permiso
ON p1.privilegios_Id_Privilegio=permiso.idprivilegios
INNER JOIN action
ON action.idaction=p1.action_idaction
where privilegios.Id_Privilegio=idPrivilegio; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarRoles` ()  NO SQL
select *
from rol$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarServicios` ()  NO SQL
SELECT*
FROM servicio$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarTipoDocumento` ()  BEGIN 
SELECT * FROM tipo_documento;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarTipoPrivilegio` ()  BEGIN 
select *
from privilegios;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listarTitulosEstudio` (IN `idDatos` INT, IN `idTipoEstudio` INT)  BEGIN    
   if  idTipoEstudio=8  THEN
   	SELECT e.Titulo FROM estudio e where e.Id_Datos=idDatos AND e.Id_Tipo_Estudio = 8;
else if idTipoEstudio=1  THEN
SELECT e.Tarjeta_Profesional FROM estudio e where e.Id_Datos=idDatos AND e.Id_Tipo_Estudio = 1;
   else 
SELECT e.Titulo FROM estudio e where e.Id_Datos=idDatos AND e.Id_Tipo_Estudio != 8;
END if;
   END if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarTodosMunicipios` ()  BEGIN
SELECT * from municipios; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarUsuarios` (IN `pagina` INT, IN `numrRegistros` INT, IN `typeSearch` INT, IN `SearchText` VARCHAR(50))  BEGIN 
		case 
		when typeSearch = 0 OR SearchText  LIKE '' then
			SELECT usuario.Id_Usuario, usuario.Id_Tipo_Documento, usuario.Id_Privilegio, usuario.Num_Documento, usuario.Lugar_Expedi, usuario.Nombre_Usuario, usuario.Email, usuario.Estado,	usuario.Fecha_Registro, privilegios.Tipo_Privilegio, concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) Nombres 
			FROM usuario
			inner join privilegios
			on usuario.Id_Privilegio = privilegios.Id_Privilegio
			INNER join datos_analista
			on datos_analista.Id_Usuario = usuario.Id_Usuario
			order by usuario.Fecha_Registro DESC
			limit pagina,numrRegistros;
		when typeSearch = 1 AND SearchText  NOT LIKE '' then 	
			SELECT usuario.Id_Usuario, usuario.Id_Tipo_Documento, usuario.Id_Privilegio, usuario.Num_Documento, usuario.Lugar_Expedi, usuario.Nombre_Usuario, usuario.Email, usuario.Estado,	usuario.Fecha_Registro, privilegios.Tipo_Privilegio, concat(datos_analista.Nombres ,' ', datos_analista.Apellidos)as names 
			FROM usuario
			inner join privilegios
			on usuario.Id_Privilegio = privilegios.Id_Privilegio
			INNER join datos_analista
			on datos_analista.Id_Usuario = usuario.Id_Usuario
			where Nombres like concat('%',SearchText,'%')
			or apellidos like concat('%',SearchText,'%')
			order by usuario.Fecha_Registro DESC
			limit pagina,numrRegistros;
		WHEN typeSearch = 2 AND SearchText NOT LIKE '' then	
			SELECT usuario.Id_Usuario, usuario.Id_Tipo_Documento, usuario.Id_Privilegio, usuario.Num_Documento, usuario.Lugar_Expedi, usuario.Nombre_Usuario, usuario.Email, usuario.Estado,	usuario.Fecha_Registro, privilegios.Tipo_Privilegio, concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) Nombres 
			FROM usuario
			inner join privilegios
			on usuario.Id_Privilegio = privilegios.Id_Privilegio
			INNER join datos_analista
			on datos_analista.Id_Usuario = usuario.Id_Usuario
			where usuario.Email like concat('%',SearchText,'%')
			order by usuario.Fecha_Registro DESC
			limit pagina,numrRegistros;
		END CASE;	
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarUsuariosAll` ()  BEGIN 
select  usuario.Id_Usuario as Id_Usuario, 
	usuario.Email, usuario.Num_Documento, 
    ciudad.Nombre_Ciudad,servicio.Nombre_Servicio, cliente.Nombre_Cliente, datos_analista.Id_Datos,rol.Nombre_Rol,cargo.Nombre_Cargo, 
	concat(datos_analista.Nombres ,' ', datos_analista.Apellidos) names 
	from usuario
	INNER join datos_analista
	on datos_analista.Id_Usuario  = usuario.Id_Usuario
	inner join ciudad
	on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
	INNER JOIN servicio
	on servicio.Id_Servicio = datos_analista.Id_Servicio
	inner join cliente 
	on cliente.Id_Cliente = datos_analista.Id_Cliente
	LEFT join rol 
	on rol.Id_Rol = datos_analista.Id_Rol
	LEFT join cargo
    on cargo.Id_Cargo = datos_analista.Id_Cargo
    where cargo.Nombre_Cargo like CONCAT('%','Especialista','%')
	group by Id_Usuario;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarUsuariosAsignarDatos` (IN `pagina` INT, IN `numrRegistros` INT)  BEGIN 
SELECT usuario.Id_Usuario, usuario.Id_Tipo_Documento, usuario.Id_Privilegio, usuario.Num_Documento, usuario.Lugar_Expedi, usuario.Nombre_Usuario, usuario.Email, usuario.Estado, usuario.Fecha_Registro, privilegios.Tipo_Privilegio, concat(datos_analista.Nombres,' ', datos_analista.Apellidos) Nombre
FROM usuario
INNER join privilegios
on usuario.Id_Privilegio = privilegios.Id_Privilegio
LEFT join datos_analista
on datos_analista.Id_Usuario= usuario.Id_Usuario
where usuario.Estado = 1 and  datos_analista.Id_Cargo is null and datos_analista.Id_Rol is null
order by usuario.Fecha_Registro desc
limit pagina,numrRegistros;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListaSector` ()  BEGIN
SELECT  
    *
    from sector;
    
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListaTipoEstudio` ()  BEGIN
SELECT  
    *
    from tipo_estudio;
    
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listClientes` (IN `pagina` INT, IN `numRegistros` INT, IN `tipoBusqueda` INT, IN `busqueda` VARCHAR(30))  BEGIN
	SELECT *
	FROM `cliente`
	WHERE cliente.Nombre_Cliente like concat('%',busqueda,'%')
	limit pagina,numRegistros;	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listCustomers` (IN `numParam` INT, IN `pag` INT, IN `typeShearch` INT, IN `shearch` VARCHAR(50))  BEGIN
  CASE
  WHEN typeShearch = 0 AND shearch LIKE '' THEN
    SELECT * FROM `customers` ORDER BY `name` ASC LIMIT numParam , pag;
  WHEN typeShearch = 1 AND shearch NOT LIKE '' THEN
    SELECT
      *
    FROM
      `customers`
    WHERE
      `customers`.`name` LIKE CONCAT('%', shearch, '%')
    ORDER BY `name` ASC
    LIMIT numParam , pag;
  END CASE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listCustomersForManagerApp` (IN `numParam` INT, IN `pag` INT, IN `typeShearch` INT, IN `shearch` VARCHAR(50))  BEGIN
  CASE
  WHEN typeShearch = 0 AND shearch LIKE '' THEN
    SELECT
      *
    FROM
      `customers`
    WHERE
      `customers`.`client_app` = 0
    ORDER BY `id` DESC
    LIMIT numParam , pag;
  WHEN typeShearch = 1 AND shearch NOT LIKE '' THEN
    SELECT
      *
    FROM
      `customers`
    WHERE
      `customers`.`name` LIKE CONCAT('%', shearch, '%') AND `customers`.`client_app` = 0
    ORDER BY `name` DESC
    LIMIT numParam , pag;
  END CASE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListorEstudios` (IN `Id_Datos` INT, IN `numrRegistros` INT, IN `pagina` INT, IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN
CASE
  WHEN TIPOBUSQUEDA=0  THEN 
  SELECT DISTINCT e.Id_Estudio, e.Titulo, tS.Nivel_Formacion, e.Institucion, e.Ciudad, e.Observaciones
      FROM estudio e

      INNER JOIN datos_analista da
       ON e.Id_Datos = da.Id_Datos

       INNER JOIN experiencia_laboral ex
       ON da.Id_Datos = ex.Id_Datos
       
       INNER JOIN Sector s
       ON ex.Id_Sector = s.Id_Sector

INNER JOIN usuario usu
ON da.Id_Usuario = usu.Id_Usuario

       INNER JOIN tipo_estudio as tS 
       ON e.Id_Tipo_Estudio=tS.Id_Tipo_Estudio

where e.Id_Datos= Id_Datos

limit numrRegistros, pagina;	

WHEN TIPOBUSQUEDA=1 THEN
  SELECT DISTINCT e.Id_Estudio, e.Titulo, tS.Nivel_Formacion, e.Institucion, e.Ciudad, e.Observaciones
      FROM estudio e

     INNER JOIN datos_analista da
       ON e.Id_Datos = da.Id_Datos

       INNER JOIN experiencia_laboral ex
       ON da.Id_Datos = ex.Id_Datos
       
       INNER JOIN Sector s
       ON ex.Id_Sector = s.Id_Sector

INNER JOIN usuario usu
ON da.Id_Usuario = usu.Id_Usuario

       INNER JOIN tipo_estudio as tS 
       ON e.Id_Tipo_Estudio=tS.Id_Tipo_Estudio

where e.Id_Datos =  Id_Datos and Concat(e.Titulo, tS.Nivel_Formacion, e.Institucion, e.Ciudad, e.Observaciones) like CONCAT('%', busqueda,'%')

limit numrRegistros, pagina;
END CASE;
   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listUsers` (IN `pagina` INT, IN `numRegistros` INT, IN `tipoBusqueda` INT, IN `busqueda` VARCHAR(30))  BEGIN
 CASE
 WHEN tipoBusqueda = 0 OR busqueda LIKE '' OR busqueda IS NULL THEN /* Todo */
   SELECT
     `users`.`id`,
     `users`.`firstname`,
     `users`.`lastname`,
     `users`.`login`,
     `users`.`mail`,
     `users`.`document`
   FROM
     `users`
   WHERE
     `user_sqa` = 0
   ORDER BY `users`.`id` DESC
   LIMIT pagina , numRegistros;
 WHEN tipoBusqueda = 1 AND busqueda NOT LIKE '' THEN /* Nombres-Apellidos */
   SELECT
     `users`.`id`,
     `users`.`firstname`,
     `users`.`lastname`,
     `users`.`login`,
     `users`.`mail`,
     `users`.`document`
   FROM
     `users`
   WHERE
     `users`.`firstname` LIKE CONCAT('%', busqueda, '%') OR `users`.`lastname` LIKE CONCAT('%', busqueda, '%') AND `user_sqa` = 0
   ORDER BY `users`.`id` DESC
   LIMIT pagina , numRegistros;
 WHEN tipoBusqueda = 2 AND busqueda NOT LIKE '' THEN /* Email */
   SELECT
     `users`.`id`,
     `users`.`firstname`,
     `users`.`lastname`,
     `users`.`login`,
     `users`.`mail`,
     `users`.`document`
   FROM
     `users`
   WHERE
     `users`.`login` LIKE CONCAT('%', busqueda, '%') AND `user_sqa` = 0
   ORDER BY `users`.`id` DESC
   LIMIT pagina , numRegistros;
 WHEN tipoBusqueda = 3 AND busqueda NOT LIKE '' THEN /* Usuario login */
   SELECT
     `users`.`id`,
     `users`.`firstname`,
     `users`.`lastname`,
     `users`.`login`,
     `users`.`mail`,
     `users`.`document`
   FROM
     `users`
   WHERE
     `users`.`mail` LIKE CONCAT('%', busqueda, '%') AND `user_sqa` = 0
   ORDER BY `users`.`id` DESC
   LIMIT pagina , numRegistros;
 END CASE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarDocuUsu` (IN `Num_DocumentoN` VARCHAR(15), IN `IdUsuarioI` INT)  BEGIN
UPDATE `usuario` 
SET `Num_Documento` = Num_DocumentoN
WHERE `usuario`.`Id_Usuario` = IdUsuarioI;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `nose` ()  begin 
	declare usuario varchar(45);
	declare Analista varchar(120);
	declare Fecha date;
	declare Hora float;
	declare Comentario varchar(200);
	declare Descripcion varchar (200); 
	declare Empresa_Contrata varchar(45);
	declare cliente varchar (45);
	declare Fecha1 date ;
	declare nose cursor for select Analista, Comentario, Fecha , Hora, Empresa_Cotrata
		from temporal_extras;
		open nose; 
			Fetch nose into Analista, Comentario, Fecha , Hora, Empresa_Contrata;
				if (Analista = usuario and Fecha > Fecha1) then
						insert into horas_extras  (usuario,Descripcion,Fecha,Cantidad_Horas,cliente) 
							select Analista, Comentario, Fecha , Hora, Empresa_Contrata 
								from temporal_extras;
		close nose;
					END IF;	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `novedades` (IN `customer` INT, IN `initialDate` DATE, IN `endDate` DATE)  BEGIN
  SELECT
    `time_entries`.`user_id` 'Usuario',
    SUM(`time_entries`.`hours`) 'horas',
    `projects`.`name` 'Proyecto',
    `issues`.`subject` 'Actividad',
    `time_entries`.`spent_on` 'fecha',
    `time_entries`.`comments` 'Comentario',
    CONCAT(`users`.`firstname`, ' ', `users`.`lastname`) 'Nombres',
    COUNT(DISTINCT (`spent_on`)) 'Dia'
  FROM
    `time_entries`
      INNER JOIN `users` ON `users`.`id` = `time_entries`.`user_id`
      INNER JOIN `projects` ON `projects`.`id` = `time_entries`.`project_id`
      INNER JOIN `issues` ON `issues`.`id` = `time_entries`.`issue_id`
      INNER JOIN `custom_values` ON `custom_values`.`customized_id` = `time_entries`.`id`
  WHERE
    `time_entries`.`project_id` IN (7430, 7790, 8012) AND `time_entries`.`spent_on` BETWEEN initialDate AND endDate AND `custom_values`.`value` = 'NO'
  GROUP BY
    `users`.`id`;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `numeroRegistrosPaginator` (IN `tipoBusqueda` INT, IN `busqueda` VARCHAR(30))  BEGIN
  CASE
  WHEN tipoBusqueda = 0 OR busqueda LIKE '' OR busqueda IS NULL THEN /* Todo */
    SELECT COUNT(*) FROM `users` WHERE `user_sqa` = 0;
  WHEN tipoBusqueda = 1 AND busqueda NOT LIKE '' THEN /* Nombres-Apellidos */
    SELECT COUNT(*) FROM `users` WHERE `users`.`firstname` LIKE CONCAT('%', busqueda, '%') OR `users`.`lastname` LIKE CONCAT('%', busqueda, '%') AND `user_sqa` = 0;
  WHEN tipoBusqueda = 2 AND busqueda NOT LIKE '' THEN /* Email */
    SELECT COUNT(*) FROM `users` WHERE `users`.`mail` LIKE CONCAT('%', busqueda, '%') AND `user_sqa` = 0;
  WHEN tipoBusqueda = 3 AND busqueda NOT LIKE '' THEN /* Usuario login */
    SELECT COUNT(*) FROM `users` WHERE `users`.`login` LIKE CONCAT('%', busqueda, '%') AND `user_sqa` = 0;
  END CASE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `numEstudios` (IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN
CASE
   WHEN TIPOBUSQUEDA = 0  then

   SELECT  count(DISTINCT CONCAT(da.Nombres," ",da.Apellidos), usu.Num_Documento,s.Descripcion,e.Titulo, te.Nivel_Formacion) as count
       FROM estudio e

       INNER JOIN datos_analista da
       ON e.Id_Datos = da.Id_Datos

       INNER JOIN experiencia_laboral ex
       ON da.Id_Datos = ex.Id_Datos
       
       INNER JOIN Sector s
       ON ex.Id_Sector = s.Id_Sector

		INNER JOIN usuario usu
		ON da.Id_Usuario = usu.Id_Usuario
       
       INNER JOIN tipo_estudio as tS 
		ON e.Id_Tipo_Estudio=tS.Id_Tipo_Estudio
	
		INNER JOIN tipo_estudio as te
        ON te.Id_Tipo_Estudio= e.Id_Tipo_Estudio
        
where tS.Nivel_Formacion is not null;
   
WHEN TIPOBUSQUEDA = 1 THEN 
SELECT count(DISTINCT CONCAT(da.Nombres," ",da.Apellidos), usu.Num_Documento,s.Descripcion,e.Titulo, te.Nivel_Formacion) as count
       FROM estudio e

       INNER JOIN datos_analista da
       ON e.Id_Datos = da.Id_Datos

       INNER JOIN experiencia_laboral ex
       ON da.Id_Datos = ex.Id_Datos
       
       INNER JOIN Sector s
       ON ex.Id_Sector = s.Id_Sector

INNER JOIN usuario usu
ON da.Id_Usuario = usu.Id_Usuario
       
       INNER JOIN tipo_estudio as tS 
      ON e.Id_Tipo_Estudio=tS.Id_Tipo_Estudio
      
		INNER JOIN tipo_estudio as te
        ON te.Id_Tipo_Estudio= e.Id_Tipo_Estudio
        
       where e.Titulo is not null and CONCAT(da.Nombres, da.Apellidos, usu.Num_Documento, s.Descripcion, e.Titulo, te.Nivel_Formacion) like concat('%',busqueda,'%')
     ;


WHEN TIPOBUSQUEDA = 2 THEN

SELECT count(DISTINCT CONCAT(da.Nombres," ",da.Apellidos), usu.Num_Documento,s.Descripcion,e.Titulo, te.Nivel_Formacion) as count
       FROM estudio e
       INNER JOIN datos_analista da
       ON e.Id_Datos = da.Id_Datos

       INNER JOIN experiencia_laboral ex
       ON da.Id_Datos = ex.Id_Datos
       
       INNER JOIN Sector s
       ON ex.Id_Sector = s.Id_Sector

INNER JOIN usuario usu
ON da.Id_Usuario = usu.Id_Usuario

       INNER JOIN tipo_estudio as tS 
       ON e.Id_Tipo_Estudio=tS.Id_Tipo_Estudio
       
       INNER JOIN tipo_estudio as te
        ON te.Id_Tipo_Estudio= e.Id_Tipo_Estudio
       
       where e.Titulo is not null and s.Descripcion like concat ('%',busqueda,'%')
       ;

END CASE; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `numListorEstudios` (IN `Id_Datos` INT, IN `numrRegistros` INT, IN `pagina` INT, IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN
CASE
  WHEN TIPOBUSQUEDA=0 and busqueda is null or busqueda='' THEN 
  SELECT count(DISTINCT e.Id_Estudio)
      FROM estudio e

      INNER JOIN datos_analista da
       ON e.Id_Datos = da.Id_Datos

       INNER JOIN experiencia_laboral ex
       ON da.Id_Datos = ex.Id_Datos
       
       INNER JOIN sector s
       ON ex.Id_Sector = s.Id_Sector

INNER JOIN usuario usu
ON da.Id_Usuario = usu.Id_Usuario

       INNER JOIN tipo_estudio as tS 
       ON e.Id_Tipo_Estudio=tS.Id_Tipo_Estudio

where e.Id_Datos= Id_Datos;	

WHEN TIPOBUSQUEDA=1 THEN
  SELECT count(DISTINCT e.Id_Estudio)
      FROM estudio e

     INNER JOIN datos_analista da
       ON e.Id_Datos = da.Id_Datos

       INNER JOIN experiencia_laboral ex
       ON da.Id_Datos = ex.Id_Datos
       
       INNER JOIN sector s
       ON ex.Id_Sector = s.Id_Sector

INNER JOIN usuario usu
ON da.Id_Usuario = usu.Id_Usuario

       INNER JOIN tipo_estudio as tS 
       ON e.Id_Tipo_Estudio=tS.Id_Tipo_Estudio

where e.Id_Datos =  Id_Datos and Concat(e.Titulo, tS.Nivel_Formacion, e.Institucion, e.Ciudad, e.Observaciones) like CONCAT('%', busqueda,'%')

limit numrRegistros, pagina;
END CASE;
   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `numRegistrosDeinformacionDelPersonal` (IN `TIPOBUSQUEDA` INT, IN `busqueda` VARCHAR(50))  BEGIN
CASE
   WHEN TIPOBUSQUEDA = 0  and busqueda like ''  THEN
SELECT count(*)
      FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null;

 WHEN TIPOBUSQUEDA = 1 and busqueda is not null  THEN
SELECT count(*)
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (dts.Id_Datos like concat('%',busqueda,'%') 
       OR dts.Nombres like concat('%',busqueda,'%') 
       OR dts.Apellidos like concat('%',busqueda,'%') 
       OR u.Num_Documento like concat('%',busqueda,'%') 
       OR dts.Celular like concat('%',busqueda,'%') 
       OR dts.Ruta_Anexos like concat('%',busqueda,'%') 
       OR tp.Descripcion like concat('%',busqueda,'%') 
       OR u.Salario like concat('%',busqueda,'%') 
       OR u.Created like concat('%',busqueda,'%') 
       OR u.Lugar_Expedi like concat('%',busqueda,'%') 
       OR dts.Celular like concat('%',busqueda,'%') 
       )
       group by dts.Id_Datos
;

WHEN TIPOBUSQUEDA = 2 and busqueda is not null  THEN
SELECT count(*)
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad
       
       INNER JOIN estudio e
       ON dts.Id_Datos = e.Id_Datos
       
       
       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (e.Tarjeta_Profesional like concat('%',busqueda,'%') 
       OR e.Titulo like concat('%',busqueda,'%'))
       group by dts.Id_Datos
;

WHEN TIPOBUSQUEDA = 3 and busqueda is not null  THEN
SELECT count(*)
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (cl.Nombre_Cliente like concat('%',busqueda,'%') 
       )
       group by dts.Id_Datos
;

WHEN TIPOBUSQUEDA = 4 and busqueda is not null  THEN
SELECT count(*)
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (s.Nombre_Servicio like concat('%',busqueda,'%') 
       )
       group by dts.Id_Datos
;

WHEN TIPOBUSQUEDA = 5 and busqueda is not null  THEN
SELECT count(*)
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (car.Nombre_Cargo like concat('%',busqueda,'%')
       )
       group by dts.Id_Datos
;

WHEN TIPOBUSQUEDA = 6 and busqueda is not null  THEN
SELECT count(*)
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (ro.Nombre_Rol like concat('%',busqueda,'%')
       )
       group by dts.Id_Datos
;

WHEN TIPOBUSQUEDA = 7 and busqueda is not null  THEN
SELECT count(*)
       FROM datos_analista dts

       LEFT JOIN cliente cl
       ON dts.Id_Cliente = cl.Id_Cliente

       INNER JOIN usuario u
       ON dts.Id_Usuario = u.Id_Usuario

       INNER JOIN tipo_documento tp
       ON u.Id_Tipo_Documento = tp.Id_Tipo_Documento
       
       INNER JOIN servicio s
       ON dts.Id_Servicio = s.Id_Servicio 
       
       INNER JOIN cargo car
       ON dts.Id_Cargo = car.Id_Cargo 
       
       INNER JOIN rol ro
       ON dts.Id_Rol = ro.Id_Rol
       
       INNER JOIN ciudad cy
       ON dts.Id_Ciudad = cy.Id_Ciudad

       where dts.Nombres is not null AND dts.Apellidos is not null AND dts.Ruta_Anexos is not null 
       AND (cy.Nombre_Ciudad like concat('%',busqueda,'%') 
       )
       group by dts.Id_Datos
;
END CASE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `numReglistarDatosAnalistasPost` (IN `typeSearch` INT, IN `SearchText` VARCHAR(50))  BEGIN
		CASE 
			WHEN typeSearch= 0 OR SearchText LIKE '' THEN 
				select  count(*)
				from usuario
				INNER join datos_analista
				on datos_analista.Id_Usuario  = usuario.Id_Usuario
				inner join ciudad
				on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
				INNER JOIN servicio
				on servicio.Id_Servicio = datos_analista.Id_Servicio
				inner join cliente 
				on cliente.Id_Cliente = datos_analista.Id_Cliente
				LEFT join rol 
				on rol.Id_Rol = datos_analista.Id_Rol
				LEFT join cargo
				on cargo.Id_Cargo = datos_analista.Id_Cargo;	
			WHEN typeSearch	= 1 AND SearchText NOT LIKE '' THEN 
				select count(*)
				from usuario
				INNER join datos_analista
				on datos_analista.Id_Usuario  = usuario.Id_Usuario
				inner join ciudad
				on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
				INNER JOIN servicio
				on servicio.Id_Servicio = datos_analista.Id_Servicio
				inner join cliente 
				on cliente.Id_Cliente = datos_analista.Id_Cliente
				LEFT join rol 
				on rol.Id_Rol = datos_analista.Id_Rol
				LEFT join cargo
				on cargo.Id_Cargo = datos_analista.Id_Cargo
				where Nombres like concat('%',SearchText,'%')
				or apellidos like concat('%',SearchText,'%');	
			WHEN typeSearch	= 2 AND SearchText NOT LIKE '' THEN 
				select count(*)
				from usuario
				INNER join datos_analista
				on datos_analista.Id_Usuario  = usuario.Id_Usuario
				inner join ciudad
				on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
				INNER JOIN servicio
				on servicio.Id_Servicio = datos_analista.Id_Servicio
				inner join cliente 
				on cliente.Id_Cliente = datos_analista.Id_Cliente
				LEFT join rol 
				on rol.Id_Rol = datos_analista.Id_Rol
				LEFT join cargo
				on cargo.Id_Cargo = datos_analista.Id_Cargo
				where usuario.Email like concat('%',SearchText,'%');

			WHEN typeSearch	= 3 AND SearchText NOT LIKE '' THEN 
				select  count(*)
				from usuario
				INNER join datos_analista
				on datos_analista.Id_Usuario  = usuario.Id_Usuario
				inner join ciudad
				on ciudad.Id_Ciudad = datos_analista.Id_Ciudad
				INNER JOIN servicio
				on servicio.Id_Servicio = datos_analista.Id_Servicio
				inner join cliente 
				on cliente.Id_Cliente = datos_analista.Id_Cliente
				LEFT join rol 
				on rol.Id_Rol = datos_analista.Id_Rol
				LEFT join cargo
				on cargo.Id_Cargo = datos_analista.Id_Cargo
				WHERE  cliente.Nombre_Cliente like concat('%',SearchText,'%');					
		END CASE;	
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `numRegListarUsuarios` (IN `typeSearch` INT, IN `SearchText` VARCHAR(50))  BEGIN 
		case 
		when typeSearch = 0 OR SearchText  LIKE '' then
			SELECT COUNT(*)
			FROM usuario
			inner join privilegios
			on usuario.Id_Privilegio = privilegios.Id_Privilegio
			INNER join datos_analista
			on datos_analista.Id_Usuario = usuario.Id_Usuario;
		when typeSearch = 1 AND SearchText  NOT LIKE '' then 
			SELECT COUNT(*)
			FROM usuario
			inner join privilegios
			on usuario.Id_Privilegio = privilegios.Id_Privilegio
			INNER join datos_analista
			on datos_analista.Id_Usuario = usuario.Id_Usuario
			where Nombres like concat('%',SearchText,'%') or Apellidos like concat('%',SearchText,'%');
	WHEN typeSearch = 2 AND SearchText NOT LIKE '' then	
			SELECT COUNT(*)
			FROM usuario
			inner join privilegios
			on usuario.Id_Privilegio = privilegios.Id_Privilegio
			INNER join datos_analista
			on datos_analista.Id_Usuario = usuario.Id_Usuario
			where usuario.Email like concat('%',SearchText,'%');
		END CASE;	
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ObtenerRol` (IN `idUser` INT)  BEGIN
	SELECT * FROM sqa.action as action1
	INNER JOIN action as action2 on action1.idaction=action2.action_idaction
	INNER JOIN action_has_permiso as ahp ON action2.idaction=ahp.action_idaction
	INNER JOIN permiso AS per ON ahp.permiso_idprivilegios=per.idprivilegios
	INNER JOIN privilegio_has_permiso as php ON ahp.id_action_has_privilegio=php.action_has_permiso_id_action_has_privilegio
	INNER JOIN privilegios as priv ON php.privilegios_Id_Privilegio= priv.Id_Privilegio
	INNER JOIN usuario as us on us.Id_Privilegio=priv.Id_Privilegio
	WHERE US.Id_Usuario=idUser;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `RECARGO` (IN `Id_Detalle_extras` INT(11), `Id_Datos` INT(11), `Id_Horas_Extras` INT(11), `Id_Tipo_Hora` INT(11), `Id_Tipo_Pago` INT(11), `Fecha` DATE, `Cantidad_Horas` FLOAT, `Comentario` VARCHAR(200), `Usuario` VARCHAR(120))  BEGIN


SELECT h.Id_Tipo_Hora, h.Descripcion,p.Id_Tipo_Pago, p.Descripcion
	FROM detalle_extras d 

	INNER JOIN tipo_hora h 
		ON h.Id_Tipo_Hora= d.Id_Tipo_Hora 

	INNER JOIN tipo_pago p 
		ON p.Id_Tipo_Pago= d.Id_Tipo_Pago;


	insert into horas_extras(Usuario,Fecha,Cantidad_Horas,Comentario)
		   value(Fecha,Cantidad_Horas,Emapresa_Contrata,Comentario);

	insert into detalle_horas(Id_Detalle_extras,Id_Datos,Id_Horas_Extras,Id_Tipo_Hora,Id_Tipo_Pago)
		   values(Id_Detalle_extras,Id_Datos,Id_Horas_Extras,Id_Tipo_Hora,Id_Tipo_Pago);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `restarDetalleDeHoras` (IN `id_datos_analistaParam` INT, IN `id_horas_extraParam` INT, IN `id_tipo_pagoParam` INT, IN `cantidad_horasParam` FLOAT, IN `idAnt` INT)  BEGIN 
set @existsRe= (select Id_Horas_Extras from detalle_horas where Id_Detalle_Horas=idAnt); 
set @cantidad=(select CantidadHoras from detalle_horas where Id_Detalle_Horas=idAnt); 
set @existsId= (select Id_Detalle_Horas from detalle_horas where Id_Detalle_Horas=idAnt); 
case 
	when  @existsId !=0  then
		update detalle_horas
		set CantidadHoras = @cantidad-cantidad_horasParam
		where Id_Horas_Extras = @existsRe and Id_Detalle_Horas=@existsId;
        
        set @cantidad=(select CantidadHoras from detalle_horas where Id_Detalle_Horas=idAnt);
        case
			when @cantidad = 0 or  @existsRe is null then
				DELETE FROM detalle_horas WHERE Id_Detalle_Horas=@existsId;
		
        end case; 
end case; 
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ValidarUsuario` (IN `paramUsuario` VARCHAR(41), IN `paramPass` VARCHAR(250))  BEGIN 
SELECT u.Nombre_Usuario, u.Estado, u.Id_Privilegio, u.Id_Usuario, da.Id_Datos,da.*        
from usuario u
inner join privilegios pr
on u.Id_Privilegio = pr.Id_Privilegio
left join datos_analista da
on u.Id_Usuario = da.Id_Usuario
WHERE u.Nombre_Usuario=paramUsuario and u.Password=paramPass;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `validateUser` (IN `loginParam` VARCHAR(25))  BEGIN
  SELECT
    `users`.`salt`, `users`.`hashed_password`, `users`.`id`
  FROM
    `users`
  WHERE
    `users`.`login` LIKE loginParam;
END$$

--
-- Funciones
--
CREATE DEFINER=`root`@`localhost` FUNCTION `workdaydiff` (`b` DATE, `a` DATE) RETURNS INT(11) BEGIN 

DECLARE freedays int;

SET freedays = 0;

SET @x = DATEDIFF(b, a);

IF @x<0 THEN

SET @m = a;

SET a = b;

SET b = @m;

SET @m = -1;

ELSE

SET @m = 1;

END IF;

SET @x = abs(@x) + 1;

 

SET @w1 = WEEKDAY(a)+1;

SET @wx1 = 8-@w1;

IF @w1>5 THEN

SET @w1 = 0;

ELSE

SET @w1 = 6-@w1;

END IF;



SET @wx2 = WEEKDAY(b)+1;

SET @w2 = @wx2;

IF @w2>5 THEN

SET @w2 = 5;

END IF;

 

SET @weeks = (@x-@wx1-@wx2)/7;

SET @noweekends = (@weeks*5)+@w1+@w2;

 

SET @result = @noweekends-freedays;

RETURN @result*@m;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `action`
--

CREATE TABLE `action` (
  `idaction` int(11) NOT NULL,
  `urlAction` varchar(45) DEFAULT NULL,
  `nombreUrl` varchar(45) DEFAULT NULL,
  `action_idaction` int(11) NOT NULL,
  `classValue` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `action`
--

INSERT INTO `action` (`idaction`, `urlAction`, `nombreUrl`, `action_idaction`, `classValue`) VALUES
(1, 'Menu', 'Configuración', 1, 'idU'),
(2, 'Sub-Menu', 'Gestión Usuarios', 1, 'param.idUsub'),
(3, 'AsignarDatos1.jsp', 'Asignar Cargo-Rol', 2, 'param.idDatosSu'),
(4, 'GestionUsuarios.jsp', 'Agregar Usuario', 2, NULL),
(5, 'Usuarios.jsp', 'Editar Datos de Registro', 2, NULL),
(6, 'EditarDatos1.jsp', 'Editar Datos Usuarios', 2, NULL),
(7, 'Menu', 'Informes', 7, 'param.idA'),
(8, 'Client.jsp', 'Informe Cargabilidad', 7, 'param.clienteInfor'),
(9, 'listarCapacidad.jsp', 'Informe de Capacidad ', 7, '${param.clienteInfor}'),
(10, 'Sub-Menu', 'Gestión Cliente', 1, 'param.gestion'),
(11, 'AddClient.jsp', 'Agregar cliente', 10, 'param.cliente'),
(12, 'EdicionDatosCliente.jsp', 'Edición de clientes', 10, 'param.cliente'),
(13, 'GestionPrivilegios.jsp', 'Gestión Privilegios', 2, NULL),
(14, 'Menu', 'Hoja de Vida', 14, NULL),
(15, 'listaHojaVida.jsp', 'Consultar Hojas de Vida', 14, NULL),
(16, 'Sub-Menu', 'Mis Datos', 14, NULL),
(17, 'DatosPersonales.jsp', 'Datos Personales', 16, NULL),
(18, 'MiHojaDeVida?opc=1', 'Mi Hoja de Vida', 16, NULL),
(19, 'Menu', 'Horas Extras', 19, NULL),
(20, 'HorasExtrasPorCliente.jsp', 'Consultar Por Cliente', 19, NULL),
(21, 'listarAuditoria.jsp', 'Historico', 7, NULL),
(22, 'DatosExperiencia.jsp', 'Experiencia Laboral', 22, NULL),
(23, 'DatosEstudios.jsp', 'Mis Estudios', 23, NULL),
(24, 'InformacionEstudios.jsp', 'Certificados De Estudios', 24, NULL),
(25, 'CargabilidadTotal.jsp', 'Informe Como Empresa', 25, NULL),
(26, 'listarAuditoria.jsp', 'Historico (Logs)', 2, NULL),
(27, 'CargabilidadPorCargo.jsp', 'Informe Por Cargo', 27, NULL),
(28, 'listaHojaVida.jsp', 'Ver sueldo ', 15, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE `auditoria` (
  `Id_Auditoria` int(11) NOT NULL,
  `Id` int(11) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `Nombre_viejo` varchar(60) DEFAULT NULL,
  `Nombre_Nuevo` varchar(60) DEFAULT NULL,
  `Apellido_Viejo` varchar(60) DEFAULT NULL,
  `Apellido_Nuevo` varchar(60) DEFAULT NULL,
  `Observacion_Viejo` varchar(200) DEFAULT NULL,
  `Observacion_Nuevo` varchar(200) DEFAULT NULL,
  `Factura_Viejo` char(2) DEFAULT NULL,
  `Factura_Nuevo` char(2) DEFAULT NULL,
  `Facturacion_Viejo` int(3) DEFAULT NULL,
  `Facturacion_Nuevo` int(3) DEFAULT NULL,
  `Ubicacion_Viejo` varchar(20) DEFAULT NULL,
  `Ubicacion_Nuevo` varchar(20) DEFAULT NULL,
  `Id_Cargo_Viejo` int(11) DEFAULT NULL,
  `Id_Cargo_Nuevo` int(11) DEFAULT NULL,
  `Id_Rol_Viejo` int(11) DEFAULT NULL,
  `Id_Rol_Nuevo` int(11) DEFAULT NULL,
  `Id_Ciudad_Viejo` int(11) DEFAULT NULL,
  `Id_Ciudad_Nuevo` int(11) DEFAULT NULL,
  `Id_Novedad_Viejo` int(11) DEFAULT NULL,
  `Id_Novedad_Nuevo` int(11) DEFAULT NULL,
  `Id_Servicio_Viejo` int(11) DEFAULT NULL,
  `Id_Servicio_Nuevo` int(11) DEFAULT NULL,
  `Id_Cliente_Viejo` int(11) DEFAULT NULL,
  `Id_Cliente_Nuevo` int(11) DEFAULT NULL,
  `Id_Usuario_Viejo` int(11) DEFAULT NULL,
  `Id_Usuario_Nuevo` int(11) DEFAULT NULL,
  `Fecha_Registro` date DEFAULT NULL,
  `TipoNovedad` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auditoria`
--

INSERT INTO `auditoria` (`Id_Auditoria`, `Id`, `action`, `Nombre_viejo`, `Nombre_Nuevo`, `Apellido_Viejo`, `Apellido_Nuevo`, `Observacion_Viejo`, `Observacion_Nuevo`, `Factura_Viejo`, `Factura_Nuevo`, `Facturacion_Viejo`, `Facturacion_Nuevo`, `Ubicacion_Viejo`, `Ubicacion_Nuevo`, `Id_Cargo_Viejo`, `Id_Cargo_Nuevo`, `Id_Rol_Viejo`, `Id_Rol_Nuevo`, `Id_Ciudad_Viejo`, `Id_Ciudad_Nuevo`, `Id_Novedad_Viejo`, `Id_Novedad_Nuevo`, `Id_Servicio_Viejo`, `Id_Servicio_Nuevo`, `Id_Cliente_Viejo`, `Id_Cliente_Nuevo`, `Id_Usuario_Viejo`, `Id_Usuario_Nuevo`, `Fecha_Registro`, `TipoNovedad`) VALUES
(132, 123, '123', '1', '23', '123', '1', '321', '32', '1', '32', 13, 21, '32', '13', 21, 23, 1, 231, 23, 123, 1, 23, 123, 1, 23, 1231, 32, 1231, '2017-08-15', ''),
(133, 28, 'Insertar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, 1, NULL, 934, '2017-08-15', ''),
(134, 29, 'Insertar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 2, NULL, 3, NULL, 925, '2017-08-15', ''),
(135, 30, 'Insertar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, 1, NULL, 188, '2017-08-16', ''),
(136, 28, 'Update', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 934, 934, '2017-08-16', ''),
(137, 28, 'Update', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 1, NULL, NULL, 1, 1, 1, 1, 934, 934, '2017-08-16', ''),
(138, 31, 'Insertar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 2, NULL, 3, NULL, 933, '2017-08-18', ''),
(139, 30, 'Update', NULL, 'Jhon', NULL, 'jhon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 1, 1, 1, 188, 188, '2017-08-18', ''),
(140, 30, 'Update', 'Jhon', 'Jhon', 'jhon', 'jhon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 1, 1, 1, 188, 188, '2017-08-18', ''),
(141, 30, 'Update', 'Jhon', 'Jhon', 'jhon', 'jhon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 1, 1, 1, 188, 188, '2017-08-18', ''),
(142, 30, 'Update', 'Jhon', 'Jhon', 'jhon', 'jhon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 1, 1, 1, 188, 188, '2017-08-18', ''),
(143, 30, 'Update', 'Jhon', 'Jhon', 'jhon', 'jhon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 1, 1, 1, 188, 188, '2017-08-29', ''),
(144, 30, 'Insertar', NULL, 'Juan Alvaro', NULL, 'Hoyos Urrego', NULL, 'No factura', NULL, '1', NULL, NULL, NULL, 'Bogotá D.C', NULL, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 188, '2018-03-28', 'Factura?'),
(145, 30, 'Update', 'Juan Alvaro', 'Juan Alvaro', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 8, 8, 188, 188, '2018-04-03', ''),
(147, 63, 'Update', 'Javier Dario', 'Javier Dario', 'Timote Quezada', 'Timote Quezada', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 18, 2, 2, 1, 1, NULL, NULL, 1, 1, 54, 54, 998, 998, '2018-04-03', ''),
(148, 63, 'Update', 'Javier Dario', 'Javier', 'Timote Quezada', 'Timote Quezada', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 18, 2, 2, 1, 1, NULL, NULL, 1, 1, 54, 54, 998, 998, '2018-04-03', ''),
(149, 30, 'Insertar', NULL, 'Juan Alvaro', NULL, 'Hoyos Urrego', NULL, 'Cambio', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 188, '2018-04-03', 'Cambio cliente'),
(150, 64, 'Update', 'Fabio Oswaldo', 'Fabio Oswaldo', 'Betancourt Buitrago', 'Betancourt Buitrago', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 29, 1, 1, 1, 1, NULL, NULL, 1, 1, 72, 72, 999, 999, '2018-04-03', ''),
(151, 64, 'Update', 'Fabio Oswaldo', 'Fabio Oswaldo', 'Betancourt Buitrago', 'Betancourt Buitrago', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 29, 1, 1, 1, 1, NULL, NULL, 1, 1, 72, 72, 999, 999, '2018-04-03', ''),
(152, 63, 'Update', 'Javier', 'Javier', 'Timote Quezada', 'Timote Quezada', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 18, 2, 2, 1, 1, NULL, NULL, 1, 1, 54, 54, 998, 998, '2018-04-03', ''),
(153, 81, 'Update', 'DIOMAR LIZETH', 'DIOMAR LIZETH', 'VARON GONZALEZ', 'VARON GONZALEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 16, 1, 1, 1, 1, NULL, NULL, 1, 1, 0, 0, 1048, 1048, '2018-04-09', ''),
(154, 81, 'Update', 'DIOMAR LIZETH', 'DIOMAR LIZETH', 'VARON GONZALEZ', 'VARON GONZALEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 16, 1, 1, 1, 1, NULL, NULL, 1, 1, 0, 0, 1048, 1048, '2018-04-09', ''),
(155, 81, 'Update', 'DIOMAR LIZETH', 'DIOMAR LIZETH', 'VARON GONZALEZ', 'VARON GONZALEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 16, 1, 1, 1, 1, NULL, NULL, 1, 1, 0, 0, 1048, 1048, '2018-04-09', ''),
(156, 64, 'Update', 'Fabio Oswaldo', 'Fabio Oswaldo', 'Betancourt Buitrago', 'Betancourt Buitrago', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 29, 1, 1, 1, 1, NULL, NULL, 1, 1, 72, 72, 999, 999, '2018-04-09', ''),
(157, 64, 'Update', 'Fabio Oswaldo', 'Fabio Oswaldo', 'Betancourt Buitrago', 'Betancourt Buitrago', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 29, 1, 1, 1, 1, NULL, NULL, 1, 1, 72, 72, 999, 999, '2018-04-09', ''),
(158, 64, 'Update', 'Fabio Oswaldo', 'Fabio Oswaldo', 'Betancourt Buitrago', 'Betancourt Buitrago', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 29, 1, 1, 1, 1, NULL, NULL, 1, 1, 72, 72, 999, 999, '2018-04-09', ''),
(159, 64, 'Update', 'Fabio Oswaldo', 'Fabio Oswaldo', 'Betancourt Buitrago', 'Betancourt Buitrago', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 29, 1, 1, 1, 1, NULL, NULL, 1, 1, 72, 72, 999, 999, '2018-04-09', ''),
(160, 46, 'Update', 'Andres', 'Andres', 'Ramirez', 'Ramirez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 1, 1, 1, 1, NULL, NULL, 2, 2, 5, 5, 871, 871, '2018-04-09', ''),
(161, 30, 'Update', 'Juan Alvaro', 'Juan Alvaro 2', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 0, 0, 188, 188, '2018-04-27', ''),
(162, 52, 'Insertar', NULL, 'Maria Claudia', NULL, 'Vallejo Rodriguez', NULL, 'novedad de prueva', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 914, '2018-04-27', 'Otras'),
(163, 54, 'Insertar', NULL, 'Lida Yesenia', NULL, 'Salcedo Lizarazo', NULL, 'madfd', NULL, '2', NULL, NULL, NULL, 'MedellÍn', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 908, '2018-04-27', 'Factura?'),
(164, 54, 'Insertar', NULL, 'Lida Yesenia', NULL, 'Salcedo Lizarazo', NULL, 'madfd', NULL, '2', NULL, NULL, NULL, 'MedellÍn', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 908, '2018-04-27', 'Factura?'),
(165, 82, 'Update', 'Gissela ', 'Gissela 2', 'Cardona Cadavid', 'Cardona Cadavid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 3, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 1096, 1096, '2018-04-27', ''),
(166, 30, 'Update', 'Juan Alvaro 2', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 0, 0, 188, 188, '2018-04-27', ''),
(167, 82, 'Update', 'Gissela 2', 'Gissela', 'Cardona Cadavid', 'Cardona Cadavid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 3, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 1096, 1096, '2018-04-30', ''),
(168, 82, 'Update', 'Gissela', 'Gissela', 'Cardona Cadavid', 'Cardona Cadavid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 3, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 1096, 1096, '2018-04-30', ''),
(169, 83, 'Update', 'Jenny Katherine', 'Jenny Katherine', 'Buitrago Martinez', 'Buitrago Martinez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 2, 2, 1, 1, NULL, NULL, 1, 1, 2, 2, 1095, 1095, '2018-04-30', ''),
(170, 83, 'Update', 'Jenny Katherine', 'Jenny Katherine', 'Buitrago Martinez', 'Buitrago Martinez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 2, 2, 1, 1, NULL, NULL, 1, 1, 2, 2, 1095, 1095, '2018-04-30', ''),
(171, 30, 'Insertar', NULL, 'Juan Alvaro ', NULL, 'Hoyos Urrego', NULL, 'trabaja en claro', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, 72, NULL, 188, '2018-05-04', 'Cambio cliente'),
(172, 46, 'Insertar', NULL, 'Andres', NULL, 'Ramirez', NULL, 'claro 2', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 72, NULL, 871, '2018-05-04', 'Cambio cliente'),
(173, 82, 'Update', 'Gissela', 'Gissela2', 'Cardona Cadavid', 'Cardona Cadavid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 3, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 1096, 1096, '2018-05-07', ''),
(174, 85, 'Update', 'Wilman Orlando', 'Wilman Orlando', 'Puentes Diaz', 'Puentes Diaz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 1, 1, 1, 1, NULL, NULL, 1, 1, 0, 0, 989, 989, '2018-05-07', ''),
(175, 82, 'Update', 'Gissela2', 'Gissela', 'Cardona Cadavid', 'Cardona Cadavid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 3, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 1096, 1096, '2018-05-09', ''),
(176, 86, 'Insertar', NULL, 'Liliana Maria', NULL, 'Naranjo Rojas', NULL, 'ni idea we', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 54, '2018-06-28', 'N/A'),
(177, 86, 'Insertar', NULL, 'Liliana Maria', NULL, 'Naranjo Rojas', NULL, 'cambio de clinet', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, 54, '2018-06-29', 'Cambio cliente'),
(178, 86, 'Insertar', NULL, 'Liliana Maria', NULL, 'Naranjo Rojas', NULL, ':v', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 54, '2018-06-29', 'Retiro'),
(179, 86, 'Insertar', NULL, 'Liliana Maria', NULL, 'Naranjo Rojas', NULL, 'esta fecha ingreso :v\r\n', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 54, '2018-06-29', 'Ingreso'),
(180, 86, 'Update', 'Liliana Maria', 'Liliana Maria', 'Naranjo Rojas', 'Naranjo Rojas', NULL, NULL, NULL, 'si', NULL, NULL, NULL, NULL, 10, 10, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 54, 54, '2018-07-03', ''),
(181, 86, 'Update', 'Liliana Maria', 'Liliana Maria', 'Naranjo Rojas', 'Naranjo Rojas', NULL, NULL, 'si', 'si', NULL, NULL, NULL, 'Alkosto call 68', 10, 10, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 54, 54, '2018-07-03', ''),
(182, 86, 'Insertar', NULL, 'Liliana Maria', NULL, 'Naranjo Rojas', NULL, 'Ingreso hoy prueba', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 54, '2018-07-03', 'Ingreso'),
(183, 57, 'Insertar', NULL, 'Julieth Andrea', NULL, 'Diaz Betancourt', NULL, 'primera novedad', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 905, '2018-07-03', 'Ingreso'),
(184, 96, 'Update', 'Julieth Andrea', 'Julieth Andrea', 'Mora Urrego', 'Mora Urrego', NULL, NULL, 'N/', 'No', NULL, NULL, 'av siempre viva', 'av siempre viva', 3, 3, 1, 1, 2, 2, NULL, NULL, 1, 1, 1, 1, 1136, 1136, '2018-07-03', ''),
(185, 96, 'Update', 'Julieth Andrea', 'Julieth Andrea', 'Mora Urrego', 'Mora Urrego', NULL, NULL, 'No', 'No', NULL, NULL, 'av siempre viva', 'av siempre viva', 3, 3, 1, 1, 2, 2, NULL, NULL, 1, 1, 1, 1, 1136, 1136, '2018-07-03', ''),
(186, 33, 'Update', '', '', NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, 'Cra 3843 #kf3', 1, 1, 1, 1, 1, 1, NULL, NULL, 1, 1, 72, 72, 918, 918, '2018-07-10', ''),
(187, 112, 'Update', 'Jhoan Francisco', 'Jhoan Francisco', 'Lopez Rincon', 'Lopez Rincon', NULL, NULL, 'No', 'No', NULL, NULL, 'Cra 3843 #kf3', 'Cra 3843 #kf3', 18, 18, 1, 1, 1, 1, NULL, NULL, 1, 1, 10, 10, 986, 986, '2018-08-01', ''),
(188, 112, 'Update', 'Jhoan Francisco', 'Jhoan Francisco', 'Lopez Rincon', 'Lopez Rincon', NULL, NULL, 'No', 'No', NULL, NULL, 'Cra 3843 #kf3', 'Cra 3843 #kf3', 18, 18, 1, 1, 1, 1, NULL, NULL, 1, 1, 10, 10, 986, 986, '2018-08-01', ''),
(189, 112, 'Update', 'Jhoan Francisco', 'Jhoan Francisco', 'Lopez Rincon', 'Lopez Rincon', NULL, NULL, 'No', 'No', NULL, NULL, 'Cra 3843 #kf3', 'Cra 3843 #kf3', 18, 18, 1, 1, 1, 1, NULL, NULL, 1, 1, 10, 10, 986, 986, '2018-08-01', ''),
(190, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, NULL, 'N/', NULL, NULL, NULL, 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(191, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(192, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(193, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(194, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(195, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(196, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(197, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(198, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(199, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(200, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(201, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(202, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(203, 51, 'Update', 'Juan Carlos', 'Juan Carlos', 'Villarraga Sabogal', 'Villarraga Sabogal', NULL, NULL, NULL, 'N/', NULL, NULL, NULL, 'N/A', 2, 2, 1, 1, 1, 1, NULL, NULL, 1, 1, 72, 72, 915, 915, '2018-08-06', ''),
(204, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(205, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(206, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-06', ''),
(207, 30, 'Update', 'Juan Alvaro ', 'Juan Alvaro ', 'Hoyos Urrego', 'Hoyos Urrego', NULL, NULL, 'N/', 'N/', NULL, NULL, 'N/A', 'N/A', 1, 1, 2, 2, 1, 1, NULL, NULL, 1, 1, 72, 72, 188, 188, '2018-08-08', ''),
(208, 125, 'Insertar', NULL, 'Tania Geraldine', NULL, 'Marin Cruz', NULL, 'cambio prueba', NULL, '0', NULL, NULL, NULL, 'Bogotá D.C', NULL, 16, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 72, NULL, 795, '2018-09-10', 'Cambio cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `Id_Cargo` int(11) NOT NULL,
  `Nombre_Cargo` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`Id_Cargo`, `Nombre_Cargo`) VALUES
(1, 'Gerente de servicios'),
(2, 'Director de Operaciones'),
(3, 'Director Comercial'),
(4, 'Gerente de Innovacion y nuevos negocios'),
(5, 'Gerente Comercial'),
(6, 'Ingeniero de Preventa'),
(7, 'Gerente de Proyectos'),
(8, 'Coordinador Administrativo y Financiero'),
(9, 'Coordinador de Talento Humano'),
(10, 'Coordinador TI'),
(11, 'Coordinador de Calidad'),
(12, 'Coordinador de Pruebas Especializadas'),
(13, 'Coordinador de Pruebas'),
(15, 'Analista de Pruebas Senior'),
(16, 'Analista de Pruebas'),
(18, 'Analista de Pruebas Automatizacion'),
(19, 'Analista de Pruebas Performance'),
(20, 'Analista TI'),
(21, 'Ejecutor de Pruebas'),
(22, 'Asistente Administrativa'),
(23, 'Asistente de Talento Humano'),
(24, 'Asistente Contable'),
(25, 'Secretaria Recepcionista'),
(26, 'Auxiliar TI'),
(27, 'Auxiliar Contable y de Nomina'),
(28, 'Auxiliar de Servicios Generales y Mensajeria'),
(29, 'Practicante TI'),
(30, 'Especialistas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `Id_Ciudad` int(11) NOT NULL,
  `Nombre_Ciudad` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`Id_Ciudad`, `Nombre_Ciudad`) VALUES
(1, 'Bogotá D.C'),
(2, 'MedellÍn'),
(3, 'Cali');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `Id_Cliente` int(11) NOT NULL,
  `Gerente_Proyecto` varchar(60) NOT NULL,
  `Nombre_Cliente` varchar(45) NOT NULL,
  `Hora_Inicio` time DEFAULT NULL,
  `Hora_Fin` time DEFAULT NULL,
  `Horas_Laboradas` float NOT NULL,
  `nombreAbreviado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`Id_Cliente`, `Gerente_Proyecto`, `Nombre_Cliente`, `Hora_Inicio`, `Hora_Fin`, `Horas_Laboradas`, `nombreAbreviado`) VALUES
(0, '?', 'Comercial', '00:00:00', '00:00:00', 9, 'COMERCIAL'),
(1, 'Pedro', 'Software Quality Assurance S.A', '07:30:00', '06:00:00', 9.5, 'Software Quality Assurance S.A'),
(2, 'Camilo', 'Trébol Software - AXEDE', '08:00:00', '05:30:00', 9.5, 'AXEDE'),
(3, 'Marisol', 'Intergrupo', '00:00:00', '01:00:00', 9.5, 'Intergrupo'),
(4, 'Adriana', 'Ingeneo', '07:30:01', '06:00:00', 9, 'Ingeneo'),
(5, 'Hugo', 'Bancolombia', '07:30:00', '19:30:00', 9.5, 'Bancolombia'),
(6, 'Samuel', 'Pragma', '00:00:00', '00:00:00', 9, 'PRAGMA'),
(7, 'TREW', 'Vestimundo', '14:13:00', '03:11:00', 0, 'Vestimundo'),
(8, 'JHON', 'GS1', '00:00:00', '00:00:00', 9, 'GRUPO URIBE'),
(9, '-', 'Consorcio TM', '00:00:00', '00:00:00', 9, 'CARVAJAL S.A'),
(10, '?', 'ESPECIALISTAS', '00:00:00', '00:00:00', 9, 'ESPECIALISTAS'),
(13, '?', 'SERFINCO', '07:30:00', '17:30:00', 9, 'SERFINCO'),
(23, '?', 'XM', '07:30:00', '17:30:00', 9, 'XM'),
(26, '?', 'EXITO', '07:30:00', '17:30:00', 9, 'EXITO'),
(41, '?', 'COMPENSAR', '07:30:00', '17:30:00', 9, 'COMPENSAR'),
(53, '?', 'TODO 1', '07:30:00', '17:30:00', 9, 'TODO 1'),
(54, 'SQASA', 'Operaciones', '00:00:00', '00:00:00', 9, 'OPERACIONES'),
(72, 'Gabriel Gonzales', 'Claro', '07:30:00', '18:00:00', 9.5, 'Claro'),
(75, '?', 'SERVIENTREGA', '07:30:00', '17:30:00', 9, 'SERVIENTREGA'),
(78, '?', 'COMPUREDES', '07:30:00', '17:30:00', 9, 'COMPUREDES'),
(80, '?', 'ALKOSTO', '00:00:00', '00:00:00', 9, 'ALKOSTO'),
(82, '?', 'QUIPUX', '07:30:00', '17:30:00', 9, 'QUIPUX'),
(84, 'Marisol ', 'A Toda Hora S.A', '00:00:00', '00:00:00', 9.5, 'ATH'),
(85, '?', 'UNE', '07:30:00', '17:30:00', 9, 'UNE'),
(88, '?', 'AXEDE', '07:30:00', '05:30:00', 9, 'AXEDE'),
(89, 'Erika sanchez ', 'Colsubsidio', '00:00:00', '00:00:00', 9, 'COLSUBSIDIO'),
(94, '?', 'RAPP', '07:30:00', '17:30:00', 9, 'RAPP'),
(99, '?', 'COOMEVA', '07:30:00', '17:30:00', 9, 'COOMEVA'),
(101, '?', 'EXPERIAN', '07:30:00', '17:30:00', 9, 'EXPERIAN'),
(102, '?', 'SETI', '07:30:00', '17:30:00', 9, 'SETI'),
(105, '?', 'LEONISA', '07:30:00', '17:30:00', 9, 'LEONISA'),
(127, '?', 'UNISYS MEDELLIN', '07:30:00', '17:30:00', 9, 'UNISYS MEDELLIN'),
(207, '?', 'UNISYS BOGOTA', '07:30:00', '17:30:00', 9, 'UNISYS BOGOTA'),
(269, '?', 'CARVAJAL S.A', '07:30:00', '05:30:00', 9, 'CARVAJAL S.A'),
(335, 'Jota Jota', 'Dinario Ltda', '00:00:00', '00:00:00', 9.5, 'Dinario Ltda'),
(353, 'Hector Jaime Páez', 'EMCALI', '07:30:00', '17:30:00', 9, 'EMCALI'),
(356, 'EEUU', 'Click Software', '00:00:00', '00:00:00', 9.5, 'Click Software'),
(357, 'Jorge', 'Ubiquo Telemedicina', '00:00:00', '00:00:00', 8.5, 'Ubiquo Telemedicina'),
(358, 'Liliana', 'Davivienda', '00:00:00', '00:00:00', 9.5, 'Davivienda'),
(361, '?', 'COGNOS', '07:30:00', '17:30:00', 9, 'COGNOS'),
(362, 'Luz', 'Stefanini', '00:00:00', '00:00:00', 8, 'Stefanini'),
(363, 'Pedro', 'Arus', '06:00:00', '13:00:00', 8, 'Arus'),
(364, 'Fran Kafka', 'Sasa Consultoria', '08:08:00', '19:05:00', 9, 'Sasa Consultoria'),
(365, 'Andres Caicedo', 'Premex', '08:30:00', '05:30:00', 8, 'Premex'),
(366, 'Sugar', 'SUGAR', '01:00:00', '01:00:00', 10, 'SUGAR'),
(367, 'David', 'Bancompartir', '10:02:00', '11:00:00', 10, 'Bancompartir'),
(368, 'Fernando ', 'Falabella', '08:08:00', '21:30:00', 8, 'Falabella'),
(369, '?', 'DIGIMARKETING', '07:30:00', '17:30:00', 9, 'DIGIMARKETING'),
(370, '?', 'DECOWRAPS', '07:30:00', '17:30:00', 9, 'DECOWRAPS'),
(372, '?', 'COLTEFINANCIERA', '07:30:00', '17:30:00', 9, 'COLTEFINANCIERA'),
(373, '?', 'GRUPO AVAL', '07:30:00', '17:30:00', 9, 'GRUPO AVAL'),
(374, '?', 'PUNTOS COLOMBIA', '07:30:00', '17:30:00', 9, 'PUNTOS COLOMBIA'),
(375, '?', 'GRUPO URIBE', '07:30:00', '17:30:00', 9, 'GRUPO URIBE'),
(376, '?', 'PLACE TO PAY', '07:30:00', '17:30:00', 9, 'PLACE TO PAY');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_analista`
--

CREATE TABLE `datos_analista` (
  `Id_Datos` int(11) NOT NULL,
  `Id_Cargo` int(11) DEFAULT NULL,
  `Id_Rol` int(11) DEFAULT NULL,
  `Id_Ciudad` int(11) DEFAULT NULL,
  `Id_Servicio` int(11) DEFAULT NULL,
  `Id_Cliente` int(11) DEFAULT NULL,
  `Id_Usuario` int(11) DEFAULT NULL,
  `Nombres` varchar(60) NOT NULL,
  `Apellidos` varchar(60) DEFAULT NULL,
  `Direccion` varchar(50) DEFAULT NULL,
  `Telefono` varchar(10) DEFAULT NULL,
  `Celular` varchar(15) DEFAULT NULL,
  `Ruta_Anexos` varchar(250) DEFAULT NULL,
  `Observaciones` varchar(200) DEFAULT NULL,
  `Factura` char(2) DEFAULT NULL,
  `Facturacion` int(4) DEFAULT NULL,
  `Ubicacion` varchar(20) DEFAULT NULL,
  `Fecha_Registro` datetime DEFAULT CURRENT_TIMESTAMP,
  `Salario` varchar(15) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datos_analista`
--

INSERT INTO `datos_analista` (`Id_Datos`, `Id_Cargo`, `Id_Rol`, `Id_Ciudad`, `Id_Servicio`, `Id_Cliente`, `Id_Usuario`, `Nombres`, `Apellidos`, `Direccion`, `Telefono`, `Celular`, `Ruta_Anexos`, `Observaciones`, `Factura`, `Facturacion`, `Ubicacion`, `Fecha_Registro`, `Salario`) VALUES
(28, 1, 1, 2, 1, 1, 934, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-15 10:47:26', '0'),
(29, 1, 1, 2, 1, 72, 925, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-15 11:17:01', '0'),
(30, 1, 2, 1, 1, 72, 188, 'Juan Alvaro ', 'Hoyos Urrego', 'calle 95 n 1-52', '564564', '3210774515', 'https://christmo99.wordpress.com/2009/05/26/conexion-a-servidor-ldap-desde-java/', NULL, 'N/', NULL, 'N/A', '2017-08-16 08:17:11', '10.000 '),
(31, 1, 1, 2, 1, 1, 933, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-18 10:37:32', '0'),
(32, 1, 1, 2, 1, 72, 932, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-01 11:46:25', '0'),
(33, 1, 1, 1, 1, 72, 918, '', NULL, NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvG', NULL, 'No', NULL, 'Cra 3843 #kf3', '2017-09-05 08:29:54', '0'),
(34, 1, 1, 1, 1, 72, 917, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-06 14:29:08', '0'),
(35, 1, 1, 1, 1, 72, 919, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-07 12:10:47', '0'),
(45, 1, 1, 1, 1, 72, 773, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-18 10:54:42', '0'),
(46, 2, 1, 1, 2, 72, 871, 'Andres', 'Ramirez', 'Cra 105 n 184 a-551', NULL, '3002175454', NULL, NULL, NULL, NULL, NULL, '2017-09-21 09:23:37', '0'),
(47, 2, 1, 2, 1, 1, 921, 'Paula', 'Hoyos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-21 09:35:08', '0'),
(51, 2, 1, 1, 1, 72, 915, 'Juan Carlos', 'Villarraga Sabogal', NULL, NULL, NULL, 'N/A', NULL, 'N/', NULL, 'N/A', '2017-09-21 13:51:53', '0'),
(52, 1, 1, 1, 1, 6, 914, 'Maria Claudia', 'Vallejo Rodriguez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 15:46:59', '0'),
(53, 2, 1, 1, 1, 367, 909, 'Maria Camila', 'Quijano Caranton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 10:42:07', '0'),
(54, 1, 1, 2, 1, 367, 908, 'Lida Yesenia', 'Salcedo Lizarazo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 14:50:49', '0'),
(55, 1, 1, 1, 1, 366, 906, 'Rafael Antonio', 'Villa Cabarcas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 15:12:36', '0'),
(56, 1, 1, 1, 1, 366, 907, 'CARLOS FERNANDO', 'VARGAS VELEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 15:22:00', '0'),
(57, 1, 1, 1, 1, 363, 905, 'Julieth Andrea', 'Diaz Betancourt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-05 15:24:39', '0'),
(58, 1, 1, 1, 1, 72, 797, 'Daniel Steven', 'Gomez Gaitan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-28 13:54:54', '0'),
(59, 1, 1, 1, 1, 1, 1039, 'Albeiro Jose', 'Suarez Ramos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-18 14:16:45', '0'),
(60, 3, 1, 1, 1, 3, 937, 'David Andres', 'Daza Diaz', NULL, NULL, NULL, 'https://chromedriver.storage.googleapis.com/index.html?path=2.37/', NULL, NULL, NULL, NULL, '2018-01-26 14:03:06', '0'),
(61, 1, 1, 1, 1, 1, 1038, 'Andres Felipe', 'Rincon Mejia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:41:54', '0'),
(62, 1, 2, 1, 1, 5, 1036, 'Brandon Daniel', 'Ortega Saavedra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 11:47:08', '0'),
(63, 18, 2, 1, 1, 101, 998, 'Javier', 'Timote Quezada', 'cRA A B -C', '2733828', '3158975776', 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-02-05 17:10:55', '0'),
(64, 29, 1, 1, 1, 72, 999, 'Fabio Oswaldo', 'Betancourt Buitrago', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-02-22 15:40:19', '0'),
(65, 29, 1, 1, 1, 72, 1034, 'JOHN ANDERSON', 'DUQUE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-23 15:10:21', '0'),
(66, NULL, NULL, NULL, NULL, NULL, 1026, 'JUAN FELIPE ', 'GAVIRIA PABON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 16:43:25', '0'),
(67, NULL, NULL, NULL, NULL, NULL, 981, 'Ivan Dario', 'Riobueno Chaparro', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 16:43:35', '0'),
(68, NULL, NULL, NULL, NULL, NULL, 949, 'Julian David', 'Rodriguez Hoyos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 16:44:08', '0'),
(69, 18, 1, 1, 1, 10, 1037, 'Carlos Mateo', 'Leal Tocasuche', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-01 15:34:32', '0'),
(70, NULL, NULL, NULL, NULL, NULL, 943, 'Ana Milena', 'Lizarazo Farfan', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-03-07 16:23:33', '0'),
(71, NULL, NULL, NULL, NULL, NULL, 930, 'Jefferson Ricardo', 'Roncancio Melo', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '2018-03-07 16:30:04', '0'),
(72, NULL, NULL, NULL, NULL, NULL, 942, 'Yesid David', 'Ramirez Espinosa', NULL, NULL, NULL, 'www.google.com', NULL, NULL, NULL, NULL, '2018-03-07 16:31:10', '0'),
(73, NULL, NULL, NULL, NULL, NULL, 929, 'Juan Javier', 'Diaz Atuesta', NULL, NULL, NULL, 'www.google.com', NULL, NULL, NULL, NULL, '2018-03-07 16:35:24', '0'),
(74, 21, 2, 1, 1, 84, 948, 'Yeramy Nailett', 'Gonzalez De Cardona', NULL, NULL, NULL, 'www.google.com', NULL, NULL, NULL, NULL, '2018-03-16 11:51:12', '0'),
(75, 16, 1, 1, 1, 72, 1062, 'Juan Manuel', 'Puyo Casas', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1ynylz1m-j-rCx7AEgAqV5ms4vKHTEe6n', NULL, NULL, NULL, NULL, '2018-03-22 16:07:51', '0'),
(76, 16, 1, 1, 1, 6, 1061, 'NATALI', 'MONSALVE RAMIREZ', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-03-23 09:17:47', '0'),
(77, NULL, NULL, NULL, NULL, NULL, 1060, 'SANTIAGO', 'BOHORQUEZ ALZATE', NULL, NULL, NULL, 'www.google.com', NULL, NULL, NULL, NULL, '2018-03-26 10:15:10', '0'),
(78, NULL, NULL, NULL, NULL, NULL, 1059, 'Milton Rene', 'Rodriguez Ramirez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-26 11:19:24', '0'),
(79, 16, 1, 1, 1, 0, 1058, 'German Alberto', 'Suarez Quintero', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-04-03 11:13:00', '0'),
(80, 16, 1, 1, 1, 0, 1052, 'SANDRA YANETH', 'OSORIO BULLA', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-04-06 17:02:55', '0'),
(81, 16, 1, 1, 1, 0, 1048, 'DIOMAR LIZETH', 'VARON GONZALEZ', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-04-09 09:16:33', '0'),
(82, 3, 1, 1, 1, 1, 1096, 'Gissela', 'Cardona Cadavid', 'null', 'null', 'null', 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-04-26 16:53:26', '0'),
(83, 5, 2, 1, 1, 2, 1095, 'Jenny Katherine', 'Buitrago Martinez', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-04-30 09:44:25', '0'),
(84, 2, 1, 3, 2, 2, 1094, 'Fernando', 'Alvarez Ariza', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-04-30 13:42:02', '0'),
(85, 2, 1, 1, 1, 0, 989, 'Wilman Orlando', 'Puentes Diaz', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-05-07 08:48:44', '0'),
(86, 10, 1, 1, 1, 1, 54, 'Liliana Maria', 'Naranjo Rojas', NULL, NULL, NULL, 'kjmkjmj', NULL, 'si', NULL, 'Alkosto call 68', '2018-05-07 14:09:27', '0'),
(87, 2, 2, 2, 1, 72, 609, 'Yiro Enrique', 'Camargo Vargas', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-05-07 14:39:28', '0'),
(88, NULL, NULL, NULL, NULL, NULL, 792, 'Keyla Valentina', 'Garcia Castaño', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-05-07 14:41:59', '0'),
(89, 4, 2, 3, 2, 53, 853, 'Julio Cesar', 'Espinal Chica', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-05-07 14:46:58', '0'),
(90, 16, 1, 1, 1, 7, 1093, 'Ferney', 'Diaz Puentes', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-05-09 14:27:15', '0'),
(91, NULL, NULL, NULL, NULL, NULL, 1092, 'Diego Alexander', 'Sierra Sepulveda', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-05-09 14:40:51', '0'),
(92, NULL, NULL, NULL, NULL, NULL, 1057, 'Johanna', 'Botero Polania', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-05-29 16:28:01', '0'),
(93, NULL, NULL, NULL, NULL, NULL, 1111, 'Miguel Angel', 'Plazas Gonzalez', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-05-29 16:29:47', '0'),
(94, 16, 1, 1, 1, 72, 931, 'Maria Alejandra', 'Sanchez Contreras', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-06-22 16:48:29', '0'),
(95, NULL, NULL, NULL, NULL, NULL, 1137, 'Monica Liliana', 'Avendaño', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, NULL, NULL, NULL, '2018-07-03 12:38:18', '0'),
(96, 3, 1, 2, 1, 1, 1136, 'Julieth Andrea', 'Mora Urrego', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'av siempre viva', '2018-07-03 12:49:04', '0'),
(97, 21, 1, 1, 1, 72, 59, 'Lina', 'Maria Cardona', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'si', NULL, 'Cra 3843 #kf3', '2018-07-10 09:30:14', '1000000'),
(98, 21, 1, 1, 1, 72, 1021, 'Juan Manuel', 'Villarraga Garzon', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 09:44:29', '0'),
(99, 21, 1, 1, 1, 72, 959, 'Leidy Brillith ', 'Castro Alméciga', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 09:53:42', '0'),
(100, 21, 1, 1, 1, 72, 222, 'Isabel Cristina', 'Coronel Segrera', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 09:57:39', '0'),
(101, 21, 1, 1, 1, 101, 613, 'Yeimmy Marisol', 'Polo Antonio', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 10:53:34', '0'),
(102, 21, 1, 1, 1, 85, 916, 'BRIANDA', 'CASTILLO LUNA', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 11:09:37', '0'),
(103, 21, 1, 1, 1, 85, 404, 'Deisy Johana', 'Marín Florez', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 11:18:27', '0'),
(104, 21, 1, 1, 1, 72, 1031, 'GUSTAVO', 'LOPEZ LOZANO', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 11:31:25', '0'),
(105, 29, 1, 1, 1, 72, 1081, 'YAREMIS YAJAIRA', 'MARTINEZ COGOLLO', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 11:36:48', '0'),
(106, 21, 1, 1, 1, 10, 176, 'Esteffany Alejandra', 'Garces Restrepo', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 11:51:59', '0'),
(107, 18, 1, 1, 1, 10, 846, 'Gelber Gabriel', 'Ospina Bello', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 14:10:21', '0'),
(108, 18, 2, 1, 1, 10, 813, 'Diana Carolina', 'Moreno Mejia', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 14:21:18', '0'),
(109, 21, 1, 1, 1, 10, 528, 'Juan Elias', 'Loaiza Valencia', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 14:32:56', '0'),
(110, 19, 1, 1, 1, 10, 940, 'Jhon Jairo', 'Muñoz Mateus', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 14:37:58', '0'),
(111, 18, 1, 1, 1, 10, 1019, 'Anderson David', 'Hernandez Farias', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 14:41:32', '0'),
(112, 18, 1, 1, 1, 10, 986, 'Jhoan Francisco', 'Lopez Rincon', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 14:44:11', '0'),
(113, 18, 1, 1, 1, 10, 1050, 'cristian', 'Bustamante', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 15:15:20', '0'),
(114, NULL, NULL, NULL, NULL, NULL, 944, 'Andres ', 'Guerra', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 15:24:07', '0'),
(115, NULL, NULL, NULL, NULL, NULL, 728, 'Juan Fernando', 'Muñoz Urrego', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Cra 3843 #kf3', '2018-07-10 17:11:12', '0'),
(116, NULL, NULL, NULL, NULL, NULL, 1152, 'Isbelia', 'Mejía Chavarriaga', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'N/', NULL, 'av 68 alkosta :v fal', '2018-07-31 12:00:54', NULL),
(117, NULL, NULL, NULL, NULL, NULL, 1151, 'ANGIE NATALIA', 'GOMEZ LUNA', NULL, NULL, NULL, 'https://www.youtube.com/watch?v=IakDItZ7f7Q', NULL, 'Si', NULL, 'Prueba Probadora', '2018-07-31 12:24:44', '0'),
(118, NULL, NULL, NULL, NULL, NULL, 1150, 'Milena', 'Castañeda Orjuela ', NULL, NULL, NULL, 'https://www.youtube.com/watch?v=IakDItZ7f7Q', NULL, 'Si', NULL, 'Prueba Probadora', '2018-07-31 16:39:41', '0'),
(119, NULL, NULL, NULL, NULL, NULL, 1149, 'Luis Alejandro', 'Diaz Montenegro', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'xxxx prueba xxxx', '2018-07-31 16:42:21', '0'),
(120, NULL, NULL, NULL, NULL, NULL, 1148, 'Oscar Miguel', 'Delgadillo Rodriguez', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'av 68 alkosta :v fal', '2018-07-31 16:57:02', '0'),
(121, NULL, NULL, NULL, NULL, NULL, 1147, 'Capacitacion_5', 'Capacitacion_5', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Prueba Probadora', '2018-07-31 17:27:42', '0'),
(122, NULL, NULL, NULL, NULL, NULL, 1146, 'Capacitacion_4', 'Capacitacion_4', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'Nechi prueba', '2018-08-01 12:25:47', '0'),
(123, NULL, NULL, NULL, NULL, NULL, 1145, 'Capacitacion_3', 'Capacitacion_3', NULL, NULL, NULL, 'https://drive.google.com/drive/folders/1_UTwWTm3gXz0JCMJUvlAHysx3RJdFvGX', NULL, 'No', NULL, 'av 68 alkosta :v fal', '2018-08-01 17:05:49', 'funciona :c'),
(124, NULL, NULL, NULL, NULL, NULL, 1144, 'Capacitacion_2', 'Capacitacion_2', NULL, NULL, NULL, 'wdsd', NULL, 'no', NULL, 'no', '2018-08-03 11:42:37', '1.001'),
(125, 16, 1, 1, 1, 72, 795, 'Tania Geraldine', 'Marin Cruz', NULL, NULL, NULL, 'N/A', NULL, 'Si', NULL, 'indefinido', '2018-09-10 11:52:32', '781.241'),
(126, NULL, NULL, NULL, NULL, NULL, 1194, 'MILENA', 'ARBOLEDA GOMEZ', NULL, NULL, NULL, 'N/A', NULL, 'No', NULL, 'N/A', '2018-09-19 11:15:44', '0'),
(127, 21, 1, 1, 1, 89, 1180, 'SOL ANYI TERESA', 'FUQUEN CALDERON', NULL, NULL, NULL, 'N/A', NULL, 'Si', NULL, 'N/A', '2018-09-19 16:01:34', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_analista_novedad`
--

CREATE TABLE `datos_analista_novedad` (
  `id_datos_analista_novedad` int(11) NOT NULL,
  `novedad_Id_Novedad` int(11) NOT NULL,
  `datos_analista_Id_Datos` int(11) NOT NULL,
  `cliente_Id_Cliente` int(11) DEFAULT NULL,
  `EstadoNovedad` tinyint(2) NOT NULL,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `Descripcion` text,
  `Factura` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datos_analista_novedad`
--

INSERT INTO `datos_analista_novedad` (`id_datos_analista_novedad`, `novedad_Id_Novedad`, `datos_analista_Id_Datos`, `cliente_Id_Cliente`, `EstadoNovedad`, `fecha`, `Descripcion`, `Factura`) VALUES
(2, 2, 29, NULL, 0, '2017-08-18 00:00:00', 'dssdgsdgaasgd', NULL),
(12, 1, 28, 2, 0, '2017-08-18 08:43:01', 'ojjj', NULL),
(13, 1, 28, 2, 0, '2017-08-18 08:43:10', 'popoop', NULL),
(14, 3, 29, NULL, 0, '2017-08-18 08:43:30', 'El usuario no ha vuelto', NULL),
(15, 3, 29, NULL, 0, '2017-08-18 08:44:12', 'No aplica', NULL),
(17, 2, 31, 2, 1, '2017-08-09 00:00:00', 'sdafasd', NULL),
(19, 1, 34, 1, 0, '2017-09-07 11:51:46', 'Cambio', NULL),
(20, 1, 34, 1, 0, '2017-09-07 11:52:55', 'Claro', NULL),
(22, 1, 34, 1, 0, '2017-09-07 12:04:25', 'a', NULL),
(25, 1, 28, 1, 0, '2017-09-07 12:10:17', '1', NULL),
(26, 1, 28, 3, 0, '2017-09-07 17:11:24', 'Cambio de cliente para apoyo', NULL),
(27, 1, 28, 4, 0, '2017-09-07 17:12:03', 'd', NULL),
(28, 1, 28, 1, 1, '2017-09-07 17:12:48', 'Claro', NULL),
(31, 1, 34, 72, 1, '2017-09-12 14:24:19', 'hb', NULL),
(33, 1, 33, 72, 1, '2017-09-15 10:38:57', 'Cambio de client ', NULL),
(35, 1, 35, 72, 1, '2017-09-15 16:32:56', 'n', NULL),
(36, 1, 29, 72, 0, '2017-09-15 16:35:03', 'nnh', NULL),
(37, 1, 32, 72, 0, '2017-09-15 16:41:22', '.l', NULL),
(38, 1, 29, 72, 1, '2017-09-15 16:41:22', 'nnh', NULL),
(39, 1, 32, 72, 0, '2017-09-15 16:43:13', 'ghh', NULL),
(40, 1, 32, 72, 0, '2017-09-15 16:47:21', 'lk', NULL),
(41, 1, 32, 72, 0, '2017-09-15 16:47:45', 'l', NULL),
(42, 1, 32, 72, 1, '2017-09-15 16:51:01', 'gfgf', NULL),
(43, 3, 45, NULL, 0, '2017-01-05 10:55:04', 'Hola ', NULL),
(44, 2, 45, NULL, 0, '2017-01-14 00:00:00', 'Mm', NULL),
(46, 1, 45, NULL, 0, '2017-08-23 00:00:00', 'hthyhy', NULL),
(47, 1, 45, NULL, 0, '2017-09-12 00:00:00', 'yyyyy', NULL),
(84, 1, 30, 4, 0, '2017-08-19 00:00:00', 'Cambio cliente', NULL),
(85, 2, 30, NULL, 0, '2017-09-25 10:38:05', 'Renuncia', NULL),
(86, 3, 30, NULL, 0, '2017-09-25 10:38:20', 'Otras novedades ', NULL),
(87, 1, 30, 2, 0, '2017-09-12 00:00:00', '55', NULL),
(88, 1, 30, 72, 0, '2017-09-25 00:00:00', '555', NULL),
(89, 1, 30, 1, 0, '2017-10-03 00:00:00', 'k', NULL),
(90, 1, 30, 2, 0, '2017-10-03 00:00:00', 'pÃ±Ã±pÃ±p', NULL),
(91, 1, 30, 1, 0, '2018-02-07 00:00:00', 'Cambio', NULL),
(92, 4, 30, NULL, 0, '2018-02-06 16:27:22', 'Prueba de si factura o no', NULL),
(93, 4, 30, NULL, 0, '2018-02-06 16:34:28', 'Prueba de si el usuario factura o no', NULL),
(94, 4, 30, NULL, 0, '2018-02-06 16:35:43', 'Prueba de que si el usuario factura o no', 1),
(95, 4, 30, NULL, 0, '2018-02-06 16:36:40', 'Prueba de que si el usuario factura o no', 1),
(96, 4, 30, NULL, 0, '2018-02-06 16:38:22', 'Prueba de que si el usuario factura o no', 1),
(97, 4, 30, NULL, 0, '2018-02-06 16:55:38', 'Prueba si factura o no', 1),
(98, 4, 30, NULL, 0, '2018-02-06 16:57:04', 'Prueba si factura o no', 1),
(99, 4, 30, NULL, 0, '2018-02-06 17:04:01', 'Prueba si factura o no', 1),
(100, 4, 30, NULL, 0, '2018-02-06 17:06:26', 'dasd', 1),
(101, 4, 53, NULL, 0, '2018-02-06 17:20:58', 'Prueba de si factura o no', 2),
(102, 4, 53, NULL, 1, '2018-02-06 17:21:51', 'Prueba de si factura o no', 1),
(103, 4, 45, NULL, 0, '2018-02-07 08:56:42', 'El analista no facturara ya que se llego a un acuerdo con el cliente', 1),
(104, 4, 45, NULL, 0, '2018-02-07 08:57:59', 'El analista no facturara ya que se llego a un acuerdo con el cliente', 1),
(105, 4, 45, NULL, 1, '2018-02-07 09:05:10', 'El analista pasa a facturaciÃ³n ya que el acuerdo con el cliente no se realizo.', 2),
(106, 4, 30, NULL, 0, '2018-02-19 09:23:13', 'Se acordÃ³ con cliente.', 1),
(107, 1, 64, 72, 1, '2018-02-23 00:00:00', 'esafdasd', 0),
(108, 1, 30, 72, 0, '2018-03-22 00:00:00', 'Cambio', 0),
(109, 1, 75, 72, 1, '2018-03-22 00:00:00', 'Cambio', 0),
(110, 1, 30, 8, 0, '2018-03-23 00:00:00', 'Cambio cliente por finalización proyecto', 0),
(111, 4, 30, NULL, 0, '2018-03-28 10:35:16', 'No factura', 1),
(112, 1, 30, NULL, 0, '2018-04-03 11:26:56', 'Cambio', 0),
(113, 1, 63, 101, 1, '2018-04-26 00:00:00', 'Cambio Cliente', 0),
(114, 3, 52, NULL, 1, '2018-04-27 10:55:39', 'novedad de prueva', 0),
(115, 4, 54, NULL, 0, '2018-04-27 10:59:54', 'madfd', 2),
(116, 4, 54, NULL, 1, '2018-04-27 10:59:54', 'madfd', 2),
(117, 1, 30, 72, 1, '2018-05-04 00:00:00', 'trabaja en claro', 0),
(118, 1, 46, 72, 1, '2018-05-03 00:00:00', 'claro 2', 0),
(119, 5, 86, NULL, 0, '2018-06-28 11:55:20', 'ni idea we', 0),
(120, 1, 86, 1, 0, '2018-03-15 00:00:00', 'cambio de clinet', 0),
(121, 2, 86, NULL, 0, '2018-06-29 16:13:35', ':v', 0),
(122, 4, 86, NULL, 0, '2018-06-29 17:05:30', 'esta fecha ingreso :v\r\n', 0),
(123, 4, 86, NULL, 1, '2018-07-03 12:40:59', 'Ingreso hoy prueba', 0),
(124, 4, 57, NULL, 1, '2018-07-03 15:43:52', 'primera novedad', 0),
(125, 1, 125, 72, 1, '2018-09-10 00:00:00', 'cambio prueba', 0),
(126, 1, 97, 72, 1, '2018-08-02 00:00:00', 'prueba', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id_deaprtamento` int(10) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id_deaprtamento`, `nombre`) VALUES
(1, 'Amazonas'),
(2, 'Antioquia'),
(3, 'Arauca'),
(4, 'Atlántico'),
(5, 'Bolívar'),
(6, 'Boyacá'),
(7, 'Caldas'),
(8, 'Caquetá'),
(9, 'Casanare'),
(10, 'Cauca'),
(11, 'Cesar'),
(12, 'Chocó'),
(13, 'Córdoba'),
(14, 'Cundinamarca'),
(15, 'Güainia'),
(16, 'Guaviare'),
(17, 'Huila'),
(18, 'La Guajira'),
(19, 'Magdalena'),
(20, 'Meta'),
(21, 'Nariño'),
(22, 'Norte de Santander'),
(23, 'Putumayo'),
(24, 'Quindio'),
(25, 'Risaralda'),
(26, 'San Andrés y Providencia'),
(27, 'Santander'),
(28, 'Sucre'),
(29, 'Tolima'),
(30, 'Valle del Cauca'),
(31, 'Vaupés'),
(32, 'Vichada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_horas`
--

CREATE TABLE `detalle_horas` (
  `Id_Detalle_Horas` int(11) NOT NULL,
  `Id_Datos` int(11) NOT NULL,
  `Id_Horas_Extras` int(11) NOT NULL,
  `Id_Tipo_Pago` int(11) NOT NULL,
  `CantidadHoras` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_horas`
--

INSERT INTO `detalle_horas` (`Id_Detalle_Horas`, `Id_Datos`, `Id_Horas_Extras`, `Id_Tipo_Pago`, `CantidadHoras`) VALUES
(140, 63, 503390, 3, 3),
(142, 63, 497427, 1, 0.5),
(144, 63, 497427, 4, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudio`
--

CREATE TABLE `estudio` (
  `Id_Estudio` int(11) NOT NULL,
  `Id_Datos` int(11) NOT NULL,
  `Id_Tipo_Estudio` int(11) NOT NULL,
  `Titulo` varchar(100) NOT NULL,
  `Institucion` varchar(200) NOT NULL,
  `Fecha_Inicio` varchar(12) DEFAULT NULL,
  `Fecha_Fin` varchar(12) DEFAULT NULL,
  `Observaciones` varchar(100) DEFAULT NULL,
  `Tarjeta_Profesional` varchar(30) DEFAULT NULL,
  `Nivel` varchar(11) DEFAULT NULL,
  `Ciudad` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudio`
--

INSERT INTO `estudio` (`Id_Estudio`, `Id_Datos`, `Id_Tipo_Estudio`, `Titulo`, `Institucion`, `Fecha_Inicio`, `Fecha_Fin`, `Observaciones`, `Tarjeta_Profesional`, `Nivel`, `Ciudad`) VALUES
(93, 30, 1, '', 'Mi idioma 1', '2014/02/05', '2017/02/05', NULL, NULL, '1', 'USA'),
(94, 30, 1, 'Ingeniero de sistemas', 'U. Nacional ', '2014/01/01', '2017/09/29', 'N/A', '0123456789', '0', 'Bogota D.C'),
(96, 30, 2, 'Desarrollo ', 'Certificacion ', '2018/02/05', '2018/02/15', 'Observaciones estudios ', '1234567890', '0', NULL),
(97, 30, 1, 'Tele Comunicaciones', 'U. ECCI', '2015/03/04', NULL, NULL, NULL, '2', 'rrrrrr'),
(98, 30, 1, 'Ingeniero de Software', 'IMPAU', '2018/02/01', '2018/02/01', 'Finalizada', 'dfsadsf', '0', 'Bogota D.C'),
(99, 30, 1, 'Administrador Base de Datos', 'U ANDES', '2018/02/01', '2018/02/03', 'Finalizada', '123123123', '0', NULL),
(100, 30, 9, '', 'Mi idioma 1aas', NULL, NULL, NULL, NULL, '5', 'Bogota colombia'),
(101, 46, 1, 'Ingeniero de sistemas', 'U Pedagogica', '2017/09/01', '2018/02/02', 'N/A', '15151515', '0', NULL),
(102, 46, 9, '', 'Duolingo', NULL, NULL, NULL, NULL, '5', 'BogotÃ¡ DC'),
(103, 46, 1, 'Maestria', 'Microsoft', '2018/02/06', '2018/02/15', 'Scrum Master', '15151515', '0', NULL),
(104, 46, 1, 'Desarrolo', 'Insat', '2017/12/01', '2018/02/01', 'N', '121323422', '0', NULL),
(105, 46, 9, '', 'Duolinguo', NULL, NULL, NULL, NULL, '11', 'BogotÃ¡'),
(106, 63, 4, 'Analisis y Desarrollo de Sistemas de Informacion', 'SENA', '2016/04/11', '2018/04/11', 'ADSI', 'N/A', '0', 'Bogota D.C'),
(107, 63, 9, 'Ingles', 'EEUU', '2016/04/11', '2018/04/11', 'EEUU', 'N/A', 'A1', 'Bogota D.C'),
(108, 63, 1, 'Ingeniero De Software', 'UNI', '2018/04/11', '2023/04/11', 'Crack!!', NULL, NULL, 'Bogota D.C'),
(113, 63, 1, 'TESTER', 'UNI', '2018/02/01', '2018/02/27', 'CRACK', NULL, NULL, 'Bogota D.C'),
(114, 63, 1, 'HACKER', 'Secret', '2018/02/01', '2024/02/27', 'crack', NULL, NULL, 'Bogota D.C'),
(118, 30, 4, 'Análisis y Desarrollo de Sistemas de Información', 'SENA', '2018/03/01', '2018/03/31', 'eSTUDIOO', 'N/A', 'N/A', 'Bogota D.C'),
(119, 30, 1, 'Ingeniero De Software', 'UNI', '2018/01/01', '2018/03/23', 'Estudioss', 'N/A', 'N/A', 'Bogota D.C'),
(120, 30, 1, 'Análisis y Desarrollo de Sistemas de Información', 'SENA', '2018/03/01', '2018/03/23', 'est', NULL, NULL, 'Bogota D.C'),
(121, 30, 1, 'Análisis y Desarrollo de Sistemas de Información', 'SENA', '2018/03/01', '2018/03/23', 'sdfsd', NULL, NULL, 'Bogota D.C'),
(122, 63, 1, 'Análisis y Desarrollo de Sistemas de Información', 'SENA', '2018/04/01', '2018/04/03', 'sENA', '0', '0', 'Bogota D.C'),
(123, 63, 8, 'ISTQB', 'S.Q.A S.A', '2017/10/11', '2018/04/11', 'ISTQB', '0', '0', 'Bogota D.C'),
(124, 63, 8, 'Modelos De Calidad Desarrollo de Software', 'SENA', '2017/04/17', '2017/04/18', 'Curso Sena', '0', '0', 'Bogota'),
(125, 63, 8, 'Modelos De Calidad Desarrollo de Software', 'SENA', '2017/04/17', '2017/04/18', 'Curso', '0', '0', 'Bogota'),
(129, 30, 1, 'p uni', 'p uni', '2018/05/02', '2018/05/02', 'p uni', 'p uni', '0', 'p uni'),
(130, 30, 5, 'r', 'r', '2018/05/02', '2018/05/02', 'rrrrr', '0', '0', 'r');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `experiencia_laboral`
--

CREATE TABLE `experiencia_laboral` (
  `Id_Exp_Laboral` int(11) NOT NULL,
  `Id_Datos` int(11) NOT NULL,
  `Id_Sector` int(11) NOT NULL,
  `Trabaja_en_empresa` varchar(3) NOT NULL,
  `Empresa` varchar(100) NOT NULL,
  `Cargo` varchar(100) NOT NULL,
  `Fecha_Inicio` varchar(12) NOT NULL,
  `Fecha_Fin` varchar(12) DEFAULT NULL,
  `Meses_Experiencia` varchar(10) DEFAULT NULL,
  `Contacto` varchar(15) NOT NULL,
  `Descripcion` varchar(1500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `experiencia_laboral`
--

INSERT INTO `experiencia_laboral` (`Id_Exp_Laboral`, `Id_Datos`, `Id_Sector`, `Trabaja_en_empresa`, `Empresa`, `Cargo`, `Fecha_Inicio`, `Fecha_Fin`, `Meses_Experiencia`, `Contacto`, `Descripcion`) VALUES
(14, 30, 1, 'No', 'Desarrollo ', 'Super Ingeniero ', '2016/02/01', '2018/01/04', NULL, '312541212', 'Desarrrollo en java con framework JSF'),
(15, 30, 1, 'No', 'QA', 'Analista de pruebas', '2018/02/02', '2018/03/09', NULL, '321748485', 'Pruebas a Tigo'),
(16, 46, 4, 'No', 'Choquer', 'Analista de pruebas', '2017/11/04', '2018/02/02', NULL, '3125145155', 'Pruebas funcionales'),
(17, 63, 1, 'No', 'SQA SA', 'Desarrollador', '2018/02/01', '2018/02/06', NULL, '3158975776', 'Desarrollo'),
(18, 63, 7, 'No', 'SQA SA', 'Desarrollador', '2017/10/11', '2018/04/11', NULL, '8053040', 'Pruebas funcionales'),
(19, 63, 7, 'No', 'SQA SA', 'Desarrollador', '2018/02/01', '2018/04/11', NULL, '8053040', 'Prueba'),
(20, 63, 7, 'No', 'SQA SA', 'Desarrollador', '2017/10/11', '2018/04/11', NULL, '324567896', 'Desarrollo'),
(21, 30, 1, 'Si', 'Microsoft', 'Analista de Pruebas', '2018/03/15', '', NULL, '123456788', 'Ejecutor'),
(22, 30, 2, 'Si', 'SQA SA', 'Estadistica', '2017/08/01', NULL, NULL, '2733456', 'Ingreso'),
(23, 30, 6, 'No', 'Microsoft', 'Analista', '2018/03/01', '2018-03-08', NULL, '3213124', 'Gerente'),
(24, 30, 2, 'Si', 'Binary', 'Ejecutor', '2018/04/22', NULL, NULL, '4324535', 'Analista de Pruebas'),
(25, 30, 1, 'Si', 'SQA', 'Ejecutor', '2018/03/01', NULL, NULL, '75458900', 'Analista de Pruebas'),
(26, 30, 1, 'No', 'Pragma', 'Ejecutor de Pruebas', '2018/01/01', '2018-03-23', NULL, '2734567', 'Experiencia Pragma'),
(27, 30, 1, 'Si', 'asdasd', 'sadasd', '2018/06/28', '', NULL, '23242424', 'dsadsa'),
(28, 30, 1, 'No', 'asdasd', 'sadasd', '2018/03/13', '', NULL, '23242424', 'dsadsa'),
(29, 63, 1, 'No', 'ADSI', 'Desarrollador', '2018/03/03', '2018-04-02', NULL, '2734536', 'tRABAJO'),
(30, 63, 1, 'No', 'SQA SA', 'Desarrollador', '2017/12/01', '2018-04-09', NULL, '2984573', 'Experiencia Laboral'),
(47, 30, 7, 'Si', 'dorian', 'dorian', '2018/05/03', NULL, NULL, '16', 'dorino'),
(48, 30, 1, 'No', 'lunjes', 'lunjes', '2018/05/07', '2018-05-09', NULL, '1451445', 'lunjes'),
(49, 30, 1, 'Si', 'dalñado', 'dalñado', '2018/05/17', NULL, NULL, '4545454', 'dalñado'),
(50, 30, 1, 'Si', 'dalñado', 'dalñado', '2018/05/17', NULL, NULL, '4545454', 'dalñado'),
(51, 30, 1, 'No', 'dfdfdfd', 'dfdfdsf', '2018/05/16', '2018-05-01', NULL, '21211', 'dsfsdfsdf'),
(52, 30, 7, 'Si', 'el modal pues', 'el modal pues', '2018/05/09', NULL, NULL, '25215', 'el modal pues'),
(53, 30, 1, 'Si', 'prueba final', 'prueba final', '2018/06/06', NULL, NULL, '000011', 'prueba final');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horas_extras`
--

CREATE TABLE `horas_extras` (
  `Id_Horas_Extras` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Cantidad_Horas` float NOT NULL,
  `Comentario` varchar(200) NOT NULL,
  `Empresa_Contrata` int(11) NOT NULL,
  `nombreProyecto` varchar(200) NOT NULL,
  `Id_Tipo_Hora_fk` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horas_extras`
--

INSERT INTO `horas_extras` (`Id_Horas_Extras`, `Fecha`, `Cantidad_Horas`, `Comentario`, `Empresa_Contrata`, `nombreProyecto`, `Id_Tipo_Hora_fk`) VALUES
(497427, '2017-10-28', 8.5, 'Ejecución de pruebas.', 998, 'ATH - RQ28622 Cambio Look&Feel Portal de Pagos Aval para Banco Occidente', 'Extras Ordinarias Diurnas'),
(503390, '2017-11-20', 3, 'Ejecucion Ciclo 4', 998, 'ATH - RQ28622 Cambio Look&Feel Portal de Pagos Aval para Banco Occidente', 'Extras Ordinarias Diurnas'),
(558181, '2018-07-03', 0.5, 'Reunión para definir como se ejecutaran las pruebas luego de la migración (horario extra 5:30 a 6 pm)', 512, 'Migración pagina de Terpel', 'Extras Ordinarias Diurnas'),
(558497, '2018-07-04', 3, 'Revisión página Aliado corporativo luego de migración (10 pm a 1 am)', 512, 'Migración pagina de Terpel', 'Extras Ordinarias Nocturnas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id_municipios` int(10) NOT NULL,
  `nombre_municipio` varchar(50) NOT NULL,
  `id_deaprtamento` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`id_municipios`, `nombre_municipio`, `id_deaprtamento`) VALUES
(1, 'Leticia', 1),
(2, 'Puerto Nariño', 1),
(3, 'Abejorral', 2),
(4, 'Abriaquí', 2),
(5, 'Alejandria', 2),
(6, 'Amagá', 2),
(7, 'Amalfi', 2),
(8, 'Andes', 2),
(9, 'Angelópolis', 2),
(10, 'Angostura', 2),
(11, 'Anorí', 2),
(12, 'Anzá', 2),
(13, 'Apartadó', 2),
(14, 'Arboletes', 2),
(15, 'Argelia', 2),
(16, 'Armenia', 2),
(17, 'Barbosa', 2),
(18, 'Bello', 2),
(19, 'Belmira', 2),
(20, 'Betania', 2),
(21, 'Betulia', 2),
(22, 'Bolívar', 2),
(23, 'Briceño', 2),
(24, 'Burítica', 2),
(25, 'Caicedo', 2),
(26, 'Caldas', 2),
(27, 'Campamento', 2),
(28, 'Caracolí', 2),
(29, 'Caramanta', 2),
(30, 'Carepa', 2),
(31, 'Carmen de Viboral', 2),
(32, 'Carolina', 2),
(33, 'Caucasia', 2),
(34, 'Cañasgordas', 2),
(35, 'Chigorodó', 2),
(36, 'Cisneros', 2),
(37, 'Cocorná', 2),
(38, 'Concepción', 2),
(39, 'Concordia', 2),
(40, 'Copacabana', 2),
(41, 'Cáceres', 2),
(42, 'Dabeiba', 2),
(43, 'Don Matías', 2),
(44, 'Ebéjico', 2),
(45, 'El Bagre', 2),
(46, 'Entrerríos', 2),
(47, 'Envigado', 2),
(48, 'Fredonia', 2),
(49, 'Frontino', 2),
(50, 'Giraldo', 2),
(51, 'Girardota', 2),
(52, 'Granada', 2),
(53, 'Guadalupe', 2),
(54, 'Guarne', 2),
(55, 'Guatapé', 2),
(56, 'Gómez Plata', 2),
(57, 'Heliconia', 2),
(58, 'Hispania', 2),
(59, 'Itagüí', 2),
(60, 'Ituango', 2),
(61, 'Jardín', 2),
(62, 'Jericó', 2),
(63, 'La Ceja', 2),
(64, 'La Estrella', 2),
(65, 'La Pintada', 2),
(66, 'La Unión', 2),
(67, 'Liborina', 2),
(68, 'Maceo', 2),
(69, 'Marinilla', 2),
(70, 'Medellín', 2),
(71, 'Montebello', 2),
(72, 'Murindó', 2),
(73, 'Mutatá', 2),
(74, 'Nariño', 2),
(75, 'Nechí', 2),
(76, 'Necoclí', 2),
(77, 'Olaya', 2),
(78, 'Peque', 2),
(79, 'Peñol', 2),
(80, 'Pueblorrico', 2),
(81, 'Puerto Berrío', 2),
(82, 'Puerto Nare', 2),
(83, 'Puerto Triunfo', 2),
(84, 'Remedios', 2),
(85, 'Retiro', 2),
(86, 'Ríonegro', 2),
(87, 'Sabanalarga', 2),
(88, 'Sabaneta', 2),
(89, 'Salgar', 2),
(90, 'San Andrés de Cuerquía', 2),
(91, 'San Carlos', 2),
(92, 'San Francisco', 2),
(93, 'San Jerónimo', 2),
(94, 'San José de Montaña', 2),
(95, 'San Juan de Urabá', 2),
(96, 'San Luís', 2),
(97, 'San Pedro', 2),
(98, 'San Pedro de Urabá', 2),
(99, 'San Rafael', 2),
(100, 'San Roque', 2),
(101, 'San Vicente', 2),
(102, 'Santa Bárbara', 2),
(103, 'Santa Fé de Antioquia', 2),
(104, 'Santa Rosa de Osos', 2),
(105, 'Santo Domingo', 2),
(106, 'Santuario', 2),
(107, 'Segovia', 2),
(108, 'Sonsón', 2),
(109, 'Sopetrán', 2),
(110, 'Tarazá', 2),
(111, 'Tarso', 2),
(112, 'Titiribí', 2),
(113, 'Toledo', 2),
(114, 'Turbo', 2),
(115, 'Támesis', 2),
(116, 'Uramita', 2),
(117, 'Urrao', 2),
(118, 'Valdivia', 2),
(119, 'Valparaiso', 2),
(120, 'Vegachí', 2),
(121, 'Venecia', 2),
(122, 'Vigía del Fuerte', 2),
(123, 'Yalí', 2),
(124, 'Yarumal', 2),
(125, 'Yolombó', 2),
(126, 'Yondó (Casabe)', 2),
(127, 'Zaragoza', 2),
(128, 'Arauca', 3),
(129, 'Arauquita', 3),
(130, 'Cravo Norte', 3),
(131, 'Fortúl', 3),
(132, 'Puerto Rondón', 3),
(133, 'Saravena', 3),
(134, 'Tame', 3),
(135, 'Baranoa', 4),
(136, 'Barranquilla', 4),
(137, 'Campo de la Cruz', 4),
(138, 'Candelaria', 4),
(139, 'Galapa', 4),
(140, 'Juan de Acosta', 4),
(141, 'Luruaco', 4),
(142, 'Malambo', 4),
(143, 'Manatí', 4),
(144, 'Palmar de Varela', 4),
(145, 'Piojo', 4),
(146, 'Polonuevo', 4),
(147, 'Ponedera', 4),
(148, 'Puerto Colombia', 4),
(149, 'Repelón', 4),
(150, 'Sabanagrande', 4),
(151, 'Sabanalarga', 4),
(152, 'Santa Lucía', 4),
(153, 'Santo Tomás', 4),
(154, 'Soledad', 4),
(155, 'Suan', 4),
(156, 'Tubará', 4),
(157, 'Usiacuri', 4),
(158, 'Achí', 5),
(159, 'Altos del Rosario', 5),
(160, 'Arenal', 5),
(161, 'Arjona', 5),
(162, 'Arroyohondo', 5),
(163, 'Barranco de Loba', 5),
(164, 'Calamar', 5),
(165, 'Cantagallo', 5),
(166, 'Cartagena', 5),
(167, 'Cicuco', 5),
(168, 'Clemencia', 5),
(169, 'Córdoba', 5),
(170, 'El Carmen de Bolívar', 5),
(171, 'El Guamo', 5),
(172, 'El Peñon', 5),
(173, 'Hatillo de Loba', 5),
(174, 'Magangué', 5),
(175, 'Mahates', 5),
(176, 'Margarita', 5),
(177, 'María la Baja', 5),
(178, 'Mompós', 5),
(179, 'Montecristo', 5),
(180, 'Morales', 5),
(181, 'Norosí', 5),
(182, 'Pinillos', 5),
(183, 'Regidor', 5),
(184, 'Río Viejo', 5),
(185, 'San Cristobal', 5),
(186, 'San Estanislao', 5),
(187, 'San Fernando', 5),
(188, 'San Jacinto', 5),
(189, 'San Jacinto del Cauca', 5),
(190, 'San Juan de Nepomuceno', 5),
(191, 'San Martín de Loba', 5),
(192, 'San Pablo', 5),
(193, 'Santa Catalina', 5),
(194, 'Santa Rosa ', 5),
(195, 'Santa Rosa del Sur', 5),
(196, 'Simití', 5),
(197, 'Soplaviento', 5),
(198, 'Talaigua Nuevo', 5),
(199, 'Tiquisio (Puerto Rico)', 5),
(200, 'Turbaco', 5),
(201, 'Turbaná', 5),
(202, 'Villanueva', 5),
(203, 'Zambrano', 5),
(204, 'Almeida', 6),
(205, 'Aquitania', 6),
(206, 'Arcabuco', 6),
(207, 'Belén', 6),
(208, 'Berbeo', 6),
(209, 'Beteitiva', 6),
(210, 'Boavita', 6),
(211, 'Boyacá', 6),
(212, 'Briceño', 6),
(213, 'Buenavista', 6),
(214, 'Busbanza', 6),
(215, 'Caldas', 6),
(216, 'Campohermoso', 6),
(217, 'Cerinza', 6),
(218, 'Chinavita', 6),
(219, 'Chiquinquirá', 6),
(220, 'Chiscas', 6),
(221, 'Chita', 6),
(222, 'Chitaraque', 6),
(223, 'Chivatá', 6),
(224, 'Chíquiza', 6),
(225, 'Chívor', 6),
(226, 'Ciénaga', 6),
(227, 'Coper', 6),
(228, 'Corrales', 6),
(229, 'Covarachía', 6),
(230, 'Cubará', 6),
(231, 'Cucaita', 6),
(232, 'Cuitiva', 6),
(233, 'Cómbita', 6),
(234, 'Duitama', 6),
(235, 'El Cocuy', 6),
(236, 'El Espino', 6),
(237, 'Firavitoba', 6),
(238, 'Floresta', 6),
(239, 'Gachantivá', 6),
(240, 'Garagoa', 6),
(241, 'Guacamayas', 6),
(242, 'Guateque', 6),
(243, 'Guayatá', 6),
(244, 'Guicán', 6),
(245, 'Gámeza', 6),
(246, 'Izá', 6),
(247, 'Jenesano', 6),
(248, 'Jericó', 6),
(249, 'La Capilla', 6),
(250, 'La Uvita', 6),
(251, 'La Victoria', 6),
(252, 'Labranzagrande', 6),
(253, 'Macanal', 6),
(254, 'Maripí', 6),
(255, 'Miraflores', 6),
(256, 'Mongua', 6),
(257, 'Monguí', 6),
(258, 'Moniquirá', 6),
(259, 'Motavita', 6),
(260, 'Muzo', 6),
(261, 'Nobsa', 6),
(262, 'Nuevo Colón', 6),
(263, 'Oicatá', 6),
(264, 'Otanche', 6),
(265, 'Pachavita', 6),
(266, 'Paipa', 6),
(267, 'Pajarito', 6),
(268, 'Panqueba', 6),
(269, 'Pauna', 6),
(270, 'Paya', 6),
(271, 'Paz de Río', 6),
(272, 'Pesca', 6),
(273, 'Pisva', 6),
(274, 'Puerto Boyacá', 6),
(275, 'Páez', 6),
(276, 'Quipama', 6),
(277, 'Ramiriquí', 6),
(278, 'Rondón', 6),
(279, 'Ráquira', 6),
(280, 'Saboyá', 6),
(281, 'Samacá', 6),
(282, 'San Eduardo', 6),
(283, 'San José de Pare', 6),
(284, 'San Luís de Gaceno', 6),
(285, 'San Mateo', 6),
(286, 'San Miguel de Sema', 6),
(287, 'San Pablo de Borbur', 6),
(288, 'Santa María', 6),
(289, 'Santa Rosa de Viterbo', 6),
(290, 'Santa Sofía', 6),
(291, 'Santana', 6),
(292, 'Sativanorte', 6),
(293, 'Sativasur', 6),
(294, 'Siachoque', 6),
(295, 'Soatá', 6),
(296, 'Socha', 6),
(297, 'Socotá', 6),
(298, 'Sogamoso', 6),
(299, 'Somondoco', 6),
(300, 'Sora', 6),
(301, 'Soracá', 6),
(302, 'Sotaquirá', 6),
(303, 'Susacón', 6),
(304, 'Sutamarchán', 6),
(305, 'Sutatenza', 6),
(306, 'Sáchica', 6),
(307, 'Tasco', 6),
(308, 'Tenza', 6),
(309, 'Tibaná', 6),
(310, 'Tibasosa', 6),
(311, 'Tinjacá', 6),
(312, 'Tipacoque', 6),
(313, 'Toca', 6),
(314, 'Toguí', 6),
(315, 'Topagá', 6),
(316, 'Tota', 6),
(317, 'Tunja', 6),
(318, 'Tunungua', 6),
(319, 'Turmequé', 6),
(320, 'Tuta', 6),
(321, 'Tutasá', 6),
(322, 'Ventaquemada', 6),
(323, 'Villa de Leiva', 6),
(324, 'Viracachá', 6),
(325, 'Zetaquirá', 6),
(326, 'Úmbita', 6),
(327, 'Aguadas', 7),
(328, 'Anserma', 7),
(329, 'Aranzazu', 7),
(330, 'Belalcázar', 7),
(331, 'Chinchiná', 7),
(332, 'Filadelfia', 7),
(333, 'La Dorada', 7),
(334, 'La Merced', 7),
(335, 'La Victoria', 7),
(336, 'Manizales', 7),
(337, 'Manzanares', 7),
(338, 'Marmato', 7),
(339, 'Marquetalia', 7),
(340, 'Marulanda', 7),
(341, 'Neira', 7),
(342, 'Norcasia', 7),
(343, 'Palestina', 7),
(344, 'Pensilvania', 7),
(345, 'Pácora', 7),
(346, 'Risaralda', 7),
(347, 'Río Sucio', 7),
(348, 'Salamina', 7),
(349, 'Samaná', 7),
(350, 'San José', 7),
(351, 'Supía', 7),
(352, 'Villamaría', 7),
(353, 'Viterbo', 7),
(354, 'Albania', 8),
(355, 'Belén de los Andaquíes', 8),
(356, 'Cartagena del Chairá', 8),
(357, 'Curillo', 8),
(358, 'El Doncello', 8),
(359, 'El Paujil', 8),
(360, 'Florencia', 8),
(361, 'La Montañita', 8),
(362, 'Milán', 8),
(363, 'Morelia', 8),
(364, 'Puerto Rico', 8),
(365, 'San José del Fragua', 8),
(366, 'San Vicente del Caguán', 8),
(367, 'Solano', 8),
(368, 'Solita', 8),
(369, 'Valparaiso', 8),
(370, 'Aguazul', 9),
(371, 'Chámeza', 9),
(372, 'Hato Corozal', 9),
(373, 'La Salina', 9),
(374, 'Maní', 9),
(375, 'Monterrey', 9),
(376, 'Nunchía', 9),
(377, 'Orocué', 9),
(378, 'Paz de Ariporo', 9),
(379, 'Pore', 9),
(380, 'Recetor', 9),
(381, 'Sabanalarga', 9),
(382, 'San Luís de Palenque', 9),
(383, 'Sácama', 9),
(384, 'Tauramena', 9),
(385, 'Trinidad', 9),
(386, 'Támara', 9),
(387, 'Villanueva', 9),
(388, 'Yopal', 9),
(389, 'Almaguer', 10),
(390, 'Argelia', 10),
(391, 'Balboa', 10),
(392, 'Bolívar', 10),
(393, 'Buenos Aires', 10),
(394, 'Cajibío', 10),
(395, 'Caldono', 10),
(396, 'Caloto', 10),
(397, 'Corinto', 10),
(398, 'El Tambo', 10),
(399, 'Florencia', 10),
(400, 'Guachené', 10),
(401, 'Guapí', 10),
(402, 'Inzá', 10),
(403, 'Jambaló', 10),
(404, 'La Sierra', 10),
(405, 'La Vega', 10),
(406, 'López (Micay)', 10),
(407, 'Mercaderes', 10),
(408, 'Miranda', 10),
(409, 'Morales', 10),
(410, 'Padilla', 10),
(411, 'Patía (El Bordo)', 10),
(412, 'Piamonte', 10),
(413, 'Piendamó', 10),
(414, 'Popayán', 10),
(415, 'Puerto Tejada', 10),
(416, 'Puracé (Coconuco)', 10),
(417, 'Páez (Belalcazar)', 10),
(418, 'Rosas', 10),
(419, 'San Sebastián', 10),
(420, 'Santa Rosa', 10),
(421, 'Santander de Quilichao', 10),
(422, 'Silvia', 10),
(423, 'Sotara (Paispamba)', 10),
(424, 'Sucre', 10),
(425, 'Suárez', 10),
(426, 'Timbiquí', 10),
(427, 'Timbío', 10),
(428, 'Toribío', 10),
(429, 'Totoró', 10),
(430, 'Villa Rica', 10),
(431, 'Aguachica', 11),
(432, 'Agustín Codazzi', 11),
(433, 'Astrea', 11),
(434, 'Becerríl', 11),
(435, 'Bosconia', 11),
(436, 'Chimichagua', 11),
(437, 'Chiriguaná', 11),
(438, 'Curumaní', 11),
(439, 'El Copey', 11),
(440, 'El Paso', 11),
(441, 'Gamarra', 11),
(442, 'Gonzalez', 11),
(443, 'La Gloria', 11),
(444, 'La Jagua de Ibirico', 11),
(445, 'La Paz (Robles)', 11),
(446, 'Manaure Balcón del Cesar', 11),
(447, 'Pailitas', 11),
(448, 'Pelaya', 11),
(449, 'Pueblo Bello', 11),
(450, 'Río de oro', 11),
(451, 'San Alberto', 11),
(452, 'San Diego', 11),
(453, 'San Martín', 11),
(454, 'Tamalameque', 11),
(455, 'Valledupar', 11),
(456, 'Acandí', 12),
(457, 'Alto Baudó (Pie de Pato)', 12),
(458, 'Atrato (Yuto)', 12),
(459, 'Bagadó', 12),
(460, 'Bahía Solano (Mútis)', 12),
(461, 'Bajo Baudó (Pizarro)', 12),
(462, 'Belén de Bajirá', 12),
(463, 'Bojayá (Bellavista)', 12),
(464, 'Cantón de San Pablo', 12),
(465, 'Carmen del Darién (CURBARADÓ)', 12),
(466, 'Condoto', 12),
(467, 'Cértegui', 12),
(468, 'El Carmen de Atrato', 12),
(469, 'Istmina', 12),
(470, 'Juradó', 12),
(471, 'Lloró', 12),
(472, 'Medio Atrato', 12),
(473, 'Medio Baudó', 12),
(474, 'Medio San Juan (ANDAGOYA)', 12),
(475, 'Novita', 12),
(476, 'Nuquí', 12),
(477, 'Quibdó', 12),
(478, 'Río Iró', 12),
(479, 'Río Quito', 12),
(480, 'Ríosucio', 12),
(481, 'San José del Palmar', 12),
(482, 'Santa Genoveva de Docorodó', 12),
(483, 'Sipí', 12),
(484, 'Tadó', 12),
(485, 'Unguía', 12),
(486, 'Unión Panamericana (ÁNIMAS)', 12),
(487, 'Ayapel', 13),
(488, 'Buenavista', 13),
(489, 'Canalete', 13),
(490, 'Cereté', 13),
(491, 'Chimá', 13),
(492, 'Chinú', 13),
(493, 'Ciénaga de Oro', 13),
(494, 'Cotorra', 13),
(495, 'La Apartada y La Frontera', 13),
(496, 'Lorica', 13),
(497, 'Los Córdobas', 13),
(498, 'Momil', 13),
(499, 'Montelíbano', 13),
(500, 'Monteria', 13),
(501, 'Moñitos', 13),
(502, 'Planeta Rica', 13),
(503, 'Pueblo Nuevo', 13),
(504, 'Puerto Escondido', 13),
(505, 'Puerto Libertador', 13),
(506, 'Purísima', 13),
(507, 'Sahagún', 13),
(508, 'San Andrés Sotavento', 13),
(509, 'San Antero', 13),
(510, 'San Bernardo del Viento', 13),
(511, 'San Carlos', 13),
(512, 'San José de Uré', 13),
(513, 'San Pelayo', 13),
(514, 'Tierralta', 13),
(515, 'Tuchín', 13),
(516, 'Valencia', 13),
(517, 'Agua de Dios', 14),
(518, 'Albán', 14),
(519, 'Anapoima', 14),
(520, 'Anolaima', 14),
(521, 'Apulo', 14),
(522, 'Arbeláez', 14),
(523, 'Beltrán', 14),
(524, 'Bituima', 14),
(525, 'Bogotá D.C.', 14),
(526, 'Bojacá', 14),
(527, 'Cabrera', 14),
(528, 'Cachipay', 14),
(529, 'Cajicá', 14),
(530, 'Caparrapí', 14),
(531, 'Carmen de Carupa', 14),
(532, 'Chaguaní', 14),
(533, 'Chipaque', 14),
(534, 'Choachí', 14),
(535, 'Chocontá', 14),
(536, 'Chía', 14),
(537, 'Cogua', 14),
(538, 'Cota', 14),
(539, 'Cucunubá', 14),
(540, 'Cáqueza', 14),
(541, 'El Colegio', 14),
(542, 'El Peñón', 14),
(543, 'El Rosal', 14),
(544, 'Facatativá', 14),
(545, 'Fosca', 14),
(546, 'Funza', 14),
(547, 'Fusagasugá', 14),
(548, 'Fómeque', 14),
(549, 'Fúquene', 14),
(550, 'Gachalá', 14),
(551, 'Gachancipá', 14),
(552, 'Gachetá', 14),
(553, 'Gama', 14),
(554, 'Girardot', 14),
(555, 'Granada', 14),
(556, 'Guachetá', 14),
(557, 'Guaduas', 14),
(558, 'Guasca', 14),
(559, 'Guataquí', 14),
(560, 'Guatavita', 14),
(561, 'Guayabal de Siquima', 14),
(562, 'Guayabetal', 14),
(563, 'Gutiérrez', 14),
(564, 'Jerusalén', 14),
(565, 'Junín', 14),
(566, 'La Calera', 14),
(567, 'La Mesa', 14),
(568, 'La Palma', 14),
(569, 'La Peña', 14),
(570, 'La Vega', 14),
(571, 'Lenguazaque', 14),
(572, 'Machetá', 14),
(573, 'Madrid', 14),
(574, 'Manta', 14),
(575, 'Medina', 14),
(576, 'Mosquera', 14),
(577, 'Nariño', 14),
(578, 'Nemocón', 14),
(579, 'Nilo', 14),
(580, 'Nimaima', 14),
(581, 'Nocaima', 14),
(582, 'Pacho', 14),
(583, 'Paime', 14),
(584, 'Pandi', 14),
(585, 'Paratebueno', 14),
(586, 'Pasca', 14),
(587, 'Puerto Salgar', 14),
(588, 'Pulí', 14),
(589, 'Quebradanegra', 14),
(590, 'Quetame', 14),
(591, 'Quipile', 14),
(592, 'Ricaurte', 14),
(593, 'San Antonio de Tequendama', 14),
(594, 'San Bernardo', 14),
(595, 'San Cayetano', 14),
(596, 'San Francisco', 14),
(597, 'San Juan de Río Seco', 14),
(598, 'Sasaima', 14),
(599, 'Sesquilé', 14),
(600, 'Sibaté', 14),
(601, 'Silvania', 14),
(602, 'Simijaca', 14),
(603, 'Soacha', 14),
(604, 'Sopó', 14),
(605, 'Subachoque', 14),
(606, 'Suesca', 14),
(607, 'Supatá', 14),
(608, 'Susa', 14),
(609, 'Sutatausa', 14),
(610, 'Tabio', 14),
(611, 'Tausa', 14),
(612, 'Tena', 14),
(613, 'Tenjo', 14),
(614, 'Tibacuy', 14),
(615, 'Tibirita', 14),
(616, 'Tocaima', 14),
(617, 'Tocancipá', 14),
(618, 'Topaipí', 14),
(619, 'Ubalá', 14),
(620, 'Ubaque', 14),
(621, 'Ubaté', 14),
(622, 'Une', 14),
(623, 'Venecia (Ospina Pérez)', 14),
(624, 'Vergara', 14),
(625, 'Viani', 14),
(626, 'Villagómez', 14),
(627, 'Villapinzón', 14),
(628, 'Villeta', 14),
(629, 'Viotá', 14),
(630, 'Yacopí', 14),
(631, 'Zipacón', 14),
(632, 'Zipaquirá', 14),
(633, 'Útica', 14),
(634, 'Inírida', 15),
(635, 'Calamar', 16),
(636, 'El Retorno', 16),
(637, 'Miraflores', 16),
(638, 'San José del Guaviare', 16),
(639, 'Acevedo', 17),
(640, 'Agrado', 17),
(641, 'Aipe', 17),
(642, 'Algeciras', 17),
(643, 'Altamira', 17),
(644, 'Baraya', 17),
(645, 'Campoalegre', 17),
(646, 'Colombia', 17),
(647, 'Elías', 17),
(648, 'Garzón', 17),
(649, 'Gigante', 17),
(650, 'Guadalupe', 17),
(651, 'Hobo', 17),
(652, 'Isnos', 17),
(653, 'La Argentina', 17),
(654, 'La Plata', 17),
(655, 'Neiva', 17),
(656, 'Nátaga', 17),
(657, 'Oporapa', 17),
(658, 'Paicol', 17),
(659, 'Palermo', 17),
(660, 'Palestina', 17),
(661, 'Pital', 17),
(662, 'Pitalito', 17),
(663, 'Rivera', 17),
(664, 'Saladoblanco', 17),
(665, 'San Agustín', 17),
(666, 'Santa María', 17),
(667, 'Suaza', 17),
(668, 'Tarqui', 17),
(669, 'Tello', 17),
(670, 'Teruel', 17),
(671, 'Tesalia', 17),
(672, 'Timaná', 17),
(673, 'Villavieja', 17),
(674, 'Yaguará', 17),
(675, 'Íquira', 17),
(676, 'Albania', 18),
(677, 'Barrancas', 18),
(678, 'Dibulla', 18),
(679, 'Distracción', 18),
(680, 'El Molino', 18),
(681, 'Fonseca', 18),
(682, 'Hatonuevo', 18),
(683, 'La Jagua del Pilar', 18),
(684, 'Maicao', 18),
(685, 'Manaure', 18),
(686, 'Riohacha', 18),
(687, 'San Juan del Cesar', 18),
(688, 'Uribia', 18),
(689, 'Urumita', 18),
(690, 'Villanueva', 18),
(691, 'Algarrobo', 19),
(692, 'Aracataca', 19),
(693, 'Ariguaní (El Difícil)', 19),
(694, 'Cerro San Antonio', 19),
(695, 'Chivolo', 19),
(696, 'Ciénaga', 19),
(697, 'Concordia', 19),
(698, 'El Banco', 19),
(699, 'El Piñon', 19),
(700, 'El Retén', 19),
(701, 'Fundación', 19),
(702, 'Guamal', 19),
(703, 'Nueva Granada', 19),
(704, 'Pedraza', 19),
(705, 'Pijiño', 19),
(706, 'Pivijay', 19),
(707, 'Plato', 19),
(708, 'Puebloviejo', 19),
(709, 'Remolino', 19),
(710, 'Sabanas de San Angel (SAN ANGEL)', 19),
(711, 'Salamina', 19),
(712, 'San Sebastián de Buenavista', 19),
(713, 'San Zenón', 19),
(714, 'Santa Ana', 19),
(715, 'Santa Bárbara de Pinto', 19),
(716, 'Santa Marta', 19),
(717, 'Sitionuevo', 19),
(718, 'Tenerife', 19),
(719, 'Zapayán (PUNTA DE PIEDRAS)', 19),
(720, 'Zona Bananera (PRADO - SEVILLA)', 19),
(721, 'Acacías', 20),
(722, 'Barranca de Upía', 20),
(723, 'Cabuyaro', 20),
(724, 'Castilla la Nueva', 20),
(725, 'Cubarral', 20),
(726, 'Cumaral', 20),
(727, 'El Calvario', 20),
(728, 'El Castillo', 20),
(729, 'El Dorado', 20),
(730, 'Fuente de Oro', 20),
(731, 'Granada', 20),
(732, 'Guamal', 20),
(733, 'La Macarena', 20),
(734, 'Lejanías', 20),
(735, 'Mapiripan', 20),
(736, 'Mesetas', 20),
(737, 'Puerto Concordia', 20),
(738, 'Puerto Gaitán', 20),
(739, 'Puerto Lleras', 20),
(740, 'Puerto López', 20),
(741, 'Puerto Rico', 20),
(742, 'Restrepo', 20),
(743, 'San Carlos de Guaroa', 20),
(744, 'San Juan de Arama', 20),
(745, 'San Juanito', 20),
(746, 'San Martín', 20),
(747, 'Uribe', 20),
(748, 'Villavicencio', 20),
(749, 'Vista Hermosa', 20),
(750, 'Albán (San José)', 21),
(751, 'Aldana', 21),
(752, 'Ancuya', 21),
(753, 'Arboleda (Berruecos)', 21),
(754, 'Barbacoas', 21),
(755, 'Belén', 21),
(756, 'Buesaco', 21),
(757, 'Chachaguí', 21),
(758, 'Colón (Génova)', 21),
(759, 'Consaca', 21),
(760, 'Contadero', 21),
(761, 'Cuaspud (Carlosama)', 21),
(762, 'Cumbal', 21),
(763, 'Cumbitara', 21),
(764, 'Córdoba', 21),
(765, 'El Charco', 21),
(766, 'El Peñol', 21),
(767, 'El Rosario', 21),
(768, 'El Tablón de Gómez', 21),
(769, 'El Tambo', 21),
(770, 'Francisco Pizarro', 21),
(771, 'Funes', 21),
(772, 'Guachavés', 21),
(773, 'Guachucal', 21),
(774, 'Guaitarilla', 21),
(775, 'Gualmatán', 21),
(776, 'Iles', 21),
(777, 'Imúes', 21),
(778, 'Ipiales', 21),
(779, 'La Cruz', 21),
(780, 'La Florida', 21),
(781, 'La Llanada', 21),
(782, 'La Tola', 21),
(783, 'La Unión', 21),
(784, 'Leiva', 21),
(785, 'Linares', 21),
(786, 'Magüi (Payán)', 21),
(787, 'Mallama (Piedrancha)', 21),
(788, 'Mosquera', 21),
(789, 'Nariño', 21),
(790, 'Olaya Herrera', 21),
(791, 'Ospina', 21),
(792, 'Policarpa', 21),
(793, 'Potosí', 21),
(794, 'Providencia', 21),
(795, 'Puerres', 21),
(796, 'Pupiales', 21),
(797, 'Ricaurte', 21),
(798, 'Roberto Payán (San José)', 21),
(799, 'Samaniego', 21),
(800, 'San Bernardo', 21),
(801, 'San Juan de Pasto', 21),
(802, 'San Lorenzo', 21),
(803, 'San Pablo', 21),
(804, 'San Pedro de Cartago', 21),
(805, 'Sandoná', 21),
(806, 'Santa Bárbara (Iscuandé)', 21),
(807, 'Sapuyes', 21),
(808, 'Sotomayor (Los Andes)', 21),
(809, 'Taminango', 21),
(810, 'Tangua', 21),
(811, 'Tumaco', 21),
(812, 'Túquerres', 21),
(813, 'Yacuanquer', 21),
(814, 'Arboledas', 22),
(815, 'Bochalema', 22),
(816, 'Bucarasica', 22),
(817, 'Chinácota', 22),
(818, 'Chitagá', 22),
(819, 'Convención', 22),
(820, 'Cucutilla', 22),
(821, 'Cáchira', 22),
(822, 'Cácota', 22),
(823, 'Cúcuta', 22),
(824, 'Durania', 22),
(825, 'El Carmen', 22),
(826, 'El Tarra', 22),
(827, 'El Zulia', 22),
(828, 'Gramalote', 22),
(829, 'Hacarí', 22),
(830, 'Herrán', 22),
(831, 'La Esperanza', 22),
(832, 'La Playa', 22),
(833, 'Labateca', 22),
(834, 'Los Patios', 22),
(835, 'Lourdes', 22),
(836, 'Mutiscua', 22),
(837, 'Ocaña', 22),
(838, 'Pamplona', 22),
(839, 'Pamplonita', 22),
(840, 'Puerto Santander', 22),
(841, 'Ragonvalia', 22),
(842, 'Salazar', 22),
(843, 'San Calixto', 22),
(844, 'San Cayetano', 22),
(845, 'Santiago', 22),
(846, 'Sardinata', 22),
(847, 'Silos', 22),
(848, 'Teorama', 22),
(849, 'Tibú', 22),
(850, 'Toledo', 22),
(851, 'Villa Caro', 22),
(852, 'Villa del Rosario', 22),
(853, 'Ábrego', 22),
(854, 'Colón', 23),
(855, 'Mocoa', 23),
(856, 'Orito', 23),
(857, 'Puerto Asís', 23),
(858, 'Puerto Caicedo', 23),
(859, 'Puerto Guzmán', 23),
(860, 'Puerto Leguízamo', 23),
(861, 'San Francisco', 23),
(862, 'San Miguel', 23),
(863, 'Santiago', 23),
(864, 'Sibundoy', 23),
(865, 'Valle del Guamuez', 23),
(866, 'Villagarzón', 23),
(867, 'Armenia', 24),
(868, 'Buenavista', 24),
(869, 'Calarcá', 24),
(870, 'Circasia', 24),
(871, 'Cordobá', 24),
(872, 'Filandia', 24),
(873, 'Génova', 24),
(874, 'La Tebaida', 24),
(875, 'Montenegro', 24),
(876, 'Pijao', 24),
(877, 'Quimbaya', 24),
(878, 'Salento', 24),
(879, 'Apía', 25),
(880, 'Balboa', 25),
(881, 'Belén de Umbría', 25),
(882, 'Dos Quebradas', 25),
(883, 'Guática', 25),
(884, 'La Celia', 25),
(885, 'La Virginia', 25),
(886, 'Marsella', 25),
(887, 'Mistrató', 25),
(888, 'Pereira', 25),
(889, 'Pueblo Rico', 25),
(890, 'Quinchía', 25),
(891, 'Santa Rosa de Cabal', 25),
(892, 'Santuario', 25),
(893, 'Providencia', 26),
(894, 'Aguada', 27),
(895, 'Albania', 27),
(896, 'Aratoca', 27),
(897, 'Barbosa', 27),
(898, 'Barichara', 27),
(899, 'Barrancabermeja', 27),
(900, 'Betulia', 27),
(901, 'Bolívar', 27),
(902, 'Bucaramanga', 27),
(903, 'Cabrera', 27),
(904, 'California', 27),
(905, 'Capitanejo', 27),
(906, 'Carcasí', 27),
(907, 'Cepita', 27),
(908, 'Cerrito', 27),
(909, 'Charalá', 27),
(910, 'Charta', 27),
(911, 'Chima', 27),
(912, 'Chipatá', 27),
(913, 'Cimitarra', 27),
(914, 'Concepción', 27),
(915, 'Confines', 27),
(916, 'Contratación', 27),
(917, 'Coromoro', 27),
(918, 'Curití', 27),
(919, 'El Carmen', 27),
(920, 'El Guacamayo', 27),
(921, 'El Peñon', 27),
(922, 'El Playón', 27),
(923, 'Encino', 27),
(924, 'Enciso', 27),
(925, 'Floridablanca', 27),
(926, 'Florián', 27),
(927, 'Galán', 27),
(928, 'Girón', 27),
(929, 'Guaca', 27),
(930, 'Guadalupe', 27),
(931, 'Guapota', 27),
(932, 'Guavatá', 27),
(933, 'Guepsa', 27),
(934, 'Gámbita', 27),
(935, 'Hato', 27),
(936, 'Jesús María', 27),
(937, 'Jordán', 27),
(938, 'La Belleza', 27),
(939, 'La Paz', 27),
(940, 'Landázuri', 27),
(941, 'Lebrija', 27),
(942, 'Los Santos', 27),
(943, 'Macaravita', 27),
(944, 'Matanza', 27),
(945, 'Mogotes', 27),
(946, 'Molagavita', 27),
(947, 'Málaga', 27),
(948, 'Ocamonte', 27),
(949, 'Oiba', 27),
(950, 'Onzaga', 27),
(951, 'Palmar', 27),
(952, 'Palmas del Socorro', 27),
(953, 'Pie de Cuesta', 27),
(954, 'Pinchote', 27),
(955, 'Puente Nacional', 27),
(956, 'Puerto Parra', 27),
(957, 'Puerto Wilches', 27),
(958, 'Páramo', 27),
(959, 'Rio Negro', 27),
(960, 'Sabana de Torres', 27),
(961, 'San Andrés', 27),
(962, 'San Benito', 27),
(963, 'San Gíl', 27),
(964, 'San Joaquín', 27),
(965, 'San José de Miranda', 27),
(966, 'San Miguel', 27),
(967, 'San Vicente del Chucurí', 27),
(968, 'Santa Bárbara', 27),
(969, 'Santa Helena del Opón', 27),
(970, 'Simacota', 27),
(971, 'Socorro', 27),
(972, 'Suaita', 27),
(973, 'Sucre', 27),
(974, 'Suratá', 27),
(975, 'Tona', 27),
(976, 'Valle de San José', 27),
(977, 'Vetas', 27),
(978, 'Villanueva', 27),
(979, 'Vélez', 27),
(980, 'Zapatoca', 27),
(981, 'Buenavista', 28),
(982, 'Caimito', 28),
(983, 'Chalán', 28),
(984, 'Colosó (Ricaurte)', 28),
(985, 'Corozal', 28),
(986, 'Coveñas', 28),
(987, 'El Roble', 28),
(988, 'Galeras (Nueva Granada)', 28),
(989, 'Guaranda', 28),
(990, 'La Unión', 28),
(991, 'Los Palmitos', 28),
(992, 'Majagual', 28),
(993, 'Morroa', 28),
(994, 'Ovejas', 28),
(995, 'Palmito', 28),
(996, 'Sampués', 28),
(997, 'San Benito Abad', 28),
(998, 'San Juan de Betulia', 28),
(999, 'San Marcos', 28),
(1000, 'San Onofre', 28),
(1001, 'San Pedro', 28),
(1002, 'Sincelejo', 28),
(1003, 'Sincé', 28),
(1004, 'Sucre', 28),
(1005, 'Tolú', 28),
(1006, 'Tolú Viejo', 28),
(1007, 'Alpujarra', 29),
(1008, 'Alvarado', 29),
(1009, 'Ambalema', 29),
(1010, 'Anzoátegui', 29),
(1011, 'Armero (Guayabal)', 29),
(1012, 'Ataco', 29),
(1013, 'Cajamarca', 29),
(1014, 'Carmen de Apicalá', 29),
(1015, 'Casabianca', 29),
(1016, 'Chaparral', 29),
(1017, 'Coello', 29),
(1018, 'Coyaima', 29),
(1019, 'Cunday', 29),
(1020, 'Dolores', 29),
(1021, 'Espinal', 29),
(1022, 'Falan', 29),
(1023, 'Flandes', 29),
(1024, 'Fresno', 29),
(1025, 'Guamo', 29),
(1026, 'Herveo', 29),
(1027, 'Honda', 29),
(1028, 'Ibagué', 29),
(1029, 'Icononzo', 29),
(1030, 'Lérida', 29),
(1031, 'Líbano', 29),
(1032, 'Mariquita', 29),
(1033, 'Melgar', 29),
(1034, 'Murillo', 29),
(1035, 'Natagaima', 29),
(1036, 'Ortega', 29),
(1037, 'Palocabildo', 29),
(1038, 'Piedras', 29),
(1039, 'Planadas', 29),
(1040, 'Prado', 29),
(1041, 'Purificación', 29),
(1042, 'Rioblanco', 29),
(1043, 'Roncesvalles', 29),
(1044, 'Rovira', 29),
(1045, 'Saldaña', 29),
(1046, 'San Antonio', 29),
(1047, 'San Luis', 29),
(1048, 'Santa Isabel', 29),
(1049, 'Suárez', 29),
(1050, 'Valle de San Juan', 29),
(1051, 'Venadillo', 29),
(1052, 'Villahermosa', 29),
(1053, 'Villarrica', 29),
(1054, 'Alcalá', 30),
(1055, 'Andalucía', 30),
(1056, 'Ansermanuevo', 30),
(1057, 'Argelia', 30),
(1058, 'Bolívar', 30),
(1059, 'Buenaventura', 30),
(1060, 'Buga', 30),
(1061, 'Bugalagrande', 30),
(1062, 'Caicedonia', 30),
(1063, 'Calima (Darién)', 30),
(1064, 'Calí', 30),
(1065, 'Candelaria', 30),
(1066, 'Cartago', 30),
(1067, 'Dagua', 30),
(1068, 'El Cairo', 30),
(1069, 'El Cerrito', 30),
(1070, 'El Dovio', 30),
(1071, 'El Águila', 30),
(1072, 'Florida', 30),
(1073, 'Ginebra', 30),
(1074, 'Guacarí', 30),
(1075, 'Jamundí', 30),
(1076, 'La Cumbre', 30),
(1077, 'La Unión', 30),
(1078, 'La Victoria', 30),
(1079, 'Obando', 30),
(1080, 'Palmira', 30),
(1081, 'Pradera', 30),
(1082, 'Restrepo', 30),
(1083, 'Riofrío', 30),
(1084, 'Roldanillo', 30),
(1085, 'San Pedro', 30),
(1086, 'Sevilla', 30),
(1087, 'Toro', 30),
(1088, 'Trujillo', 30),
(1089, 'Tulúa', 30),
(1090, 'Ulloa', 30),
(1091, 'Versalles', 30),
(1092, 'Vijes', 30),
(1093, 'Yotoco', 30),
(1094, 'Yumbo', 30),
(1095, 'Zarzal', 30),
(1096, 'Carurú', 31),
(1097, 'Mitú', 31),
(1098, 'Taraira', 31),
(1099, 'Cumaribo', 32),
(1100, 'La Primavera', 32),
(1101, 'Puerto Carreño', 32),
(1102, 'Santa Rosalía', 32),
(1103, 'Sin ciudad', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedad`
--

CREATE TABLE `novedad` (
  `Id_Novedad` int(11) NOT NULL,
  `Tipo_Novedad` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `novedad`
--

INSERT INTO `novedad` (`Id_Novedad`, `Tipo_Novedad`) VALUES
(1, 'Cambio cliente'),
(2, 'Retiro'),
(3, 'Otras'),
(4, 'Ingreso'),
(5, 'N/A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `idprivilegios` int(11) NOT NULL,
  `tipoPrivilegios` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`idprivilegios`, `tipoPrivilegios`) VALUES
(1, 'Crear'),
(2, 'Actualizar'),
(3, 'Eliminar'),
(4, 'Consultar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_has_action`
--

CREATE TABLE `permiso_has_action` (
  `permiso_idprivilegios` int(11) NOT NULL,
  `action_idaction` int(11) NOT NULL,
  `privilegios_Id_Privilegio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso_has_action`
--

INSERT INTO `permiso_has_action` (`permiso_idprivilegios`, `action_idaction`, `privilegios_Id_Privilegio`) VALUES
(1, 17, 1),
(1, 17, 4),
(1, 18, 4),
(1, 22, 1),
(1, 22, 2),
(1, 22, 3),
(1, 22, 4),
(1, 23, 1),
(1, 23, 2),
(1, 23, 3),
(1, 23, 4),
(2, 3, 1),
(2, 3, 2),
(2, 3, 4),
(2, 4, 1),
(2, 4, 2),
(2, 4, 4),
(2, 5, 1),
(2, 5, 2),
(2, 5, 3),
(2, 5, 4),
(2, 6, 1),
(2, 6, 2),
(2, 6, 4),
(2, 13, 1),
(2, 13, 2),
(2, 13, 3),
(2, 13, 4),
(2, 17, 1),
(2, 17, 4),
(2, 18, 1),
(2, 18, 4),
(2, 22, 1),
(2, 22, 4),
(2, 23, 1),
(2, 23, 4),
(3, 3, 1),
(3, 3, 2),
(3, 3, 3),
(3, 3, 4),
(3, 4, 1),
(3, 4, 2),
(3, 4, 3),
(3, 4, 4),
(3, 5, 1),
(3, 5, 2),
(3, 5, 3),
(3, 5, 4),
(3, 6, 1),
(3, 6, 2),
(3, 6, 3),
(3, 6, 4),
(3, 8, 1),
(3, 8, 3),
(3, 8, 4),
(3, 9, 1),
(3, 9, 3),
(3, 9, 4),
(3, 11, 1),
(3, 11, 2),
(3, 11, 3),
(3, 11, 4),
(3, 12, 1),
(3, 12, 2),
(3, 12, 3),
(3, 12, 4),
(3, 13, 1),
(3, 13, 2),
(3, 13, 3),
(3, 13, 4),
(3, 15, 1),
(3, 15, 2),
(3, 15, 3),
(3, 15, 4),
(3, 17, 1),
(3, 17, 3),
(3, 17, 4),
(3, 18, 1),
(3, 18, 3),
(3, 18, 4),
(3, 20, 1),
(3, 20, 2),
(3, 20, 3),
(3, 20, 4),
(3, 22, 1),
(3, 22, 2),
(3, 22, 3),
(3, 22, 4),
(3, 23, 1),
(3, 23, 2),
(3, 23, 3),
(3, 23, 4),
(3, 24, 1),
(3, 24, 2),
(3, 24, 3),
(3, 24, 4),
(3, 25, 1),
(3, 25, 2),
(3, 25, 3),
(3, 25, 4),
(3, 26, 4),
(3, 27, 4),
(3, 28, 4),
(5, 8, 1),
(5, 8, 2),
(5, 8, 4),
(5, 9, 1),
(5, 9, 2),
(5, 9, 4),
(5, 15, 1),
(5, 15, 2),
(5, 15, 4),
(5, 17, 1),
(5, 17, 2),
(5, 17, 4),
(5, 18, 1),
(5, 18, 2),
(5, 18, 4),
(5, 20, 1),
(5, 20, 2),
(5, 20, 4),
(5, 22, 1),
(5, 22, 2),
(5, 22, 4),
(5, 23, 1),
(5, 23, 2),
(5, 23, 4),
(5, 24, 1),
(5, 24, 2),
(5, 24, 4),
(5, 25, 1),
(5, 25, 2),
(5, 25, 4),
(5, 26, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegios`
--

CREATE TABLE `privilegios` (
  `Id_Privilegio` int(11) NOT NULL,
  `Tipo_Privilegio` varchar(45) NOT NULL,
  `Fecha_Registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `privilegios`
--

INSERT INTO `privilegios` (`Id_Privilegio`, `Tipo_Privilegio`, `Fecha_Registro`) VALUES
(1, 'Analista', '2017-02-28 11:18:21'),
(2, 'Talento humano', '2017-02-28 11:18:21'),
(3, 'Administrador', '2017-03-15 10:02:48'),
(4, 'Invitado', '2017-03-15 10:02:48'),
(5, 'Gerente de proyecto', '2017-03-15 10:02:48'),
(6, 'Direccion de operaciones', '2017-03-15 10:02:48'),
(9, 'Analista de Datos', '2018-03-12 15:17:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `Id_Rol` int(11) NOT NULL,
  `Nombre_Rol` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`Id_Rol`, `Nombre_Rol`) VALUES
(1, 'Junior A'),
(2, 'Junior B'),
(3, 'Senior ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sector`
--

CREATE TABLE `sector` (
  `Id_Sector` int(11) NOT NULL,
  `Descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sector`
--

INSERT INTO `sector` (`Id_Sector`, `Descripcion`) VALUES
(1, 'Prueba'),
(2, 'Banca'),
(3, 'Telecomunicaciones'),
(4, 'Retail'),
(5, 'Logística'),
(6, 'Financiero'),
(7, 'Servicios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `Id_Servicio` int(11) NOT NULL,
  `Nombre_Servicio` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`Id_Servicio`, `Nombre_Servicio`) VALUES
(1, 'Servicio'),
(2, 'Staffing');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `Id_Tipo_Documento` int(11) NOT NULL,
  `Descripcion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`Id_Tipo_Documento`, `Descripcion`) VALUES
(1, 'Cédula de Ciudadanía'),
(2, 'Cédula de Extranjería'),
(3, 'Tarjetas de Identidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estudio`
--

CREATE TABLE `tipo_estudio` (
  `Id_Tipo_Estudio` int(11) NOT NULL,
  `Nivel_Formacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_estudio`
--

INSERT INTO `tipo_estudio` (`Id_Tipo_Estudio`, `Nivel_Formacion`) VALUES
(1, 'Universitarios'),
(2, 'Posgrados'),
(3, 'Diplomados'),
(4, 'Tecnologo'),
(5, 'Doctorados'),
(6, 'Tecnico'),
(7, 'Licenciatura'),
(8, 'Certificación'),
(9, 'Idioma');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_hora`
--

CREATE TABLE `tipo_hora` (
  `Id_Tipo_Hora` varchar(45) NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_hora`
--

INSERT INTO `tipo_hora` (`Id_Tipo_Hora`, `Descripcion`) VALUES
('Extras Dominicales y Festivas Diurnas', 'Extras Dominicales y Festivas Diurnas'),
('Extras Dominicales y Festivas Nocturnas', 'Extras Dominicales y Festivas Nocturnas'),
('Extras Ordinarias Diurnas', 'Extras Ordinarias Diurnas'),
('Extras Ordinarias Nocturnas', 'Extras Ordinarias Nocturnas'),
('Otras', 'Otras'),
('Recargo Nocturno', 'Recargo Nocturno'),
('Si', 'Si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `Id_Tipo_Pago` int(11) NOT NULL,
  `Descripcion` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`Id_Tipo_Pago`, `Descripcion`) VALUES
(1, 'Pagas '),
(2, 'Compensadas'),
(3, 'Por Compensar '),
(4, 'Por pagar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Id_Usuario` int(11) NOT NULL,
  `Id_Tipo_Documento` int(11) DEFAULT NULL,
  `Id_Privilegio` int(11) DEFAULT NULL,
  `Num_Documento` varchar(15) DEFAULT NULL,
  `Lugar_Expedi` varchar(60) DEFAULT NULL,
  `Nombre_Usuario` varchar(45) DEFAULT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Email` varchar(50) NOT NULL,
  `Estado` tinyint(1) NOT NULL,
  `Fecha_Registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Created` varchar(25) DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Id_Usuario`, `Id_Tipo_Documento`, `Id_Privilegio`, `Num_Documento`, `Lugar_Expedi`, `Nombre_Usuario`, `Password`, `Email`, `Estado`, `Fecha_Registro`, `Created`) VALUES
(54, 1, 1, '43626729', 'Bogota', NULL, '-', 'liliana.naranjo@sqasa.com', 1, '2018-05-07 14:09:26', '2008-08-01 12:07:00.0'),
(59, 1, 1, '32140831', 'Leticia', NULL, '-', 'lina.cardona@sqasa.com', 1, '2018-07-10 09:30:13', '2008-09-01 08:29:00.0'),
(176, 1, 1, '1128404708', 'Bogota D.C', NULL, '-', 'esteffany.garces@sqasa.com', 1, '2018-07-10 11:51:59', '2011-04-13 14:47:00.0'),
(188, 1, 3, '71743963', 'Abejorral', '', '-', 'dora.daza@sqasa.com', 1, '2017-08-11 14:59:30', '2018-08-07 00:00:00.0'),
(222, 1, 1, '37182767', 'Bogota D.C', NULL, '-', 'isabel.coronel@sqasa.com', 1, '2018-07-10 09:57:39', '2012-05-03 08:08:00.0'),
(404, 1, 1, '43185970', 'Bogota D.C', NULL, '-', 'deisy.marin@sqasa.com', 1, '2018-07-10 11:18:27', '2013-08-06 11:41:00.0'),
(528, 1, 1, '16075058', 'Bogota D.C', NULL, '-', 'juan.loaiza@sqasa.com', 1, '2018-07-10 14:32:56', '2014-08-24 20:06:00.0'),
(609, 1, 1, '11111', 'Bogotá DC', NULL, '-', 'yiro.camargo@sqasa.com', 1, '2018-05-07 14:39:28', '2015-05-21 13:27:00.0'),
(613, 1, 1, '6130000000', 'Bogota D.C', NULL, '-', 'marisol.polo@sqasa.com', 1, '2018-07-10 10:53:34', '2015-06-02 16:34:00.0'),
(728, 1, 1, '1.036,604.143', 'Bogotá D.C.', NULL, '-', 'juan.munoz@sqasa.com', 1, '2018-07-10 17:11:12', '2015-10-26 17:51:30.0'),
(773, 1, 2, '1082848446', 'Bogota DC', NULL, '-', 'devys.fuentes@sqasa.com', 1, '2017-09-18 10:25:29', '0000-00-00 00:00:00'),
(792, 3, 1, '1.036.674.696', 'Bogotá DC', NULL, '-', 'keyla.garcia@sqasa.com', 0, '2018-05-07 14:41:59', '2016-05-31 10:04:53.0'),
(795, 1, 1, '1.074.557.424 ', 'Leticia', NULL, '-', 'tania.marin@sqasa.com', 1, '2018-09-10 11:52:32', '2016-06-14 07:48:02.0'),
(797, 1, 3, '1018503888', 'Bogota DC', NULL, '-', 'daniel.gomez@sqasa.com', 1, '2017-11-28 13:54:54', '0000-00-00 00:00:00'),
(813, 1, 1, '52.930.167', 'Bogota D.C', NULL, '-', 'diana.moreno@sqasa.com', 1, '2018-07-10 14:21:18', '2016-08-30 17:23:40.0'),
(846, 1, 1, '1032428201', 'Bogota D.C', NULL, '-', 'gelber.ospina@sqasa.com', 1, '2018-07-10 14:10:21', '2016-12-20 09:49:04.0'),
(853, 3, 4, '1.017.136.377', 'Bogotá DC', NULL, '-', 'julio.espinal@sqasa.com', 1, '2018-05-07 14:46:58', '2016-12-23 08:19:49.0'),
(871, 1, 3, '1051874158', 'Bogota DC', NULL, '-', 'yudy.presiga@sqasa.com', 1, '2017-08-15 10:06:14', '0000-00-00 00:00:00'),
(905, 2, 2, '5184584108', 'Bogota DC', NULL, '-', 'julieth.diaz@sqasa.com', 1, '2017-10-05 15:24:39', '0000-00-00 00:00:00'),
(906, 1, 1, '122348748', 'Bogota DC', NULL, '-', 'rafael.villa@sqasa.com', 1, '2017-10-05 15:12:36', '0000-00-00 00:00:00'),
(907, 1, 1, '415151515', 'Bogota DC', NULL, '-', 'carlos.vargas@sqasa.com', 1, '2017-10-05 15:22:00', '0000-00-00 00:00:00'),
(908, 1, 1, '5154874', 'Bogota DC', NULL, '-', 'lida.salcedo@sqasa.com', 1, '2017-10-05 14:50:49', '0000-00-00 00:00:00'),
(909, 1, 1, '1510054', 'Bogota DC', NULL, '-', 'maria.quijano@sqasa.com', 1, '2017-10-05 10:42:07', '0000-00-00 00:00:00'),
(914, 1, 1, '1073601276', 'Bogota DC', NULL, '-', 'maria.vallejo@sqasa.com', 1, '2017-09-25 15:46:58', '0000-00-00 00:00:00'),
(915, 1, 1, '15105484', 'Andes', NULL, '-', 'juan.villarraga@sqasa.com', 1, '2017-09-21 13:51:53', '0000-00-00 00:00:00'),
(916, 1, 1, '9160000000', 'Bogota D.C', NULL, '-', 'brianda.castillo@sqasa.com', 1, '2018-07-10 11:09:37', '2017-05-10 10:13:53.0'),
(917, 1, 1, '15125481', 'Bogota DC', NULL, '-', 'juan.estrada@sqasa.com', 1, '2017-09-05 14:26:45', '0000-00-00 00:00:00'),
(918, 1, 1, '879595', 'Bogota DC', NULL, '-', 'miguel.sabogal@sqasa.com', 1, '2017-09-05 08:29:19', '0000-00-00 00:00:00'),
(919, 1, 1, '8484848', 'Bogota DC', NULL, '-', 'carolina.corrales@sqasa.com', 1, '2017-09-05 08:28:44', '0000-00-00 00:00:00'),
(921, 1, 1, '12154578785', 'Bogota DC', NULL, '-', 'isabel.duque@sqasa.com', 1, '2017-09-01 13:18:23', '0000-00-00 00:00:00'),
(925, 1, 1, '102210154', 'Bogota DC', NULL, '-', 'jose.rodriguez@sqasa.com', 1, '2017-08-15 11:16:12', '0000-00-00 00:00:00'),
(929, 1, 1, '12345678900', 'Bogota D.C', NULL, '-', 'juan.diaz@sqasa.com', 1, '2018-03-07 16:35:24', '2017-06-06 16:55:06.'),
(930, 1, 1, '1234567899', 'Bogota D.C', NULL, '-', 'jefferson.roncancio@sqasa.com', 1, '2018-03-07 16:30:04', '2017-06-06 16:57:01.'),
(931, 1, 1, '0000000', 'Ciudad Gotica', NULL, '-', 'maria.sanchez@sqasa.com', 1, '2018-06-22 16:48:28', '2017-06-07 15:56:26.0'),
(932, 1, 1, '12584848', 'Medellin', NULL, '-', 'jonnier.leon@sqasa.com', 1, '2017-09-01 11:42:35', '0000-00-00 00:00:00'),
(933, 1, 1, '102104985', 'Cali', NULL, '-', 'emma.castiblanco@sqasa.com', 1, '2017-08-18 10:36:12', '0000-00-00 00:00:00'),
(934, 1, 1, '102104848', 'Bogota DC', NULL, '-', 'andrea.porras@sqasa.com', 1, '2017-08-15 10:27:20', '0000-00-00 00:00:00'),
(937, 1, 3, '2015487', 'Bogota DC', NULL, '-', 'david.daza@sqasa.com', 1, '2018-01-26 14:03:06', '0000-00-00 00:00:00'),
(940, 1, 1, '9400000000', 'Bogota D.C', NULL, '-', 'jhon.munoz@sqasa.com', 1, '2018-07-10 14:37:58', '2017-06-23 19:36:29.0'),
(942, 1, 1, '12345678900', 'Bogota D.C', NULL, '-', 'yesidd.ramirez@sqasa.com', 1, '2018-03-07 16:31:10', '2017-06-29 11:11:08.'),
(943, 1, 1, '1234567892', 'Bogota D.C', NULL, '-', 'ana.lizarazo@sqasa.com', 1, '2018-03-07 16:23:33', NULL),
(944, 1, 1, '9440000000', 'Bogota D.C', NULL, '-', 'andres.guerra@sqasa.com', 1, '2018-07-10 15:24:07', '2017-07-11 10:39:30.0'),
(948, 1, 1, '12345678990', 'Bogota D.C', NULL, '-', 'yeramy.gonzalez@sqasa.com', 1, '2018-03-16 11:51:12', '2017-07-26 11:01:55.'),
(949, 1, 1, '2121212121', '21', NULL, '-', 'julian.rodriguez@sqasa.com', 1, '2018-02-28 16:44:08', NULL),
(959, 1, 1, '1019046296', 'Bogota D.C', NULL, '-', 'leidy.castro@sqasa.com', 1, '2018-07-10 09:53:42', '2017-08-10 17:19:05.0'),
(981, 1, 1, '', '', NULL, '-', 'ivan.riobueno@sqasa.com', 1, '2018-02-28 16:43:35', NULL),
(986, 1, 4, '9860000000', 'Angelópolis', NULL, '-', 'jhoan.lopez@sqasa.com', 1, '2018-07-10 14:44:11', '2017-09-18 16:12:00.0'),
(989, 1, 1, '0000000', 'Ciudad Gotica', NULL, '-', 'wilman.puentes@sqasa.com', 1, '2018-05-07 08:48:44', '2017-09-18 16:16:10.0'),
(998, 1, 3, '1030690342', 'Bogota D.C', NULL, '-', 'javier.timote@sqasa.com', 1, '2018-02-05 17:10:55', '2017-10-11 00:00:00'),
(999, 1, 3, '12345678901', 'Bogota D.C', NULL, '-', 'fabio.betancourt@sqasa.com', 1, '2018-02-22 15:40:19', '0000-00-00 00:00:00'),
(1019, 1, 1, '1019000000', 'Bogota D.C', NULL, '-', 'anderson.hernandez@sqasa.com', 1, '2018-07-10 14:41:32', '2017-11-03 17:18:17.0'),
(1021, 1, 1, '1021000000', 'Bogota D.C', NULL, '-', 'juanm.villarraga@sqasa.com', 1, '2018-07-10 09:44:29', '2017-11-15 10:16:02.0'),
(1026, 1, 1, '1036935721', '', NULL, '-', 'juan.gaviria@sqasa.com', 1, '2018-02-28 16:43:25', NULL),
(1031, 1, 1, '1031000000', 'Bogota D.C', NULL, '-', 'gustavo.lopez@sqasa.com', 1, '2018-07-10 11:31:25', '2017-12-15 17:21:03.0'),
(1034, 1, 1, '12345678900', 'Narnia', NULL, '-', 'john.duque@sqasa.com', 1, '2018-02-23 15:10:21', NULL),
(1036, 2, 3, '1021049', 'Bogota DC', NULL, '-', 'bradon.ortega@sqasa.com', 1, '2018-02-01 11:47:08', '0000-00-00 00:00:00'),
(1037, 1, 3, '1014276648', 'Luna', NULL, '-', 'carlos.leal@sqasa.com', 1, '2018-03-01 15:34:32', NULL),
(1038, 1, 2, '102555145', 'Bogota DC', NULL, '-', 'andres.rincon@sqasa.com', 0, '2018-02-01 11:41:54', '0000-00-00 00:00:00'),
(1039, 1, 1, '125454105', 'Bogota DC', NULL, '-', 'albeiro.suarez@sqasa.com', 1, '2018-01-18 14:16:45', '0000-00-00 00:00:00'),
(1048, 1, 1, '092836421', 'Bogota D.C', NULL, '-', 'diomar.varon@sqasa.com', 1, '2018-04-09 09:16:33', '2018-01-18 08:59:59.'),
(1050, 1, 1, '1050000000', 'Bogota D.C', NULL, '-', 'auxiliat.ti@sqasa.com', 1, '2018-07-10 15:15:20', '2018-01-23 07:29:04.0'),
(1052, 1, 1, '55555555', 'Bogota D.C', NULL, '-', 'sandra.osorio@sqasa.com', 1, '2018-04-06 17:02:55', '2018-01-23 13:55:41.'),
(1057, 1, 1, '0000000', 'Bogotá DC', NULL, '-', 'johanna.botero@sqasa.com', 0, '2018-05-29 16:28:01', '2018-01-26 11:14:14.0'),
(1058, 1, 1, '1234567788', 'Bogota D.C', NULL, '-', 'german.suarez@sqasa.com', 1, '2018-04-03 11:13:00', '2018-02-06 09:51:01.'),
(1059, 1, 1, 'f', 'd', NULL, '-', 'milton.rodriguez@sqasa.com', 1, '2018-03-26 11:19:24', '2018-02-12 10:38:28.'),
(1060, 1, 1, '', 'Bogota D.C', NULL, '-', 'santiago.bohorquez@sqasa.com', 1, '2018-03-26 10:15:09', '2018-02-14 17:28:45.'),
(1061, 1, 1, '23890789', 'Bogota D.C', NULL, '-', 'natali.monsalve@sqasa.com', 1, '2018-03-23 09:17:47', '2018-02-14 17:30:24.'),
(1062, 1, 1, '9987654321', 'Bogota D.C', NULL, '-', 'juan.puyo@sqasa.com', 1, '2018-03-22 16:07:51', '2018-02-19 11:30:43.'),
(1081, 1, 1, '1081000000', 'Bogota D.C', NULL, '-', 'yaremis.martinez@sqasa.com', 1, '2018-07-10 11:36:48', '2018-04-09 14:13:27.0'),
(1092, 1, 1, '1014276648', 'Bogotá DC', NULL, '-', 'diego.sierra@sqasa.com', 0, '2018-05-09 14:40:51', '2018-04-13 11:36:07.0'),
(1093, 1, 1, '1215112', 'Bogotá DC', NULL, '-', 'ferney.diaz@sqasa.com', 1, '2018-05-09 14:27:15', '2018-04-16 13:57:02.0'),
(1094, 2, 5, '11111', 'Ciudad Gotica', NULL, '-', 'fernand.alvarez@sqasa.com', 1, '2018-04-30 13:42:02', '2018-04-17 07:48:00.0'),
(1095, 1, 2, '0000000', 'Bogotá DC', NULL, '-', 'jenny.buitrago@sqasa.com', 1, '2018-04-30 09:44:25', '2018-04-18 10:22:22.0'),
(1096, 1, 1, '0000000', 'Ciudad Gotica', NULL, '-', 'gissela.cardona@sqasa.com', 1, '2018-04-26 16:53:26', '2018-04-18 10:34:06.0'),
(1111, 1, 1, '1014276648', 'Bogotá DC', NULL, '-', 'miguel.plazas@sqasa.com', 1, '2018-05-29 16:29:47', '2018-05-18 15:18:16.0'),
(1136, 1, 1, '1014276648', 'Bogotá DC', NULL, '-', 'julieth.mora@sqasa.com', 1, '2018-07-03 12:49:04', '2018-06-20 15:27:39.0'),
(1137, 1, 1, '0000000', 'Ciudad Gotica', NULL, '-', 'monica.avendano@sqasa.com', 1, '2018-07-03 12:38:17', '2018-06-21 11:27:07.0'),
(1144, 1, 1, '0000000', 'San Pedro', NULL, '-', 'Capacitacion_2@sqasa.com', 1, '2018-08-03 11:42:37', '2018-06-21 17:42:35.0'),
(1145, 1, 1, '11111', 'Leticia', NULL, '-', 'Capacitacion_3@sqasa.com', 1, '2018-08-01 17:05:49', '2018-06-21 17:43:01.0'),
(1146, 1, 2, '00000001', 'Nechí', NULL, '-', 'Capacitacion_4@sqasa.com', 1, '2018-08-01 12:25:47', '2018-06-21 17:43:26.0'),
(1147, 3, 5, '11111', 'Leticia', NULL, '-', 'Capacitacion_5@sqasa.com', 0, '2018-07-31 17:27:42', '2018-06-21 17:43:53.0'),
(1148, 1, 1, 's', 'Anzá', NULL, '-', 'oscar.delgadillo@sqasa.com', 1, '2018-07-31 16:57:02', '2018-06-25 10:56:36.0'),
(1149, 3, 9, '11111', 'Leticia', NULL, '-', 'luis.diaz@sqasa.com', 1, '2018-07-31 16:42:21', '2018-06-25 10:57:24.0'),
(1150, 1, 1, '11111', 'Bogotá D.C.', NULL, '-', 'milena.orjuela@sqasa.com', 1, '2018-07-31 16:39:41', '2018-06-27 11:25:54.0'),
(1151, 1, 2, '0000000', 'Ciudad Gotica', NULL, '-', 'angie.gomez@sqasa.com', 0, '2018-07-31 12:24:44', '2018-06-27 16:54:24.0'),
(1152, 1, 4, '0000000', 'Leticia', NULL, '-', 'asistente.th@sqasa.com', 1, '2018-07-31 12:00:53', '2018-06-29 09:42:47.0'),
(1180, 1, 1, '0', 'Sin ciudad', NULL, '-', 'sol.fuquen@sqasa.com', 1, '2018-09-19 16:01:33', '2018-07-26 15:51:15.0'),
(1194, 1, 1, '0', 'Sin ciudad', NULL, '-', 'milena.arboleda@sqasa.com', 1, '2018-09-19 11:15:44', '2018-08-09 13:20:19.0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_has_cliente`
--

CREATE TABLE `usuario_has_cliente` (
  `idExcepcion` int(11) NOT NULL,
  `usuario_Id_Usuario` int(11) NOT NULL,
  `cliente_Id_Cliente` int(11) NOT NULL,
  `Contar` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_has_cliente`
--

INSERT INTO `usuario_has_cliente` (`idExcepcion`, `usuario_Id_Usuario`, `cliente_Id_Cliente`, `Contar`) VALUES
(1, 999, 72, 1),
(7, 998, 72, 1),
(12, 1037, 54, 1),
(14, 1057, 84, 1),
(30, 998, 84, 1),
(31, 59, 53, 1),
(32, 1021, 72, 1),
(33, 959, 72, 1),
(34, 222, 72, 1),
(35, 613, 101, 1),
(36, 916, 85, 1),
(37, 404, 85, 1),
(40, 1031, 72, 1),
(41, 1081, 72, 1),
(42, 176, 10, 1),
(43, 998, 10, 1),
(44, 846, 10, 1),
(45, 1037, 10, 1),
(46, 813, 10, 1),
(47, 528, 10, 1),
(48, 940, 10, 1),
(49, 1019, 10, 1),
(50, 986, 10, 1),
(51, 1038, 10, 1),
(52, 999, 10, 1),
(53, 918, 10, 1),
(55, 1036, 10, 1),
(56, 1050, 10, 1),
(57, 937, 10, 1),
(59, 944, 10, 1),
(61, 1039, 101, 1),
(62, 1036, 41, 1),
(73, 1180, 72, 1),
(74, 1180, 84, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`idaction`),
  ADD KEY `fk_action_action1_idx1` (`action_idaction`);

--
-- Indices de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  ADD PRIMARY KEY (`Id_Auditoria`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`Id_Cargo`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`Id_Ciudad`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`Id_Cliente`);

--
-- Indices de la tabla `datos_analista`
--
ALTER TABLE `datos_analista`
  ADD PRIMARY KEY (`Id_Datos`),
  ADD UNIQUE KEY `Id_Usuario` (`Id_Usuario`),
  ADD KEY `Cargo_FK_01` (`Id_Cargo`),
  ADD KEY `Rol_FK_01` (`Id_Rol`),
  ADD KEY `Ciudad_Fk_01` (`Id_Ciudad`),
  ADD KEY `Servicio_FK_01` (`Id_Servicio`),
  ADD KEY `Cliente_FK_01` (`Id_Cliente`),
  ADD KEY `Usuario_FK_01` (`Id_Usuario`);

--
-- Indices de la tabla `datos_analista_novedad`
--
ALTER TABLE `datos_analista_novedad`
  ADD PRIMARY KEY (`id_datos_analista_novedad`),
  ADD KEY `fk_datos_analista_novedad_novedad1_idx` (`novedad_Id_Novedad`),
  ADD KEY `fk_datos_analista_novedad_cliente1_idx` (`cliente_Id_Cliente`),
  ADD KEY `fk_datos_analista_novedad_datos_analista1_idx` (`datos_analista_Id_Datos`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id_deaprtamento`);

--
-- Indices de la tabla `detalle_horas`
--
ALTER TABLE `detalle_horas`
  ADD PRIMARY KEY (`Id_Detalle_Horas`),
  ADD KEY `Datos_Fk_03` (`Id_Datos`),
  ADD KEY `Horas_Extras_Fk_01` (`Id_Horas_Extras`),
  ADD KEY `Tipo_Pago_Fk_01` (`Id_Tipo_Pago`);

--
-- Indices de la tabla `estudio`
--
ALTER TABLE `estudio`
  ADD PRIMARY KEY (`Id_Estudio`),
  ADD KEY `Datos_FK_01` (`Id_Datos`),
  ADD KEY `Tipo_Estudio_FK_01` (`Id_Tipo_Estudio`);

--
-- Indices de la tabla `experiencia_laboral`
--
ALTER TABLE `experiencia_laboral`
  ADD PRIMARY KEY (`Id_Exp_Laboral`),
  ADD KEY `Datos_FK_02` (`Id_Datos`),
  ADD KEY `Sector_FK_01` (`Id_Sector`);

--
-- Indices de la tabla `horas_extras`
--
ALTER TABLE `horas_extras`
  ADD PRIMARY KEY (`Id_Horas_Extras`),
  ADD KEY `Id_Tipo_Hora_fk` (`Id_Tipo_Hora_fk`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id_municipios`),
  ADD KEY `departamento` (`id_deaprtamento`);

--
-- Indices de la tabla `novedad`
--
ALTER TABLE `novedad`
  ADD PRIMARY KEY (`Id_Novedad`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`idprivilegios`);

--
-- Indices de la tabla `permiso_has_action`
--
ALTER TABLE `permiso_has_action`
  ADD PRIMARY KEY (`permiso_idprivilegios`,`action_idaction`,`privilegios_Id_Privilegio`),
  ADD KEY `fk_permiso_has_action_action1_idx` (`action_idaction`),
  ADD KEY `fk_permiso_has_action_permiso1_idx` (`permiso_idprivilegios`),
  ADD KEY `fk_permiso_has_action_privilegios1_idx` (`privilegios_Id_Privilegio`);

--
-- Indices de la tabla `privilegios`
--
ALTER TABLE `privilegios`
  ADD PRIMARY KEY (`Id_Privilegio`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`Id_Rol`);

--
-- Indices de la tabla `sector`
--
ALTER TABLE `sector`
  ADD PRIMARY KEY (`Id_Sector`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`Id_Servicio`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`Id_Tipo_Documento`);

--
-- Indices de la tabla `tipo_estudio`
--
ALTER TABLE `tipo_estudio`
  ADD PRIMARY KEY (`Id_Tipo_Estudio`);

--
-- Indices de la tabla `tipo_hora`
--
ALTER TABLE `tipo_hora`
  ADD PRIMARY KEY (`Id_Tipo_Hora`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`Id_Tipo_Pago`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Id_Usuario`),
  ADD KEY `Tipo_Documento_FK_01` (`Id_Tipo_Documento`),
  ADD KEY `Perfil_01_FK` (`Id_Privilegio`);

--
-- Indices de la tabla `usuario_has_cliente`
--
ALTER TABLE `usuario_has_cliente`
  ADD PRIMARY KEY (`idExcepcion`),
  ADD KEY `fk_usuario_has_cliente_cliente1_idx` (`cliente_Id_Cliente`),
  ADD KEY `fk_usuario_has_cliente_usuario1_idx` (`usuario_Id_Usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `action`
--
ALTER TABLE `action`
  MODIFY `idaction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  MODIFY `Id_Auditoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `Id_Cargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `Id_Ciudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `datos_analista`
--
ALTER TABLE `datos_analista`
  MODIFY `Id_Datos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT de la tabla `datos_analista_novedad`
--
ALTER TABLE `datos_analista_novedad`
  MODIFY `id_datos_analista_novedad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT de la tabla `detalle_horas`
--
ALTER TABLE `detalle_horas`
  MODIFY `Id_Detalle_Horas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT de la tabla `estudio`
--
ALTER TABLE `estudio`
  MODIFY `Id_Estudio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT de la tabla `experiencia_laboral`
--
ALTER TABLE `experiencia_laboral`
  MODIFY `Id_Exp_Laboral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `novedad`
--
ALTER TABLE `novedad`
  MODIFY `Id_Novedad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `idprivilegios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `privilegios`
--
ALTER TABLE `privilegios`
  MODIFY `Id_Privilegio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `Id_Rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sector`
--
ALTER TABLE `sector`
  MODIFY `Id_Sector` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `Id_Servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `Id_Tipo_Documento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_estudio`
--
ALTER TABLE `tipo_estudio`
  MODIFY `Id_Tipo_Estudio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `Id_Tipo_Pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuario_has_cliente`
--
ALTER TABLE `usuario_has_cliente`
  MODIFY `idExcepcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `action`
--
ALTER TABLE `action`
  ADD CONSTRAINT `fk_action_action1` FOREIGN KEY (`action_idaction`) REFERENCES `action` (`idaction`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `datos_analista`
--
ALTER TABLE `datos_analista`
  ADD CONSTRAINT `Cargo_FK_01` FOREIGN KEY (`Id_Cargo`) REFERENCES `cargo` (`Id_Cargo`),
  ADD CONSTRAINT `Ciudad_Fk_01` FOREIGN KEY (`Id_Ciudad`) REFERENCES `ciudad` (`Id_Ciudad`),
  ADD CONSTRAINT `Cliente_FK_01` FOREIGN KEY (`Id_Cliente`) REFERENCES `cliente` (`Id_Cliente`),
  ADD CONSTRAINT `Rol_FK_01` FOREIGN KEY (`Id_Rol`) REFERENCES `rol` (`Id_Rol`),
  ADD CONSTRAINT `Servicio_FK_01` FOREIGN KEY (`Id_Servicio`) REFERENCES `servicio` (`Id_Servicio`),
  ADD CONSTRAINT `Usuario_FK_01` FOREIGN KEY (`Id_Usuario`) REFERENCES `usuario` (`Id_Usuario`);

--
-- Filtros para la tabla `datos_analista_novedad`
--
ALTER TABLE `datos_analista_novedad`
  ADD CONSTRAINT `fk_datos_analista_novedad_cliente1` FOREIGN KEY (`cliente_Id_Cliente`) REFERENCES `cliente` (`Id_Cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_datos_analista_novedad_datos_analista1` FOREIGN KEY (`datos_analista_Id_Datos`) REFERENCES `datos_analista` (`Id_Datos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_datos_analista_novedad_novedad1` FOREIGN KEY (`novedad_Id_Novedad`) REFERENCES `novedad` (`Id_Novedad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_horas`
--
ALTER TABLE `detalle_horas`
  ADD CONSTRAINT `Datos_Fk_03` FOREIGN KEY (`Id_Datos`) REFERENCES `datos_analista` (`Id_Datos`),
  ADD CONSTRAINT `Horas_Extras_Fk_01` FOREIGN KEY (`Id_Horas_Extras`) REFERENCES `horas_extras` (`Id_Horas_Extras`),
  ADD CONSTRAINT `Tipo_Pago_Fk_01` FOREIGN KEY (`Id_Tipo_Pago`) REFERENCES `tipo_pago` (`Id_Tipo_Pago`);

--
-- Filtros para la tabla `estudio`
--
ALTER TABLE `estudio`
  ADD CONSTRAINT `Datos_FK_01` FOREIGN KEY (`Id_Datos`) REFERENCES `datos_analista` (`Id_Datos`),
  ADD CONSTRAINT `Tipo_Estudio_FK_01` FOREIGN KEY (`Id_Tipo_Estudio`) REFERENCES `tipo_estudio` (`Id_Tipo_Estudio`);

--
-- Filtros para la tabla `experiencia_laboral`
--
ALTER TABLE `experiencia_laboral`
  ADD CONSTRAINT `Datos_FK_02` FOREIGN KEY (`Id_Datos`) REFERENCES `datos_analista` (`Id_Datos`),
  ADD CONSTRAINT `Sector_FK_01` FOREIGN KEY (`Id_Sector`) REFERENCES `sector` (`Id_Sector`);

--
-- Filtros para la tabla `horas_extras`
--
ALTER TABLE `horas_extras`
  ADD CONSTRAINT `Id_Tipo_Hora_fk` FOREIGN KEY (`Id_Tipo_Hora_fk`) REFERENCES `tipo_hora` (`Id_Tipo_Hora`);

--
-- Filtros para la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD CONSTRAINT `municipios_ibfk_1` FOREIGN KEY (`id_deaprtamento`) REFERENCES `departamentos` (`id_deaprtamento`);

--
-- Filtros para la tabla `permiso_has_action`
--
ALTER TABLE `permiso_has_action`
  ADD CONSTRAINT `fk_permiso_has_action_action1` FOREIGN KEY (`action_idaction`) REFERENCES `action` (`idaction`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_permiso_has_action_privilegios1` FOREIGN KEY (`privilegios_Id_Privilegio`) REFERENCES `privilegios` (`Id_Privilegio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `Perfil_01_FK` FOREIGN KEY (`Id_Privilegio`) REFERENCES `privilegios` (`Id_Privilegio`),
  ADD CONSTRAINT `Tipo_Documento_FK_01` FOREIGN KEY (`Id_Tipo_Documento`) REFERENCES `tipo_documento` (`Id_Tipo_Documento`);

--
-- Filtros para la tabla `usuario_has_cliente`
--
ALTER TABLE `usuario_has_cliente`
  ADD CONSTRAINT `fk_usuario_has_cliente_cliente1` FOREIGN KEY (`cliente_Id_Cliente`) REFERENCES `cliente` (`Id_Cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_cliente_usuario1` FOREIGN KEY (`usuario_Id_Usuario`) REFERENCES `usuario` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
